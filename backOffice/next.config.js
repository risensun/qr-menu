/** @type {import('next').NextConfig} */

const withPreact = require('next-plugin-preact');

const nextConfig = withPreact({
  reactStrictMode: true,
  env: {
    TENANT_ID: process.env.TENANT_ID,
  },
  webpack: (config, { dev, isServer }) => {
    if (!dev && !isServer) {
      Object.assign(config.resolve.alias, {
        react: 'preact/compat',
        'react-dom/test-utils': 'preact/test-utils',
        'react-dom': 'preact/compat',
      });
    }

    return config;
  },
});

module.exports = nextConfig;

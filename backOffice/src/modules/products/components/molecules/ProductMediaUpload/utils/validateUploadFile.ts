import { message } from 'antd';
import { RcFile } from 'antd/es/upload/interface';

export function validateUploadFile(file: RcFile): boolean {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';

  if (!isJpgOrPng) {
    message.error('Разрешены только изображения в формате JPG/PNG');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;

  if (!isLt2M) {
    message.error('Размер файла не должен превышать 2MB');
  }

  return isJpgOrPng && isLt2M;
}

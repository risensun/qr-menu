import type { RcFile } from 'antd/es/upload/interface';

export function getBase64(img: RcFile): Promise<string> {
  return new Promise((resolve) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => resolve(reader.result as string));
    reader.readAsDataURL(img);
  });
}

import React from 'react';
import { Form, Upload, UploadProps } from 'antd';
import { UploadChangeParam } from 'antd/lib/upload';
import { RcFile, UploadFile } from 'antd/lib/upload/interface';
import { validateUploadFile } from './utils/validateUploadFile';
import { getBase64 } from './utils/getBase64';
import { Loader } from 'src/modules/common';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

export type ProductMediaUploadProps = {
  readonly error: string | null;
  readonly mediaSrc?: string;
  readonly onChange: (file: Blob) => void;
};

export const ProductMediaUpload: React.FC<ProductMediaUploadProps> = ({
  error,
  mediaSrc,
  onChange,
}) => {
  const [isLoading, setIsLoading] = React.useState(false);
  const [imageUrl, setImageUrl] = React.useState<string | undefined>(
    mediaSrc || undefined
  );

  const handleChange: UploadProps['onChange'] = (
    info: UploadChangeParam<UploadFile>
  ) => {
    const { file } = info;

    if (file.status === 'uploading') {
      setIsLoading(true);

      return;
    }

    if (file.status === 'done') {
      getBase64(info.file.originFileObj as RcFile).then((url) => {
        setIsLoading(false);
        setImageUrl(url);

        if (info.file.originFileObj) {
          onChange(info.file.originFileObj);
        }
      });
    }
  };

  return (
    <Form layout="vertical">
      <Form.Item
        label="Изображение товара"
        validateStatus={error ? 'error' : 'success'}
        help={error}
      >
        <Upload
          name="avatar"
          listType="picture-card"
          className="avatar-uploader"
          showUploadList={false}
          action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
          beforeUpload={validateUploadFile}
          onChange={handleChange}
          customRequest={(options) => {
            const { onSuccess } = options;

            setTimeout(() => {
              if (onSuccess) {
                onSuccess('ok');
              }
            }, 1);
          }}
        >
          {imageUrl ? (
            <Loader spinning={isLoading}>
              <div
                style={{
                  width: 102,
                  height: 102,
                }}
              >
                <img
                  src={imageUrl}
                  alt="avatar"
                  style={{
                    width: '100%',
                    height: '100%',
                    objectFit: 'cover',
                    objectPosition: 'center',
                  }}
                />
              </div>
            </Loader>
          ) : (
            <div>
              {isLoading ? <LoadingOutlined /> : <PlusOutlined />}
              <div style={{ marginTop: 8 }}>Upload</div>
            </div>
          )}
        </Upload>
      </Form.Item>
    </Form>
  );
};

export type ProductSizeOptionInputGroupState = {
  readonly key: string;
  readonly name: string;
  readonly value: number;
  readonly price: number;
  readonly kkal: number;
  readonly proteins: number;
  readonly fat: number;
  readonly carbohydrate: number;
  // readonly isActive: boolean;
};

export type ProductBadgesCheckboxGroupState = {
  readonly badges: string[];
};

export type ProductSizeInputGroupState = {
  readonly name: string;
  readonly unit: string;
  readonly options: {
    entities: {
      [key: string]: ProductSizeOptionInputGroupState;
    };
    keys: string[];
  };
};

export type ProductSizeInputGroupErrors = {
  name: string | null;
  unit: string | null;
  options: {
    // Option key + field name
    [key: string]: string | null;
  };
};

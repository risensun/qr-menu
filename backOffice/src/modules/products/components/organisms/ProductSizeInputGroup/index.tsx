import React from 'react';
import { Button, Form, Input, Space, Typography } from 'antd';
import { nanoid } from '@reduxjs/toolkit';
import { PlusOutlined } from '@ant-design/icons';
import { SizeOptionInputGroup } from './partials/SizeOptionInputGroup';
import {
  ProductSizeInputGroupErrors,
  ProductSizeInputGroupState,
  ProductSizeOptionInputGroupState,
} from './types';
import { getProductSizeOptionEmpty } from 'src/modules/products/utils';

type InputConfig<T> = {
  name: T;
  label: string;
  placeholder: string;
};

type SizeFlatDataKeys = keyof Pick<ProductSizeInputGroupState, 'name' | 'unit'>;

export type ProductSizeInputGroupProps = {
  readonly state: ProductSizeInputGroupState;
  readonly errors: ProductSizeInputGroupErrors;
  readonly onChange: (state: ProductSizeInputGroupState) => void;
};

export const ProductSizeInputGroup: React.FC<ProductSizeInputGroupProps> = ({
  state,
  errors,
  onChange,
}) => {
  const inputs: InputConfig<SizeFlatDataKeys>[] = [
    {
      name: 'name',
      label: 'Что измеряем?',
      placeholder: 'Например: объем, порция...',
    },
    {
      name: 'unit',
      label: 'В чем измеряем?',
      placeholder: 'Например: мл, гр, шт',
    },
  ];

  const handleSizeInputChange = React.useCallback(
    (name: keyof ProductSizeInputGroupState, value: string) => {
      onChange({
        ...state,
        [name]: value,
      });
    },
    [state, onChange]
  );

  const handleSizeOptionStateChange = React.useCallback(
    (optionState: ProductSizeOptionInputGroupState) => {
      const { options } = state;

      const nextEntities = {
        ...options.entities,
        [optionState.key]: optionState,
      };

      onChange({
        ...state,
        options: {
          ...options,
          entities: nextEntities,
        },
      });
    },
    [state, onChange]
  );

  const handleSizeOptionSwap = React.useCallback(
    (currentIndex: number, nextIndex: number) => {
      const { options } = state;

      if (nextIndex < 0 || nextIndex >= options.keys.length) {
        return;
      }

      const nextKeys = [...options.keys];

      const temp = nextKeys[currentIndex];
      nextKeys[currentIndex] = nextKeys[nextIndex];
      nextKeys[nextIndex] = temp;

      onChange({
        ...state,
        options: {
          ...options,
          keys: nextKeys,
        },
      });
    },
    [state, onChange]
  );

  const handleSizeOptionAdd = React.useCallback(() => {
    const { options } = state;

    const emptySizeOption = getProductSizeOptionEmpty();
    const key = emptySizeOption.key;

    const nextKeys = [...options.keys, key];
    const nextEntities = {
      ...options.entities,
      [emptySizeOption.key]: emptySizeOption,
    };

    onChange({
      ...state,
      options: {
        ...options,
        keys: nextKeys,
        entities: nextEntities,
      },
    });
  }, [state, onChange]);

  const handleSizeOptionDelete = React.useCallback(
    (key: string, index: number) => {
      const { options } = state;

      const nextKeys = [...options.keys];
      nextKeys.splice(index, 1);

      const nextEntities = { ...options.entities };
      delete nextEntities[key];

      onChange({
        ...state,
        options: {
          ...options,
          entities: nextEntities,
          keys: nextKeys,
        },
      });
    },
    [state, onChange]
  );

  const getValidateStatus = React.useCallback(
    (name: keyof ProductSizeInputGroupErrors) => {
      if (!errors || !errors[name]) {
        return 'success';
      }

      return 'error';
    },
    [errors]
  );

  const getErrorMessage = React.useCallback(
    (name: keyof Omit<ProductSizeInputGroupErrors, 'options'>) =>
      errors[name] || null,
    [errors]
  );

  return (
    <>
      <Typography.Paragraph strong>Конфигурация размеров</Typography.Paragraph>
      <Form layout="vertical">
        {inputs.map(({ name, label, placeholder }) => (
          <Form.Item
            key={name}
            label={label}
            validateStatus={getValidateStatus(name)}
            help={getErrorMessage(name)}
          >
            <Input
              value={state[name]}
              placeholder={placeholder}
              onChange={({ target }) =>
                handleSizeInputChange(name, target.value)
              }
            />
          </Form.Item>
        ))}
      </Form>
      <Typography.Paragraph>Размеры</Typography.Paragraph>
      <Space size="small" direction="vertical" style={{ width: '100%' }}>
        {state.options.keys.map((key, index, options) => (
          <SizeOptionInputGroup
            key={key}
            unit={state.unit}
            state={state.options.entities[key]}
            errors={errors.options}
            isDeletable={options.length > 1}
            isMoveUpAvailable={index > 0}
            isMoveDownAvailable={index < options.length - 1}
            onChange={handleSizeOptionStateChange}
            onDelete={() => handleSizeOptionDelete(key, index)}
            onMoveUp={() => handleSizeOptionSwap(index, index - 1)}
            onMoveDown={() => handleSizeOptionSwap(index, index + 1)}
          />
        ))}
        <Button icon={<PlusOutlined />} onClick={handleSizeOptionAdd}>
          Добавить еще размер
        </Button>
      </Space>
    </>
  );
};

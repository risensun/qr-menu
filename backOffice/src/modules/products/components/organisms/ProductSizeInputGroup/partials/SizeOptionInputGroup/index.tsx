import React from 'react';
import {
  ProductSizeInputGroupErrors,
  ProductSizeOptionInputGroupState,
} from '../../types';
import { UpOutlined, DownOutlined, DeleteOutlined } from '@ant-design/icons';
import { Button, Card, Col, Form, Input, Row, Space, Tooltip } from 'antd';

type InputConfig = {
  name: keyof ProductSizeOptionInputGroupState;
  type: 'text' | 'number';
  placeholder: string;
  addon?: React.ReactNode;
  tooltip?: string;
};

export type SizeOptionInputGroupProps = {
  readonly unit: string;
  readonly state: ProductSizeOptionInputGroupState;
  readonly errors: ProductSizeInputGroupErrors['options'];
  readonly isDeletable: boolean;
  readonly isMoveUpAvailable: boolean;
  readonly isMoveDownAvailable: boolean;
  readonly onChange: (state: ProductSizeOptionInputGroupState) => void;
  readonly onDelete: () => void;
  readonly onMoveUp: () => void;
  readonly onMoveDown: () => void;
};

export const SizeOptionInputGroup: React.FC<SizeOptionInputGroupProps> = ({
  unit,
  state,
  errors,
  isDeletable,
  isMoveUpAvailable,
  isMoveDownAvailable,
  onChange,
  onDelete,
  onMoveUp,
  onMoveDown,
}) => {
  const actions = [
    {
      key: 'up',
      icon: <UpOutlined />,
      isDisabled: !isMoveUpAvailable,
      onClick: onMoveUp,
    },
    {
      key: 'down',
      icon: <DownOutlined />,
      isDisabled: !isMoveDownAvailable,
      onClick: onMoveDown,
    },
    {
      key: 'delete',
      icon: <DeleteOutlined />,
      isDisabled: !isDeletable,
      onClick: onDelete,
    },
  ];

  const rows: InputConfig[][] = [
    [
      {
        name: 'name',
        type: 'text',
        placeholder: 'Навзвание размера',
      },
    ],
    [
      {
        name: 'value',
        type: 'number',
        placeholder: '500',
        addon: unit,
      },
      {
        name: 'price',
        type: 'number',
        placeholder: '349',
        addon: '₽',
      },
    ],
    [
      {
        name: 'kkal',
        type: 'number',
        placeholder: '500',
        addon: <Tooltip overlay="Ккал">К</Tooltip>,
      },
      {
        name: 'proteins',
        type: 'number',
        placeholder: '500',
        addon: <Tooltip overlay="Белки">Б</Tooltip>,
      },
      {
        name: 'fat',
        type: 'number',
        placeholder: '500',
        addon: <Tooltip overlay="Жиры">Ж</Tooltip>,
      },
      {
        name: 'carbohydrate',
        type: 'number',
        placeholder: '500',
        addon: <Tooltip overlay="У">У</Tooltip>,
      },
    ],
  ];

  const handleChange = React.useCallback(
    (name: keyof ProductSizeOptionInputGroupState, value: string | number) => {
      onChange({
        ...state,
        [name]: value,
      });
    },
    [state, onChange]
  );

  const getValidateStatus = React.useCallback(
    (key: keyof typeof errors) => {
      if (!errors[key]) {
        return 'success';
      }

      return 'error';
    },
    [errors]
  );

  const getErrorMessage = React.useCallback(
    (key: keyof typeof errors) => errors[key] || null,
    [errors]
  );

  return (
    <Card bodyStyle={{ paddingBottom: 0 }}>
      <Space
        direction="horizontal"
        size="middle"
        style={{ width: '100%' }}
        align="start"
      >
        <Space direction="vertical" size="small" align="end">
          {actions.map(({ key, icon, isDisabled, onClick }) => (
            <Button
              key={key}
              icon={icon}
              size="middle"
              shape="circle"
              disabled={isDisabled}
              danger={key === 'delete'}
              onClick={onClick}
            />
          ))}
        </Space>
        <Form layout="vertical">
          {rows.map((inputs, index) => (
            <Row key={index} gutter={[8, 0]}>
              {inputs.map(({ name, type, placeholder, addon }) => {
                const value =
                  typeof state[name] === 'number' && state[name] < 0
                    ? ''
                    : state[name];

                return (
                  <Col key={name} span={Math.floor(24 / inputs.length)}>
                    <Form.Item
                      validateStatus={getValidateStatus(`${state.key}.${name}`)}
                      help={getErrorMessage(`${state.key}.${name}`)}
                    >
                      <Input
                        type={type}
                        value={value}
                        placeholder={placeholder}
                        addonAfter={addon}
                        onChange={({ target }) =>
                          handleChange(
                            name,
                            type === 'number'
                              ? parseInt(target.value) || -1
                              : target.value
                          )
                        }
                      />
                    </Form.Item>
                  </Col>
                );
              })}
            </Row>
          ))}
        </Form>
      </Space>
    </Card>
  );
};

import { Tabs } from 'antd';
import React from 'react';
import {
  useProductInfoInputGroup,
  useProductMediaUpload,
  useProductSizeInputGroup,
} from 'src/modules/products/hooks';
import { useProductBadgesCheckboxGroup } from 'src/modules/products/hooks/useProductBadgesCheckboxGroup';
import { projectProductSizeToState } from 'src/modules/products/utils';
import { Product } from 'src/types/Product';
import { ProductMediaUpload } from '../../molecules';
import { ProductBadgesCheckboxGroup } from '../ProductBadgesCheckboxGroup';
import {
  ProductInfoInputGroup,
  ProductInfoInputGroupState,
} from '../ProductInfoInputGroup';
import { ProductSizeInputGroup } from '../ProductSizeInputGroup';
import { ProductBadgesCheckboxGroupState } from '../ProductSizeInputGroup/types';

enum TabKey {
  Common = 'COMMON',
  Characteristics = 'CHARACTERISTICS',
}

export type ProductFormState = ProductInfoInputGroupState & {
  badges: string[];
  size: Product['size'];
};

export type ProductFormProps = {
  readonly initialState: ProductInfoInputGroupState &
    ProductBadgesCheckboxGroupState & {
      size: Product['size'];
      media: Product['media'];
    };
  readonly __controller: {
    handleSave: () => [ProductFormState, Blob | null] | null;
  };
};

export const ProductForm: React.FC<ProductFormProps> = ({
  initialState,
  __controller,
}) => {
  const { title, ingredients, description, size, badges } = initialState;

  const [activeTabKey, setActiveTabKey] = React.useState(TabKey.Common);
  const {
    mediaSrc,
    error: photoUploadError,
    handleChange: handlePhotoUploadChange,
    getValidatedState: getPhotoUploadValidatedState,
  } = useProductMediaUpload({ mediaSrc: initialState.media.miniatureSrc });
  const {
    state: infoInputGroupState,
    errors: infoInputGroupErrors,
    handleChange: handleInfoInputGroupChange,
    getValidatedState: getInfoInputGroupValidatedState,
  } = useProductInfoInputGroup({
    initialState: {
      title,
      ingredients,
      description,
    },
  });
  const {
    state: badgesCheckboxGroupState,
    handleChange: handleBadgesCheckboxGroupState,
    getValidatedState: getBadgesCheckboxGroupState,
  } = useProductBadgesCheckboxGroup({
    initialState: {
      badges,
    },
  });
  const {
    state: sizeInputGroupState,
    errors: sizeInputGroupErrors,
    handleChange: handleSizeInputGroupChange,
    getValidatedState: getSizeInputGroupValidatedState,
  } = useProductSizeInputGroup({
    initialState: projectProductSizeToState(size),
  });

  React.useEffect(() => {
    __controller.handleSave = () => {
      const photo = getPhotoUploadValidatedState();
      const info = getInfoInputGroupValidatedState();
      const badges = getBadgesCheckboxGroupState();
      const size = getSizeInputGroupValidatedState();

      const isPhotoValid = Boolean(mediaSrc || photo);

      if (!info || !isPhotoValid) {
        setActiveTabKey(TabKey.Common);

        return null;
      }

      if (!badges || !size) {
        setActiveTabKey(TabKey.Characteristics);

        return null;
      }

      return [
        {
          ...info,
          badges,
          size,
        },
        photo,
      ];
    };
  }, [
    __controller,
    mediaSrc,
    getPhotoUploadValidatedState,
    getInfoInputGroupValidatedState,
    getBadgesCheckboxGroupState,
    getSizeInputGroupValidatedState,
  ]);

  return (
    <Tabs
      activeKey={activeTabKey}
      onChange={(key: string) => setActiveTabKey(key as TabKey)}
    >
      <Tabs.TabPane key={TabKey.Common} tab="Общая информация">
        <ProductMediaUpload
          mediaSrc={mediaSrc}
          error={photoUploadError}
          onChange={handlePhotoUploadChange}
        />
        <ProductInfoInputGroup
          state={infoInputGroupState}
          errors={infoInputGroupErrors}
          onChange={handleInfoInputGroupChange}
        />
      </Tabs.TabPane>
      <Tabs.TabPane key={TabKey.Characteristics} tab="Характеристики">
        <ProductBadgesCheckboxGroup
          state={badgesCheckboxGroupState}
          onChange={handleBadgesCheckboxGroupState}
        />
        <ProductSizeInputGroup
          state={sizeInputGroupState}
          errors={sizeInputGroupErrors}
          onChange={handleSizeInputGroupChange}
        />
      </Tabs.TabPane>
    </Tabs>
  );
};

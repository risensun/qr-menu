import { Typography, Form, Checkbox } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';
import { BadgeRedux } from 'src/modules/badge';
import { ProductBadgesCheckboxGroupState } from '../ProductSizeInputGroup/types';

export type ProductBadgesCheckboxGroupProps = {
  state: ProductBadgesCheckboxGroupState;
  onChange: (state: ProductBadgesCheckboxGroupState) => void;
};

export const ProductBadgesCheckboxGroup: React.FC<
  ProductBadgesCheckboxGroupProps
> = ({ state, onChange }) => {
  const { badges } = useSelector((state) => BadgeRedux.selectState(state));

  const badgesPlainList = badges.ids.map((id) => ({
    label: badges.entities[id]?.name as string,
    value: badges.entities[id]?.id as string,
  }));

  return (
    <>
      <Typography.Paragraph strong>Бейджики</Typography.Paragraph>
      <Form layout="vertical">
        <Form.Item name="badges" initialValue={state.badges}>
          <Checkbox.Group
            options={badgesPlainList}
            value={state.badges}
            onChange={(values) => {
              onChange({ badges: values as string[] });
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
};

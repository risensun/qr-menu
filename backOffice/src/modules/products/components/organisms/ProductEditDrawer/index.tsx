import React from 'react';
import { Drawer, Dropdown, Menu, message } from 'antd';
import { EllipsisOutlined } from '@ant-design/icons';
import { Product } from 'src/types/Product';
import { useProductForm } from 'src/modules/products/hooks';
import { ProductForm, ProductFormState } from 'src/modules/products/components';
import { MenuInfo } from 'rc-menu/lib/interface';
import { ProductService } from 'src/modules/products/ProductService';
import { displayErrorIfPossible } from 'src/utils';
import { Loader } from 'src/modules/common';

export type ProductEditDrawerProps = {
  readonly isOpened: boolean;
  readonly product: Product | null;
  readonly onClose: () => void;
  readonly onSuccess: () => void;
};

export const ProductEditDrawer: React.FC<ProductEditDrawerProps> = ({
  isOpened,
  product,
  onClose,
  onSuccess,
}) => {
  const [isLoading, setIsLoading] = React.useState(false);

  const handleSubmit = ([state, blob]: [ProductFormState, Blob | null]) => {
    if (!product) {
      return;
    }

    setIsLoading(true);
    ProductService.editProductById(product.id, state as Product, blob)
      .then(() => {
        onClose();
        onSuccess();
        message.success('Товар обновлен');
      })
      .catch(displayErrorIfPossible)
      .finally(() => {
        setIsLoading(false);
        onClose();
      });
  };

  const form = useProductForm({
    onSubmit: (state) => state && handleSubmit(state),
  });

  /** Handlers */
  const handleMenuClick = React.useCallback(
    (info: MenuInfo) => {
      switch (info.key) {
        case 'save':
          form.submit();
          break;

        case 'close':
          onClose();
          break;

        default:
          break;
      }
    },
    [form, onClose]
  );

  const menuItems = [
    {
      key: 'save',
      label: 'Сохранить',
    },
    {
      key: 'close',
      label: 'Отменить',
    },
  ];

  return (
    <Drawer
      className="qr-drawer"
      width={550}
      visible={isOpened}
      onClose={onClose}
      title={product?.title ? product.title : 'Новый товар'}
      extra={
        <Dropdown.Button
          type="text"
          overlay={<Menu onClick={handleMenuClick} items={menuItems} />}
          icon={<EllipsisOutlined />}
        />
      }
    >
      {product && (
        <Loader spinning={isLoading}>
          <ProductForm
            key={product.id}
            initialState={product}
            {...form.props}
          />
        </Loader>
      )}
    </Drawer>
  );
};

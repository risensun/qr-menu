import React from 'react';
import { Drawer, Dropdown, Menu, message } from 'antd';
import { EllipsisOutlined } from '@ant-design/icons';
import { useProductForm } from 'src/modules/products/hooks';
import { ProductForm, ProductFormState } from 'src/modules/products/components';
import { MenuInfo } from 'rc-menu/lib/interface';
import { ProductService } from 'src/modules/products/ProductService';
import { MenuService } from 'src/modules/menu';
import { Category } from 'src/types';
import { displayErrorIfPossible } from 'src/utils';
import { Loader } from 'src/modules/common';

export type ProductAddDrawerProps = {
  readonly categoryId: Category['id'] | null;
  readonly isOpened: boolean;
  readonly onClose: () => void;
  readonly onSuccess: () => void;
};

export const ProductAddDrawer: React.FC<ProductAddDrawerProps> = ({
  categoryId,
  isOpened,
  onClose,
  onSuccess,
}) => {
  const [isLoading, setIsLoading] = React.useState(false);

  const handleSubmit = React.useCallback(
    ([state, blob]: [ProductFormState, Blob | null]) => {
      if (!categoryId || !blob) {
        return;
      }

      setIsLoading(true);
      ProductService.createProduct(categoryId, state.badges, state, blob)
        .then(({ id }) => {
          MenuService.addProduct([id])
            .then(({ isSucceeded }) => {
              if (isSucceeded) {
                message.success('Товар успешно добавлен');
                onSuccess();
                return;
              }

              message.error('Что-то пошло не так, попробуйте снова');
            })
            .catch(displayErrorIfPossible)
            .finally(() => {
              setIsLoading(false);
              onClose();
            });
        })
        .catch(displayErrorIfPossible);
    },
    [categoryId, onClose, onSuccess]
  );

  const form = useProductForm({
    onSubmit: (state) => state && handleSubmit(state),
  });

  /** Handlers */
  const handleMenuClick = React.useCallback(
    (info: MenuInfo) => {
      switch (info.key) {
        case 'save':
          form.submit();
          break;

        case 'close':
          onClose();
          break;

        default:
          break;
      }
    },
    [form, onClose]
  );

  /** Markup helpers */
  const menuItems = [
    {
      key: 'save',
      label: 'Сохранить',
    },
    {
      key: 'close',
      label: 'Отменить',
    },
  ];

  return (
    <Drawer
      className="qr-drawer"
      width={550}
      visible={isOpened}
      onClose={onClose}
      title="Новый товар"
      extra={
        <Dropdown.Button
          type="text"
          overlay={<Menu onClick={handleMenuClick} items={menuItems} />}
          icon={<EllipsisOutlined />}
        />
      }
    >
      <Loader spinning={isLoading}>
        <ProductForm
          key={String(isOpened)}
          initialState={{
            title: '',
            ingredients: '',
            description: '',
            badges: [],
            size: {
              name: '',
              unit: '',
              options: [],
            },
            media: {
              miniatureSrc: '',
              gallery: [],
            },
          }}
          {...form.props}
        />
      </Loader>
    </Drawer>
  );
};

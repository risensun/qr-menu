import { Form, Input } from 'antd';
import React from 'react';

export type ProductInfoInputGroupState = {
  readonly title: string;
  readonly ingredients: string;
  readonly description: string;
};

export type ProductInfoInputGroupErrors = {
  [key in keyof ProductInfoInputGroupState]: string | null;
};

export type ProductInfoInputGroupProps = {
  readonly state: ProductInfoInputGroupState;
  readonly onChange: (state: ProductInfoInputGroupState) => void;
  readonly errors?: ProductInfoInputGroupErrors;
};

type InputConfig = {
  name: keyof ProductInfoInputGroupState;
  label: string;
  placeholder: string;
  isTextarea: boolean;
  rows?: number;
};

export const ProductInfoInputGroup: React.FC<ProductInfoInputGroupProps> = ({
  state,
  onChange,
  errors,
}) => {
  const inputs: InputConfig[] = [
    {
      name: 'title',
      label: 'Название',
      placeholder: 'Укажите навзвание',
      isTextarea: false,
    },
    {
      name: 'ingredients',
      label: 'Состав',
      placeholder: 'Укажите состав',
      isTextarea: true,
      rows: 2,
    },
    {
      name: 'description',
      label: 'Описание',
      placeholder: 'Укажите описание',
      isTextarea: true,
      rows: 3,
    },
  ];

  const getValidateStatus = React.useCallback(
    (name: keyof ProductInfoInputGroupState) => {
      if (!errors || !errors[name]) {
        return 'success';
      }

      return 'error';
    },
    [errors]
  );

  const getErrorMessage = React.useCallback(
    (name: keyof ProductInfoInputGroupState) => errors?.[name] || null,
    [errors]
  );

  const handleChange = React.useCallback(
    (name: keyof ProductInfoInputGroupState, value: string) => {
      onChange({
        ...state,
        [name]: value,
      });
    },
    [state, onChange]
  );

  return (
    <Form layout="vertical">
      {inputs.map(({ name, label, placeholder, isTextarea, rows }) => (
        <Form.Item
          key={name}
          label={label}
          validateStatus={getValidateStatus(name)}
          help={getErrorMessage(name)}
        >
          {isTextarea ? (
            <Input.TextArea
              value={state[name]}
              placeholder={placeholder}
              rows={rows}
              onChange={({ target }) => handleChange(name, target.value)}
            />
          ) : (
            <Input
              value={state[name]}
              placeholder={placeholder}
              onChange={({ target }) => handleChange(name, target.value)}
            />
          )}
        </Form.Item>
      ))}
    </Form>
  );
};

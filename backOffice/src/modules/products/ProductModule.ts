import { createCommonModule } from 'src/core/module/factory/createCommonModule';
import { ProductRedux } from './redux';

export const ProductModule = createCommonModule({
  getReducers() {
    return {
      [ProductRedux.sliceName]: ProductRedux.reducer,
    };
  },
});

import { Filter } from 'src/modules/menu/components';

export const shouldProductBeDisplayed = (
  currentFilter: Filter,
  isProductPublished: boolean
) => {
  if (
    currentFilter === Filter.ALL ||
    (currentFilter === Filter.HIDDEN && !isProductPublished) ||
    (currentFilter === Filter.PUBLISHED && isProductPublished)
  ) {
    return true;
  }
  return false;
};

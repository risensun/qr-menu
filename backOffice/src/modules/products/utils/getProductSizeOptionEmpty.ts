import { nanoid } from '@reduxjs/toolkit';
import { ProductSizeOptionInputGroupState } from '../components/organisms/ProductSizeInputGroup/types';

export function getProductSizeOptionEmpty(): ProductSizeOptionInputGroupState {
  return {
    key: nanoid(),
    name: '',
    value: -1,
    price: -1,
    kkal: -1,
    proteins: -1,
    fat: -1,
    carbohydrate: -1,
  };
}

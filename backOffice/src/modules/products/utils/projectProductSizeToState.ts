import { Product } from 'src/types/Product';
import { ProductSizeInputGroupState } from '../components/organisms/ProductSizeInputGroup/types';

export function projectProductSizeToState(
  size: Product['size']
): ProductSizeInputGroupState {
  const { name, unit, options } = size;

  const optionsState: ProductSizeInputGroupState['options'] = {
    entities: {},
    keys: [],
  };

  for (const option of options) {
    const {
      nutrients: { kkal, proteins, fat, carbohydrate },
      ...rest
    } = option;

    optionsState.entities[option.key] = {
      ...rest,
      kkal,
      proteins,
      fat,
      carbohydrate,
    };
    optionsState.keys.push(option.key);
  }

  return {
    name,
    unit,
    options: optionsState,
  };
}

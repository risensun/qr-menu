import { Product } from 'src/types/Product';
import { ProductSizeInputGroupState } from '../components/organisms/ProductSizeInputGroup/types';

export function normalizeProductSizeInputGroupState(
  state: ProductSizeInputGroupState
): Product['size'] {
  const {
    name,
    unit,
    options: { entities, keys },
  } = state;

  const size: Product['size'] = {
    name,
    unit,
    options: keys.map((key) => {
      const { name, value, price, kkal, proteins, fat, carbohydrate } =
        entities[key];

      return {
        key,
        name,
        value,
        price,
        nutrients: {
          kkal,
          proteins,
          fat,
          carbohydrate,
        },
        isActive: true,
      };
    }),
  };

  return size;
}

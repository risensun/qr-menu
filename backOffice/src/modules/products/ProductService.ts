import { Config } from 'src/Config';
import { Fetcher, Method } from 'src/helpers';
import { Product } from 'src/types/Product';
import { withAuth } from '../auth/utils';

export abstract class ProductService {
  protected static readonly fetcher = new Fetcher(Config.getApiUrl());

  //TODO: need to refactor product type in param
  public static async createProduct(
    categoryId: number,
    badges: string[],
    product: Omit<
      Product,
      | 'id'
      | 'menuIds'
      | 'createdAt'
      | 'tenantId'
      | 'categoryId'
      | 'options'
      | 'gallery'
      | 'badgeIds'
      | 'media'
    >,
    media: Blob
  ): Promise<Product> {
    const { entity } = await withAuth(() =>
      this.fetcher.request(Method.POST, {
        url: '/bo/products',
        body: {
          ...product,
          badges,
          categoryId,
        },
      })
    );

    await this.uploadMedia(entity.id, media);

    return entity;
  }

  public static async getProducts(): Promise<Product[]> {
    const { entities } = await withAuth(() =>
      this.fetcher.request(Method.GET, { url: '/bo/products' })
    );

    return entities;
  }

  public static async editProductById(
    id: number,
    product: Product,
    blob: Blob | null
  ): Promise<{ isSucceeded: boolean }> {
    const isSucceeded = await withAuth(() =>
      this.fetcher.request(Method.PATCH, {
        url: `/bo/products/${id}`,
        body: { ...product },
      })
    );

    if (blob) {
      await this.uploadMedia(id, blob);
    }

    return { isSucceeded };
  }

  public static async uploadMedia(
    id: Product['id'],
    media: Blob
  ): Promise<boolean> {
    const formData = new FormData();

    formData.append('media', media);

    const { isSucceeded } = await withAuth(() =>
      this.fetcher.request(Method.PATCH, {
        url: `/bo/products/${id}/media`,
        body: formData,
        isFormData: true,
      })
    );

    return isSucceeded;
  }

  public static async deleteProductById(
    id: number
  ): Promise<{ isSucceeded: boolean }> {
    const { data } = await withAuth(() =>
      this.fetcher.request(Method.DELETE, { url: `/bo/products/${id}` })
    );

    return { isSucceeded: data.isSucceeded };
  }

  // TODO: move to menu service
  public static async changeProductsVisibilityById(
    productIds: number[],
    isPublished: boolean
  ): Promise<{ isSucceeded: boolean }> {
    const { isSucceeded } = await withAuth(() =>
      this.fetcher.request(Method.PATCH, {
        url: `/bo/menus/${Config.getMenuId()}/products/publishing`,
        body: {
          productIds,
          isPublished,
        },
      })
    );
    return isSucceeded;
  }
}

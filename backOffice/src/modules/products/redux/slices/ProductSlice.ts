import {
  createEntityAdapter,
  createSlice,
  EntityState,
  PayloadAction,
} from '@reduxjs/toolkit';
import { Product } from 'src/types/Product';

export type ProductSliceState = {
  products: EntityState<Product>;
  isLoading: boolean;
};

const entityAdapter = createEntityAdapter<Product>();

const initialState: ProductSliceState = {
  products: entityAdapter.getInitialState(),
  isLoading: false,
};

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    setProducts(state, action: PayloadAction<{ products: Product[] }>) {
      const { products } = action.payload;

      entityAdapter.addMany(state.products, products);
    },
    setIsLoading(state, action: PayloadAction<{ value: boolean }>) {
      const { value } = action.payload;

      state.isLoading = value;
    },
  },
});

export const productSelectors = entityAdapter.getSelectors();
export const selectProductState = (state: any): ProductSliceState =>
  state?.[productSlice.name] || initialState;

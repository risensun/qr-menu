import {
  productSelectors,
  productSlice,
  selectProductState,
} from './slices/ProductSlice';

export const ProductRedux = {
  sliceName: productSlice.name,
  reducer: productSlice.reducer,
  actions: productSlice.actions,
  selectors: productSelectors,
  selectState: selectProductState,
};

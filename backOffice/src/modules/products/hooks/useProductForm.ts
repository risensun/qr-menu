import React from 'react';
import { ProductFormState } from '../components';

export type UseProductFormParams = {
  onSubmit: (state: [ProductFormState, Blob | null] | null) => void;
};

export const useProductForm = ({ onSubmit }: UseProductFormParams) => {
  const controllerRef = React.useRef({
    handleSave: (): [ProductFormState, Blob | null] | null => null,
  });

  const submit = React.useCallback(() => {
    onSubmit(controllerRef.current.handleSave());
  }, [onSubmit]);

  return {
    submit,
    props: {
      __controller: controllerRef.current,
    },
  };
};

import React from 'react';
import { ProductBadgesCheckboxGroupState } from '../components/organisms/ProductSizeInputGroup/types';

export type UseProductBadgesCheckboxGroupParams = {
  initialState: ProductBadgesCheckboxGroupState;
};

export const useProductBadgesCheckboxGroup = ({
  initialState,
}: UseProductBadgesCheckboxGroupParams) => {
  const [state, setState] = React.useState(initialState);

  const handleChange = React.useCallback(
    (state: ProductBadgesCheckboxGroupState) => {
      setState(state);
    },
    []
  );

  const getValidatedState = () => state.badges;

  return {
    handleChange,
    getValidatedState,
    state,
  };
};

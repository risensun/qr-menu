import React from 'react';
import { Category } from 'src/types';

export const useProductAddDrawer = () => {
  const [isOpened, setIsOpened] = React.useState(false);
  const categoryIdRef = React.useRef<Category['id'] | null>(null);

  const open = React.useCallback((categoryId: Category['id']) => {
    categoryIdRef.current = categoryId;
    setIsOpened(true);
  }, []);

  const close = React.useCallback(() => {
    setIsOpened(false);
  }, []);

  return {
    open,
    close,
    props: {
      isOpened,
      categoryId: categoryIdRef.current,
      onClose: close,
    },
  };
};

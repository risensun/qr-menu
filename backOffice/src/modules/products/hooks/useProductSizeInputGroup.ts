import React from 'react';
import { ValidationMessages } from 'src/constants';
import * as Yup from 'yup';
import {
  ProductSizeInputGroupErrors,
  ProductSizeInputGroupState,
} from '../components/organisms/ProductSizeInputGroup/types';
import {
  getProductSizeOptionEmpty,
  normalizeProductSizeInputGroupState,
} from '../utils';

const getInitialState = (
  initialState: ProductSizeInputGroupState
): ProductSizeInputGroupState => {
  if (initialState.options.keys.length > 0) {
    return initialState;
  }

  const emptySizeOption = getProductSizeOptionEmpty();

  return {
    ...initialState,
    options: {
      entities: {
        [emptySizeOption.key]: emptySizeOption,
      },
      keys: [emptySizeOption.key],
    },
  };
};

const getInitialErrors = (): ProductSizeInputGroupErrors => ({
  name: null,
  unit: null,
  options: {},
});

export type UseProductSizeInputGroupParams = {
  initialState: ProductSizeInputGroupState;
};

export const useProductSizeInputGroup = ({
  initialState,
}: UseProductSizeInputGroupParams) => {
  const [state, setState] = React.useState(getInitialState(initialState));
  const [errors, setErrors] = React.useState<ProductSizeInputGroupErrors>(
    getInitialErrors()
  );

  const handleChange = React.useCallback(
    (state: ProductSizeInputGroupState) => {
      setState((prevState) => ({
        ...prevState,
        ...state,
      }));
      setErrors(getInitialErrors());
    },
    []
  );

  const getValidatedState = React.useCallback(() => {
    const commonSchema = Yup.object({
      name: Yup.string().required(ValidationMessages.REQUIRED),
      unit: Yup.string().required(ValidationMessages.REQUIRED),
    }).required();

    const optionsSchema = Yup.object({
      keys: Yup.array(Yup.string().required()).required(),
      entities: Yup.object({
        ...state.options.keys.reduce(
          (acc, key) => ({
            ...acc,
            [key]: Yup.object({
              key: Yup.string().required(),
              name: Yup.string().required(ValidationMessages.REQUIRED),
              value: Yup.number()
                .positive(ValidationMessages.MUST_BE_POSITIVE)
                .required(ValidationMessages.REQUIRED),
              price: Yup.number()
                .positive(ValidationMessages.MUST_BE_POSITIVE)
                .required(ValidationMessages.REQUIRED),
              kkal: Yup.number()
                .positive(ValidationMessages.MUST_BE_POSITIVE)
                .required(ValidationMessages.REQUIRED),
              proteins: Yup.number()
                .positive(ValidationMessages.MUST_BE_POSITIVE)
                .required(ValidationMessages.REQUIRED),
              fat: Yup.number()
                .positive(ValidationMessages.MUST_BE_POSITIVE)
                .required(ValidationMessages.REQUIRED),
              carbohydrate: Yup.number()
                .positive(ValidationMessages.MUST_BE_POSITIVE)
                .required(ValidationMessages.REQUIRED),
            }),
          }),
          {} as any
        ),
      }),
    }).required();

    try {
      const { name, unit } = commonSchema.validateSync(state);

      try {
        const { entities, keys } = optionsSchema.validateSync(state.options);

        return normalizeProductSizeInputGroupState({
          name,
          unit,
          options: {
            entities,
            keys,
          },
        });
      } catch (error) {
        if (error instanceof Yup.ValidationError) {
          const path = error.path || '';
          const message = error.message;

          const [, key, field] = path.split('.');

          setErrors((prevErrors) => ({
            ...prevErrors,
            options: {
              ...prevErrors.options,
              [`${key}.${field}`]: message,
            },
          }));

          return null;
        }

        throw error;
      }
    } catch (error) {
      if (error instanceof Yup.ValidationError) {
        const path = error.path || '';
        const message = error.message;

        setErrors((prevErrors) => ({
          ...prevErrors,
          [path]: message,
        }));

        return null;
      }

      throw error;
    }
  }, [state]);

  return {
    getValidatedState,
    handleChange,
    errors,
    state,
  };
};

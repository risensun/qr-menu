import React from 'react';
import { Product } from 'src/types/Product';

export const useProductEditDrawer = () => {
  const [isOpened, setIsOpened] = React.useState(false);
  const productRef = React.useRef<Product | null>(null);

  const open = React.useCallback((product: Product) => {
    productRef.current = product;
    setIsOpened(true);
  }, []);

  const close = React.useCallback(() => {
    setIsOpened(false);
  }, []);

  return {
    open,
    close,
    props: {
      isOpened,
      product: productRef.current,
      onClose: close,
    },
  };
};

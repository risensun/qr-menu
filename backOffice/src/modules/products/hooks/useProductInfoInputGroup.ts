import React from 'react';
import { ValidationMessages } from 'src/constants';
import * as Yup from 'yup';
import {
  ProductInfoInputGroupState,
  ProductInfoInputGroupErrors,
} from '../components';

const getInitialErrors = (initialState: ProductInfoInputGroupState) =>
  Object.keys(initialState).reduce<ProductInfoInputGroupErrors>(
    (acc, curr) => ({
      ...acc,
      [curr]: null,
    }),
    {} as any
  );

const schema = Yup.object({
  title: Yup.string().required(ValidationMessages.REQUIRED),
  ingredients: Yup.string().required(ValidationMessages.REQUIRED),
  description: Yup.string().required(ValidationMessages.REQUIRED),
}).required();

export type UseProductInfoInputGroupProps = {
  initialState: ProductInfoInputGroupState;
};

export const useProductInfoInputGroup = ({
  initialState,
}: UseProductInfoInputGroupProps) => {
  const [state, setState] = React.useState({ ...initialState });
  const [errors, setErrors] = React.useState(getInitialErrors(initialState));

  /** Handlers */
  const handleChange = React.useCallback(
    (state: ProductInfoInputGroupState) => {
      setState((prevState) => ({
        ...prevState,
        ...state,
      }));
    },
    []
  );

  const getValidatedState = React.useCallback(() => {
    try {
      const fixedState = schema.validateSync(state, { strict: true });

      setErrors(getInitialErrors(fixedState));

      return fixedState;
    } catch (error) {
      if (error instanceof Yup.ValidationError) {
        const path = error.path || '';
        const message = error.message;

        setErrors((prevErrors) => ({
          ...prevErrors,
          [path]: message,
        }));

        return null;
      }

      throw error;
    }
  }, [state]);

  return {
    getValidatedState,
    handleChange,
    errors,
    state,
  };
};

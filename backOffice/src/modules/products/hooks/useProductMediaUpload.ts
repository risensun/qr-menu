import React from 'react';
import { ValidationMessages } from 'src/constants';

export type UseProductMediaUploadParams = {
  readonly mediaSrc?: string;
};

export const useProductMediaUpload = ({
  mediaSrc,
}: UseProductMediaUploadParams) => {
  const [error, setError] = React.useState<string | null>(null);
  // TODO: parse blob from url if url passed
  const blobRef = React.useRef<Blob | null>(null);

  const handleChange = React.useCallback((file: Blob) => {
    blobRef.current = file;

    setError(null);
  }, []);

  const getValidatedState = React.useCallback(() => {
    const blob = blobRef.current;

    if (!blob && !mediaSrc) {
      setError(ValidationMessages.REQUIRED);

      return null;
    }

    return blob;
  }, [mediaSrc]);

  return {
    error,
    mediaSrc,
    handleChange,
    getValidatedState,
  };
};

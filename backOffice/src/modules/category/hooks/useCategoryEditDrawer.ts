import React from 'react';
import { Category } from 'src/types/Category';

export const useCategoryEditDrawer = () => {
  const [isOpened, setIsOpened] = React.useState<boolean>(false);
  const drawerPropsRef = React.useRef<{
    category: Category | null;
    parentCategoryId: Category['id'] | null;
  }>({
    category: null,
    parentCategoryId: null,
  });

  const open = React.useCallback(
    (
      category: Category | undefined,
      parentCategoryId: Category['id'] | null
    ) => {
      if (!category) {
        return;
      }

      drawerPropsRef.current = {
        category,
        parentCategoryId,
      };
      setIsOpened(true);
    },
    []
  );

  const close = React.useCallback(() => {
    setIsOpened(false);
  }, []);

  return {
    open,
    close,
    props: {
      ...drawerPropsRef.current,
      isOpened,
      onClose: close,
    },
  };
};

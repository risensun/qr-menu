import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { displayErrorIfPossible } from 'src/utils';
import { CategoryService } from '../CategoryService';
import { CategoryRedux } from '../redux';

export const useCategoryCatalogue = () => {
  const dispatch = useDispatch();
  const { catalogue } = useSelector(CategoryRedux.selectState);

  const fetchCatalogue = React.useCallback(() => {
    dispatch(CategoryRedux.actions.setCatalogueIsLoading({ value: true }));

    CategoryService.getCatalogue()
      .then((catalogue) => {
        dispatch(CategoryRedux.actions.setCatalogue({ catalogue }));
      })
      .catch(displayErrorIfPossible)
      .finally(() =>
        dispatch(CategoryRedux.actions.setCatalogueIsLoading({ value: false }))
      );
  }, [dispatch]);

  return {
    catalogue,
    fetchCatalogue,
  };
};

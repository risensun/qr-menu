import React from 'react';
import { Category } from 'src/types/Category';

export const useCategoryAddDrawer = () => {
  const [isOpened, setIsOpened] = React.useState<boolean>(false);
  const drawerPropsRef = React.useRef<{
    parentCategoryId: Category['id'] | null;
  }>({
    parentCategoryId: null,
  });

  const open = React.useCallback((parentCategoryId: Category['id'] | null) => {
    drawerPropsRef.current = {
      parentCategoryId,
    };
    setIsOpened(true);
  }, []);

  const close = React.useCallback(() => {
    setIsOpened(false);
  }, []);

  return {
    open,
    close,
    props: {
      ...drawerPropsRef.current,
      isOpened,
      onClose: close,
    },
  };
};

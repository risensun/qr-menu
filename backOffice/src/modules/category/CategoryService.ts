import { Config } from 'src/Config';
import { Fetcher, Method } from 'src/helpers';
import { Catalogue } from 'src/types/Catalogue';
import { Category } from 'src/types/Category';
import { withAuth } from '../auth/utils';

export abstract class CategoryService {
  protected static readonly fetcher = new Fetcher(Config.getApiUrl());

  public static async getCategories(): Promise<Category[]> {
    const { entities } = await withAuth(() =>
      this.fetcher.request(Method.GET, { url: '/bo/categories' })
    );

    return entities;
  }

  public static async getCatalogue(): Promise<Catalogue> {
    const { entity } = await withAuth(() =>
      this.fetcher.request(Method.GET, { url: '/bo/categories/catalogue' })
    );

    return entity;
  }

  public static async deleteCategoryById(
    id: number
  ): Promise<{ isSucceeded: boolean }> {
    const { isSucceeded } = await withAuth(() =>
      this.fetcher.request(Method.DELETE, { url: `/bo/categories/${id}` })
    );

    return { isSucceeded };
  }

  public static async renameCategoryById(
    id: number,
    title: string
  ): Promise<{ isSucceeded: boolean }> {
    const { isSucceeded } = await withAuth(() =>
      this.fetcher.request(Method.PATCH, {
        url: `/bo/categories/${id}`,
        body: { title },
      })
    );

    return { isSucceeded };
  }

  public static async createCategory(
    title: string,
    parentCategoryId?: number | null
  ): Promise<{ entity: Category }> {
    const { entity } = await withAuth(() =>
      this.fetcher.request(Method.POST, {
        url: '/bo/categories',
        body: { title, parentCategoryId },
      })
    );

    return { entity };
  }
}

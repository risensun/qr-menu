import { Drawer } from 'antd';
import React from 'react';
import { CategoryService } from 'src/modules/category';
import { useCategoryCatalogue } from 'src/modules/category/hooks';
import { Category } from 'src/types/Category';
import { displayErrorIfPossible } from 'src/utils';
import { CategoryForm, CategoryFormState } from '../CategoryForm';

export type CategoryAddDrawerProps = {
  readonly isOpened: boolean;
  readonly parentCategoryId: Category['id'] | null;
  readonly onClose: () => void;
};

export const CategoryAddDrawer: React.FC<CategoryAddDrawerProps> = ({
  isOpened,
  parentCategoryId,
  onClose,
}) => {
  const { fetchCatalogue } = useCategoryCatalogue();

  const title = parentCategoryId ? 'Новая категория' : 'Новый раздел';

  const handleSubmitCategory = async (values: CategoryFormState) => {
    const { title, parentCategoryId } = values;
    CategoryService.createCategory(title, parentCategoryId || undefined)
      .then(fetchCatalogue)
      .catch(displayErrorIfPossible)
      .finally(onClose);

    return true;
  };

  return (
    <Drawer visible={isOpened} title={title} onClose={onClose}>
      <CategoryForm
        initialState={{
          parentCategoryId,
          title: '',
        }}
        onSubmit={handleSubmitCategory}
      />
    </Drawer>
  );
};

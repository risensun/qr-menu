import { DeleteOutlined } from '@ant-design/icons';
import { nanoid } from '@reduxjs/toolkit';
import { Button, Drawer, message, Popconfirm } from 'antd';
import React from 'react';
import { CategoryService } from 'src/modules/category';
import { useCategoryCatalogue } from 'src/modules/category/hooks';
import { Category } from 'src/types/Category';
import { displayErrorIfPossible } from 'src/utils';
import { CategoryForm, CategoryFormState } from '../CategoryForm';

export type CategoryEditDrawerProps = {
  isOpened: boolean;
  category: Category | null;
  parentCategoryId: Category['id'] | null;
  onClose: () => void;
};

export const CategoryEditDrawer: React.FC<CategoryEditDrawerProps> = ({
  isOpened,
  category,
  onClose,
}) => {
  const { fetchCatalogue } = useCategoryCatalogue();

  const [isDeleteButtonLoading, setIsDeleteButtonLoading] =
    React.useState(false);

  /** Handlers */
  const handleEditCategory = async ({ title }: CategoryFormState) => {
    if (category) {
      CategoryService.renameCategoryById(category.id, title)
        .then(() => {
          fetchCatalogue();
          message.success('Категория обновлена');
        })
        .catch(displayErrorIfPossible)
        .finally(onClose);
    }
    return true;
  };

  const handleDeleteCategory = async () => {
    if (!category) {
      return;
    }

    setIsDeleteButtonLoading(true);
    CategoryService.deleteCategoryById(category.id)
      .then(fetchCatalogue)
      .catch(displayErrorIfPossible)
      .finally(() => {
        setIsDeleteButtonLoading(false);
        onClose();
      });

    return true;
  };

  return (
    <Drawer
      visible={isOpened}
      onClose={onClose}
      title={category?.title}
      extra={
        <Popconfirm
          title="Вы уверены? Это действие нельзя отменить"
          okText="Да"
          cancelText="Отмена"
          okButtonProps={{ loading: isDeleteButtonLoading }}
          onConfirm={handleDeleteCategory}
          placement="leftTop"
        >
          <Button danger icon={<DeleteOutlined />} />
        </Popconfirm>
      }
    >
      {category && (
        <CategoryForm
          key={nanoid()}
          initialState={{ title: category.title }}
          onSubmit={handleEditCategory}
        />
      )}
    </Drawer>
  );
};

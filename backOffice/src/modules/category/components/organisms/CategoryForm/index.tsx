import React from 'react';
import { Button, Form, FormInstance, Input } from 'antd';
import { Category } from 'src/types/Category';

export type CategoryFormState = {
  readonly title: string;
  readonly parentCategoryId?: Category['id'] | null;
};

export type CategoryFormProps = {
  readonly initialState: CategoryFormState;
  readonly onSubmit: (state: CategoryFormState) => Promise<boolean>;
};

export const CategoryForm: React.FC<CategoryFormProps> = ({
  initialState,
  onSubmit,
}) => {
  const formRef = React.useRef<FormInstance>(null);
  const [isLoading, setIsLoading] = React.useState(false);

  const inputs = [
    {
      name: 'title',
      label: 'Название',
      placeholder: 'Лимонады',
    },
  ];

  return (
    <Form
      ref={formRef}
      layout="vertical"
      onFinish={(values) => {
        setIsLoading(true);

        onSubmit({ ...initialState, ...values })
          .then(() => formRef.current?.resetFields())
          .finally(() => setIsLoading(false));
      }}
    >
      {inputs.map(({ name, label, placeholder }) => (
        <Form.Item
          key={name}
          name={name}
          label={label}
          rules={[{ required: true, message: 'Введите название' }]}
          initialValue={initialState[name as keyof CategoryFormState]}
        >
          <Input placeholder={placeholder} />
        </Form.Item>
      ))}
      <Form.Item>
        <Button htmlType="submit" type="primary" loading={isLoading}>
          Сохранить
        </Button>
      </Form.Item>
    </Form>
  );
};

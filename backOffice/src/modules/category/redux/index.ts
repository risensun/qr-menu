import {
  categorySelectors,
  categorySlice,
  selectCategoryState,
} from './slices/CategorySlice';

export const CategoryRedux = {
  sliceName: categorySlice.name,
  reducer: categorySlice.reducer,
  actions: categorySlice.actions,
  selectors: categorySelectors,
  selectState: selectCategoryState,
};

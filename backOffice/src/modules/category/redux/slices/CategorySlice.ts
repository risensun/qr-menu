import {
  createEntityAdapter,
  createSlice,
  EntityState,
  PayloadAction,
} from '@reduxjs/toolkit';
import { Catalogue } from 'src/types';
import { Category } from 'src/types/Category';

export type CategorySliceState = {
  catalogue: {
    state: Catalogue;
    isLoading: boolean;
  };

  categories: {
    state: EntityState<Category>;
    isLoading: boolean;
  };
};

const entityAdapter = createEntityAdapter<Category>();

const initialState: CategorySliceState = {
  catalogue: {
    state: {
      ids: [],
      rootIds: [],
      categories: {},
    },
    isLoading: false,
  },

  categories: {
    state: entityAdapter.getInitialState(),
    isLoading: false,
  },
};

export const categorySlice = createSlice({
  name: 'category',
  initialState,
  reducers: {
    /**
     * Set catalogue
     * @param state
     * @param action
     */
    setCatalogue(state, action: PayloadAction<{ catalogue: Catalogue }>) {
      const { catalogue } = action.payload;

      state.catalogue.state = catalogue;
    },

    /**
     * Set catalogue is loading
     * @param state
     * @param action
     */
    setCatalogueIsLoading(state, action: PayloadAction<{ value: boolean }>) {
      const { value } = action.payload;

      state.catalogue.isLoading = value;
    },

    /**
     * Set categories
     * @param state
     * @param action
     */
    setCategories(state, action: PayloadAction<{ categories: Category[] }>) {
      const { categories } = action.payload;

      entityAdapter.addMany(state.categories.state, categories);
    },

    /**
     * Set categories is loading
     * @param state
     * @param action
     */
    setCategoriesIsLoading(state, action: PayloadAction<{ value: boolean }>) {
      const { value } = action.payload;

      state.categories.isLoading = value;
    },
  },
});

export const categorySelectors = entityAdapter.getSelectors();
export const selectCategoryState = (state: any): CategorySliceState =>
  state?.[categorySlice.name] || initialState;

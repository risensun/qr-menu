import { createCommonModule } from 'src/core/module/factory/createCommonModule';
import { CategoryRedux } from './redux';

export const CategoryModule = createCommonModule({
  getReducers() {
    return {
      [CategoryRedux.sliceName]: CategoryRedux.reducer,
    };
  },
});

import { createCommonModule } from 'src/core/module/factory/createCommonModule';
import { BadgeRedux } from './redux';

export * from './redux';
export * from './BadgeService';

export const BadgeModule = createCommonModule({
  getReducers() {
    return {
      [BadgeRedux.sliceName]: BadgeRedux.reducer,
    };
  },
});

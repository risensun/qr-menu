import { Config } from 'src/Config';
import { Fetcher, Method } from 'src/helpers';
import { Badge } from 'src/types/Badge';
import { withAuth } from '../auth/utils';

export abstract class BadgeService {
  protected static readonly fetcher = new Fetcher(Config.getApiUrl());

  public static async getBadges(): Promise<Badge[]> {
    const { entities } = await withAuth(() =>
      this.fetcher.request(Method.GET, { url: '/bo/badges/' })
    );

    return entities;
  }
}

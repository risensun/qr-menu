import {
  badgeSlice,
  badgeSelectors,
  selectBadgeState,
} from './slices/BadgeSlice';

export const BadgeRedux = {
  sliceName: badgeSlice.name,
  reducer: badgeSlice.reducer,
  actions: badgeSlice.actions,
  selectors: badgeSelectors,
  selectState: selectBadgeState,
};

import {
  createEntityAdapter,
  createSlice,
  EntityState,
  PayloadAction,
} from '@reduxjs/toolkit';
import { Badge } from 'src/types/Badge';

export type BadgeSliceState = {
  badges: EntityState<Badge>;
  isLoading: boolean;
};

const entityAdapter = createEntityAdapter<Badge>();

const initialState: BadgeSliceState = {
  badges: entityAdapter.getInitialState(),
  isLoading: false,
};

export const badgeSlice = createSlice({
  name: 'badge',
  initialState,
  reducers: {
    setBadges(state, action: PayloadAction<{ badges: Badge[] }>) {
      const { badges } = action.payload;

      entityAdapter.addMany(state.badges, badges);
    },
    setIsLoading(state, action: PayloadAction<{ value: boolean }>) {
      const { value } = action.payload;

      state.isLoading = value;
    },
  },
});

export const badgeSelectors = entityAdapter.getSelectors();
export const selectBadgeState = (state: any): BadgeSliceState =>
  state?.[badgeSlice.name] || initialState;

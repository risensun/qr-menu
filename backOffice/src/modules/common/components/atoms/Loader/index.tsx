import { LoadingOutlined } from '@ant-design/icons';
import { Spin, SpinProps } from 'antd';

export type LoaderProps = Omit<SpinProps, 'indicator'>;

export const Loader: React.FC<LoaderProps> = (props) => (
  <Spin {...props} indicator={<LoadingOutlined />} />
);

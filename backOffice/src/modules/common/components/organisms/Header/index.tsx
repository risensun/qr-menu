/**
 * Separate contents, and move Header to templates
 */

import React from 'react';
import { Button, Select, Space, Typography } from 'antd';
import styles from './header.module.scss';
import { LogoutOutlined } from '@ant-design/icons';
import Avatar from 'antd/lib/avatar/avatar';
import { AuthService } from 'src/modules/auth/AuthService';

export const Header: React.FC = () => {
  const [searchValue, setSearchValue] = React.useState('');
  const [searchResults, setSearchResults] = React.useState<
    { id: number; title: string }[]
  >([]);

  // const handleSearch = (value: string) => {
  //   if (value) {
  //     const filteredResults = searchMock.filter((product) =>
  //       product.title.includes(value)
  //     );
  //     setSearchResults(filteredResults);
  //   } else {
  //     setSearchResults([]);
  //   }
  // };

  // const handleSelect = (value: number) => {
  //   const result = searchMock.find((item) => item.id === value);
  //   alert(result ? result.title : 'Не найдено');
  //   setSearchValue('');
  // };

  const options = searchResults.map((item) => (
    <Select.Option key={item.id}>{item.title}</Select.Option>
  ));

  return (
    <Space direction="horizontal" className={styles.root}>
      <Space direction="horizontal" size={16}>
        <Avatar shape="square" src="/static/images/logo.png" />
        {/* <Select
          showSearch
          value={searchValue}
          placeholder="Введите название товара"
          defaultActiveFirstOption={false}
          showArrow={false}
          filterOption={false}
          onChange={(value) => setSearchValue(value)}
          notFoundContent={null}
          style={{ width: '391px' }}
          onSearch={handleSearch}
          onSelect={handleSelect as any}
        >
          {options}
        </Select> 
          TODO: add search functionality
        */}
      </Space>
      <Space direction="horizontal" size={8}>
        <Avatar src="/static/images/default.png" />
        <Typography.Text strong>Администратор</Typography.Text>
        <Button
          type="link"
          icon={<LogoutOutlined />}
          style={{ color: 'black' }}
          onClick={() => AuthService.logout()}
        />
      </Space>
    </Space>
  );
};

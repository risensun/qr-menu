import { Layout } from 'antd';
import React from 'react';

export type PageProps = {
  readonly children: React.ReactNode;
  readonly layoutClassName?: string;
};

export const Page: React.FC<PageProps> = ({ children, layoutClassName }) => (
  <main className="qr-page">
    <Layout className={layoutClassName}>{children}</Layout>
  </main>
);

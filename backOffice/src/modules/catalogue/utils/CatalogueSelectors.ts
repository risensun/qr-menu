import { Catalogue } from 'src/types/Catalogue';
import { Category } from 'src/types/Category';

export const CatalogueSelectors = {
  selectCategoryById: (state: Catalogue, id: Category['id']) =>
    state.categories[id],

  selectCategoriesByIds: (state: Catalogue, ids: Category['id'][]) =>
    ids.map((id) => state.categories[id]),
};

import React from 'react';
import { Catalogue, Category } from 'src/types';
import { ActiveCategoryKeys } from '../components/organisms/CatalogueTree/types';
import { CatalogueGraph } from '../helpers';

export type UseCatalogueTreeParams = {
  readonly catalogue: Catalogue | null;
};

export const useCatalogueTree = ({ catalogue }: UseCatalogueTreeParams) => {
  const [activeCategoryKeys, setActiveCategoryKeys] =
    React.useState<ActiveCategoryKeys>({
      levels: {},
      final: undefined,
    });

  const graph = React.useMemo(
    () => (catalogue ? new CatalogueGraph(catalogue) : null),
    [catalogue]
  );

  const handleCategoryChange = React.useCallback(
    (id: Category['id']) => {
      if (!graph) {
        return;
      }

      const finalActiveCategoryKey = id;
      const pathToRoot = graph.getPathToRoot(finalActiveCategoryKey);

      const nextActiveCategoryKeys: ActiveCategoryKeys = {
        final: finalActiveCategoryKey,
        levels: pathToRoot.reduce(
          (acc, curr, index) => ({
            ...acc,
            [pathToRoot.length - 1 - index]: curr,
          }),
          {}
        ),
      };

      setActiveCategoryKeys(nextActiveCategoryKeys);
    },
    [graph]
  );

  React.useEffect(() => {
    if (!activeCategoryKeys.final && catalogue) {
      handleCategoryChange(catalogue.rootIds[0]);
    }
  }, [handleCategoryChange, catalogue, activeCategoryKeys]);

  return {
    activeCategoryKeys,
    handleCategoryChange,
  };
};

import { Catalogue, Category } from 'src/types';
import { CatalogueSelectors } from '../utils';

export class CatalogueGraph {
  protected readonly idToParentIdMap: {
    [key: Category['id']]: Category['id'] | null;
  };

  public constructor(protected readonly catalogue: Catalogue) {
    this.idToParentIdMap = {};

    this.init();
  }

  public getPathToRoot(id: Category['id']): Category['id'][] {
    const path: Category['id'][] = [id];
    let parentId = this.idToParentIdMap[id];

    while (parentId) {
      path.push(parentId);

      parentId = this.idToParentIdMap[parentId];
    }

    return path;
  }

  public getDeepestChildId(id: Category['id']): Category['id'] | undefined {
    const category = CatalogueSelectors.selectCategoryById(this.catalogue, id);

    if (!category) {
      return;
    }

    if (category.children.length > 0) {
      return this.getDeepestChildId(category.children[0]);
    }

    return category.id;
  }

  protected init(): void {
    const queue: Category['id'][] = [...this.catalogue.rootIds];

    while (queue.length > 0) {
      const id = queue.shift();

      if (!id) {
        break;
      }

      const category = CatalogueSelectors.selectCategoryById(
        this.catalogue,
        id
      );

      if (category) {
        for (const childId of category.children) {
          queue.push(childId);

          this.idToParentIdMap[childId] = category.id;
        }
      }
    }
  }
}

import React from 'react';
import { Space, Tabs, Typography } from 'antd';
import { EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Catalogue } from 'src/types/Catalogue';
import { Category as CategoryType } from 'src/types/Category';
import { CategoryTab } from '../CategoryTab';
import { CatalogueSelectors } from 'src/modules/catalogue/utils';
import { ActiveCategoryKeys } from '../../types';

export type CategoryProps = {
  readonly catalogue: Catalogue;
  readonly activeKeys: ActiveCategoryKeys;
  readonly category: CategoryType;
  readonly level: number;
  readonly onEdit: (
    action: 'add' | 'remove',
    id: CategoryType['id'],
    parentCategoryId: CategoryType['id'] | null
  ) => void;
  readonly onChange: (key: string) => void;
  readonly shouldDisplayTitle?: boolean;
};

export const Category: React.FC<CategoryProps> = ({
  catalogue,
  activeKeys,
  category,
  level,
  onEdit,
  onChange,
  shouldDisplayTitle,
}) => {
  /** Markup helpers */
  const children = React.useMemo(
    () =>
      category.children.length > 0
        ? CatalogueSelectors.selectCategoriesByIds(
            catalogue,
            category.children
          ).filter((category): category is CategoryType => Boolean(category))
        : [],
    [catalogue, category]
  );
  const childrenLevel = level + 1;
  const activeKey = activeKeys.levels[childrenLevel]
    ? String(activeKeys.levels[childrenLevel])
    : '-1';

  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      {shouldDisplayTitle && (
        <Typography.Title level={3}>{category.title}</Typography.Title>
      )}
      {(level !== 1 || children.length > 0) && (
        <Tabs
          type="editable-card"
          activeKey={activeKey}
          addIcon={
            <CategoryTab
              title="Добавить категорию"
              addonBefore={<PlusOutlined />}
            />
          }
          onEdit={(event, action) =>
            onEdit(action, parseInt(event.toString()), category.id)
          }
        >
          {children.map((category) => (
            <Tabs.TabPane
              key={category.id}
              tab={
                <CategoryTab
                  title={category.title}
                  isActive={activeKey === String(category.id)}
                  onClick={() => onChange(String(category.id))}
                />
              }
              closeIcon={<EditOutlined />}
            >
              <Category
                catalogue={catalogue}
                activeKeys={activeKeys}
                category={category}
                level={childrenLevel}
                onEdit={onEdit}
                onChange={onChange}
                shouldDisplayTitle={shouldDisplayTitle}
              />
            </Tabs.TabPane>
          ))}
        </Tabs>
      )}
    </Space>
  );
};

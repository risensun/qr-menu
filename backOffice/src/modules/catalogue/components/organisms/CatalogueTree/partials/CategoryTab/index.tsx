import React from 'react';
import { Space, Typography } from 'antd';

export type CategoryTabProps = {
  readonly title: string;
  readonly isActive?: boolean;
  readonly addonBefore?: React.ReactNode;
  readonly onClick?: () => void;
};

export const CategoryTab: React.FC<CategoryTabProps> = ({
  title,
  isActive = false,
  addonBefore = null,
  onClick,
}) => (
  <Space direction="horizontal" style={{ padding: 2 }} onClick={onClick}>
    {addonBefore}
    <Typography.Text
      style={{ whiteSpace: 'nowrap', opacity: !isActive ? '0.8' : 1 }}
    >
      {title}
    </Typography.Text>
  </Space>
);

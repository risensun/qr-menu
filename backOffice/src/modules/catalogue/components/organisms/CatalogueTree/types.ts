import { Category } from 'src/types';

export type ActiveCategoryKeys = {
  levels: {
    [level: number]: Category['id'] | undefined;
  };
  final: Category['id'] | undefined;
};

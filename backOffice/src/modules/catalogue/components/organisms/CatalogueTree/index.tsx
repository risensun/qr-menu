import React from 'react';
import { Tabs } from 'antd';
import { EditOutlined, PlusOutlined } from '@ant-design/icons';
import { CatalogueSelectors } from 'src/modules/catalogue/utils';
import { Catalogue as CatalogueType } from 'src/types/Catalogue';
import { Category as CategoryType } from 'src/types/Category';
import { CategoryTab } from './partials/CategoryTab';
import { Category } from './partials/Category';
import { ActiveCategoryKeys } from './types';

export type CatalogueTreeProps = {
  readonly catalogue: CatalogueType;
  readonly activeKeys: ActiveCategoryKeys;
  readonly onCategoryAdd: (parentCategoryId: CategoryType['id'] | null) => void;
  readonly onCategoryEdit: (
    id: CategoryType['id'],
    parentCategoryId: CategoryType['id'] | null
  ) => void;
  readonly onCategoryChange: (id: CategoryType['id']) => void;
};

export const CatalogueTree: React.FC<CatalogueTreeProps> = ({
  catalogue,
  activeKeys,
  onCategoryAdd,
  onCategoryEdit,
  onCategoryChange,
}) => {
  /** Handlers */
  const handleEdit = React.useCallback(
    (
      action: 'add' | 'remove',
      id: CategoryType['id'],
      parentCategoryId: CategoryType['id'] | null
    ) => {
      if (action === 'remove') {
        onCategoryEdit(id, parentCategoryId);
      } else {
        onCategoryAdd(parentCategoryId);
      }
    },
    [onCategoryAdd, onCategoryEdit]
  );

  const handleChange = React.useCallback(
    (key: string) => {
      onCategoryChange(parseInt(key));
    },
    [onCategoryChange]
  );

  /** Markup helpers */
  const rootCategories = React.useMemo(
    () =>
      CatalogueSelectors.selectCategoriesByIds(
        catalogue,
        catalogue.rootIds
      ).filter((category): category is CategoryType => Boolean(category)),
    [catalogue]
  );

  const activeKey = activeKeys.levels[0]
    ? String(activeKeys.levels[0])
    : undefined;

  return (
    <Tabs
      size="large"
      type="editable-card"
      activeKey={activeKey}
      addIcon={
        <CategoryTab title="Добавить раздел" addonBefore={<PlusOutlined />} />
      }
      onEdit={(event, action) =>
        handleEdit(action, parseInt(event.toString()), null)
      }
    >
      {rootCategories.map((category) => (
        <Tabs.TabPane
          key={category.id}
          tab={
            <CategoryTab
              title={category.title}
              isActive={activeKey === String(category.id)}
              onClick={() => handleChange(String(category.id))}
            />
          }
          closeIcon={<EditOutlined />}
        >
          <Category
            catalogue={catalogue}
            activeKeys={activeKeys}
            category={category}
            level={0}
            onEdit={handleEdit}
            onChange={handleChange}
            shouldDisplayTitle={false}
          />
        </Tabs.TabPane>
      ))}
    </Tabs>
  );
};

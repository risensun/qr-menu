import React from 'react';
import { Category } from 'src/types';
import { MenuService } from '../MenuService';
import { MenuProduct } from '../types';

// TODO: move to redux
export const useMenuProducts = () => {
  const [products, setProducts] = React.useState<MenuProduct[]>([]);
  const [areProductsLoading, setAreProductsLoading] = React.useState(false);

  /** Handlers */
  const fetchProducts = React.useCallback((categoryId: Category['id']) => {
    setAreProductsLoading(true);

    MenuService.getCategoryProducts(categoryId)
      .then(setProducts)
      .catch(() => setProducts([]))
      .finally(() => setAreProductsLoading(false));
  }, []);

  return {
    products: {
      state: products,
      isLoading: areProductsLoading,
    },
    fetchProducts,
  };
};

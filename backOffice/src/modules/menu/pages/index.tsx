/**
 * Display global catalogue. Need to change to only menu catalogue
 */

import React from 'react';
import { Button, Card, Row, Typography } from 'antd';
import { NextPage } from 'next';
import Head from 'next/head';
import { useDispatch } from 'react-redux';
import { PlusOutlined } from '@ant-design/icons';

import { createNextPage, REDUX_INJECTOR, store, injectPage } from 'src/core';
import { displayErrorIfPossible } from 'src/utils';

import { Loader, Page, Header } from 'src/modules/common';
import { BadgeService, BadgeRedux } from 'src/modules/badge';
import {
  CatalogueTree,
  CatalogueSelectors,
  useCatalogueTree,
} from 'src/modules/catalogue';
import {
  CategoryAddDrawer,
  CategoryEditDrawer,
  useCategoryAddDrawer,
  useCategoryEditDrawer,
  useCategoryCatalogue,
} from 'src/modules/category';
import {
  ProductAddDrawer,
  ProductEditDrawer,
  useProductAddDrawer,
  useProductEditDrawer,
} from 'src/modules/products';

import { MenuModule } from '..';
import { MenuProductTable } from '../components';
import { useMenuProducts } from '../hooks';

const Component: NextPage = () => {
  const dispatch = useDispatch();
  const { catalogue, fetchCatalogue } = useCategoryCatalogue();
  const { activeCategoryKeys, handleCategoryChange } = useCatalogueTree({
    catalogue: catalogue.state,
  });
  const { products, fetchProducts } = useMenuProducts();
  const categoryEditDrawer = useCategoryEditDrawer();
  const categoryAddDrawer = useCategoryAddDrawer();
  const productEditDrawer = useProductEditDrawer();
  const productAddDrawer = useProductAddDrawer();

  const activeCategoryId = activeCategoryKeys.final;

  /** Handlers */
  const handleProductsUpdate = React.useCallback(() => {
    if (activeCategoryId) {
      fetchProducts(activeCategoryId);
    }
  }, [activeCategoryId]);

  /** Effects */
  React.useEffect(() => {
    fetchCatalogue();

    dispatch(BadgeRedux.actions.setIsLoading({ value: true }));
    BadgeService.getBadges()
      .then((badges) => {
        dispatch(BadgeRedux.actions.setBadges({ badges }));
      })
      .catch(displayErrorIfPossible)
      .finally(() =>
        dispatch(BadgeRedux.actions.setIsLoading({ value: false }))
      );
  }, [fetchCatalogue, dispatch]);

  React.useEffect(() => {
    if (activeCategoryId) {
      fetchProducts(activeCategoryId);
    }
  }, [activeCategoryId, fetchProducts]);

  /** Markup helpers */
  const activeCategoryTitle =
    (activeCategoryKeys.final &&
      CatalogueSelectors.selectCategoryById(
        catalogue.state,
        activeCategoryKeys.final
      )?.title) ||
    null;

  return (
    <>
      <Head>
        <title>Настройка меню</title>
      </Head>
      <Page>
        <Header />
        <CategoryEditDrawer {...categoryEditDrawer.props} />
        <CategoryAddDrawer {...categoryAddDrawer.props} />
        <ProductEditDrawer
          {...productEditDrawer.props}
          onSuccess={handleProductsUpdate}
        />
        <ProductAddDrawer
          {...productAddDrawer.props}
          onSuccess={handleProductsUpdate}
        />
        <Card
          style={{
            width: '98%',
            alignSelf: 'center',
            marginTop: '40px',
            marginBottom: '80px',
          }}
        >
          <Loader spinning={catalogue.isLoading} size="large">
            {catalogue ? (
              <CatalogueTree
                catalogue={catalogue.state}
                activeKeys={activeCategoryKeys}
                onCategoryAdd={categoryAddDrawer.open}
                onCategoryChange={handleCategoryChange}
                onCategoryEdit={(id, parentCategoryId) =>
                  categoryEditDrawer.open(
                    CatalogueSelectors.selectCategoryById(catalogue.state, id),
                    parentCategoryId
                  )
                }
              />
            ) : (
              <div style={{ height: 80 }} />
            )}
          </Loader>
          <MenuProductTable
            title={
              <Row justify="space-between">
                <Typography.Title level={4}>
                  {activeCategoryTitle && `Товары в «${activeCategoryTitle}»`}
                </Typography.Title>
                <Button
                  icon={<PlusOutlined />}
                  onClick={() =>
                    activeCategoryId && productAddDrawer.open(activeCategoryId)
                  }
                >
                  Добавить товар
                </Button>
              </Row>
            }
            onVisibilityChange={handleProductsUpdate}
            products={products.state}
            isLoading={products.isLoading}
            onProductClick={productEditDrawer.open}
          />
        </Card>
      </Page>
    </>
  );
};

export const MenuPage = createNextPage({
  module: MenuModule,

  getDefaultExport: () => Component,

  getPrerenderingFunctions: () => ({}),
});

injectPage(store, REDUX_INJECTOR, MenuPage);

import { createNextModule } from 'src/core/module/factory/createNextModule';
import { AuthModule } from '../auth';
import { BadgeModule } from '../badge';
import { CategoryModule } from '../category';
// TODO: use import from /products. Not causes error
import { ProductModule } from '../products/ProductModule';

export const MenuModule = createNextModule({
  dependencies: [AuthModule, ProductModule, BadgeModule, CategoryModule],
});

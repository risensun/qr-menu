import { EditOutlined, SaveOutlined } from '@ant-design/icons';
import { Button, message, Space, Typography } from 'antd';
import Input from 'antd/lib/input/Input';
import React from 'react';
import { useDispatch } from 'react-redux';
import { CategoryService } from 'src/modules/category/CategoryService';
import { CategoryRedux } from 'src/modules/category/redux';

export type EditableTitleProps = {
  readonly id: number;
  readonly title: string;
};

export const EditableTitle: React.FC<EditableTitleProps> = ({ id, title }) => {
  const [isLoading, setIsloading] = React.useState(false);
  const [isEditing, setIsEditing] = React.useState(false);
  const [inputValue, setInputValue] = React.useState(title);
  const dispatch = useDispatch();

  const handleRenameProduct = async () => {
    setIsloading(true);
    try {
      const result = await CategoryService.renameCategoryById(id, inputValue);

      if (result.isSucceeded) {
        setIsloading(false);
        setIsEditing(false);
        dispatch(CategoryRedux.actions.setCategoriesIsLoading({ value: true }));

        CategoryService.getCategories().then((categories) => {
          dispatch(
            CategoryRedux.actions.setCategories({ categories: [...categories] })
          );
          dispatch(
            CategoryRedux.actions.setCategoriesIsLoading({ value: false })
          );
        });
        message.success('Название успешно изменено!');
        return;
      }
      throw { status: 'error' };
    } catch {
      setInputValue(title);
      setIsloading(false);
      setIsEditing(false);
      message.error('Что-то пошло не так :(');
    }
  };

  return (
    <Space direction="horizontal" size={10}>
      <Typography.Title level={4} style={{ margin: 0 }}>
        Товары категории
      </Typography.Title>
      {isEditing ? (
        <Space size={10}>
          <Input
            value={inputValue}
            size="small"
            onChange={(event) => {
              setInputValue(event.target.value);
            }}
            disabled={isLoading}
            style={{ width: 'min-content' }}
          />
          <Button
            icon={<SaveOutlined />}
            onClick={handleRenameProduct}
            size="small"
            loading={isLoading}
            disabled={isLoading}
          />
        </Space>
      ) : (
        <Space size={10}>
          <Typography.Title level={4} style={{ margin: 0 }}>
            {inputValue}
          </Typography.Title>
          <Button
            icon={<EditOutlined />}
            onClick={() => setIsEditing(true)}
            size="small"
            type="ghost"
          />
        </Space>
      )}
    </Space>
  );
};

import { Affix, Button, Space, Typography } from 'antd';
import clsx from 'clsx';
import React from 'react';
import { getPlural } from 'src/modules/menu/utils/getPlural';
import styles from './save-affix.module.scss';

export type SaveAffixProps = {
  readonly loading: boolean;
  readonly isToggled: boolean;
  readonly itemsSelected: number;
  readonly onPublish?: () => void;
  readonly onHide?: () => void;
  // readonly onDelete?: () => void;
  // readonly onSaveChanges?: () => void;
  // readonly onCancel?: () => void;
};

export const SaveAffix: React.FC<SaveAffixProps> = ({
  loading,
  isToggled = false,
  itemsSelected = 0,
  onPublish,
  onHide,
  // onDelete,
  // onSaveChanges,
  // onCancel,
}) => {
  const a = 2;
  return (
    <div className={clsx(styles.root, isToggled && styles.toggled)}>
      {/* <Space direction="horizontal" size={16}> */}
      <Typography.Text>
        {`${getPlural(
          itemsSelected,
          'Выбран',
          'Выбрано',
          'Выбрано'
        )} ${itemsSelected} ${getPlural(
          itemsSelected,
          'товар',
          'товара',
          'товаров'
        )}`}
      </Typography.Text>
      <Button
        disabled={loading}
        type="primary"
        size="small"
        ghost
        onClick={onPublish}
      >
        Опубликовать
      </Button>
      <Button disabled={loading} size="small" onClick={onHide}>
        Скрыть
      </Button>
      {/* <Button danger size="small" ghost>
        Удалить
      </Button> */}
      {/* TODO: add multi deleting functionality */}
      {/* </Space> */}
      {/* <Space direction="horizontal" size={16}>
        <Button type="primary" size="small" onClick={onSaveChanges}>
          Сохранить изменения
        </Button>
        <Button size="small">Отменить</Button>
      </Space> */}
      {/* TODO: add multi editing functionality */}
    </div>
  );
};

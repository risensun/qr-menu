/**
 * Move to modules/products/comopnents/molecules
 */

import React from 'react';
import { Tag } from 'antd';

export type StatusProps = {
  readonly isPublished: boolean;
};

export const Status: React.FC<StatusProps> = ({ isPublished }) => (
  <Tag color={isPublished ? 'blue' : 'default'}>
    {isPublished ? 'Опубликован' : 'Скрыт'}
  </Tag>
);

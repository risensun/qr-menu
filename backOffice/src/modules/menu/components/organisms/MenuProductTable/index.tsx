import React from 'react';
import { message, Radio, Space, Table, Typography } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { MenuProduct } from 'src/modules/menu/types';
import { shouldProductBeDisplayed } from 'src/modules/products/utils/shouldProductBeDisplayed';
import { Status } from '../../atoms';
import { SaveAffix } from '../../molecules/SaveAffix';
import { ProductService } from 'src/modules/products';

export enum Filter {
  ALL = 'ALL',
  PUBLISHED = 'PUBLISHED',
  HIDDEN = 'HIDDEN',
}

export type MenuProductTableProps = {
  readonly products: MenuProduct[];
  readonly isLoading: boolean;
  readonly title?: React.ReactNode | string | null;
  readonly onVisibilityChange: () => void;
  readonly onProductClick: (product: MenuProduct) => void;
};

export const MenuProductTable: React.FC<MenuProductTableProps> = ({
  products,
  isLoading,
  title: titleFromProps = null,
  onVisibilityChange,
  onProductClick,
}) => {
  const [filter, setFilter] = React.useState(Filter.ALL);
  const [loading, setIsLoading] = React.useState(false);
  const [selectedProductIds, setSelectedProductIds] = React.useState<number[]>(
    []
  );

  const handleChangeVisibility = (
    productIds: number[],
    isPublished: boolean
  ) => {
    setIsLoading(true);
    ProductService.changeProductsVisibilityById(productIds, isPublished)
      .then((isSucceeded) => {
        if (isSucceeded) {
          message.success(
            `Продукты успешно ${isPublished ? 'опубликованы' : 'скрыты'}`
          );
          setIsLoading(false);
          setSelectedProductIds([]);
          onVisibilityChange();
          return;
        }
        message.error('Что-то пошло не так, попробуйте снова');
        setIsLoading(false);
        setSelectedProductIds([]);
      })
      .catch(() => {
        message.error('Что-то пошло не так, попробуйте снова');
      });
  };

  const title =
    typeof titleFromProps === 'string' ? (
      <Typography.Title level={4}>{titleFromProps}</Typography.Title>
    ) : (
      titleFromProps
    );

  return (
    <>
      {title && title}
      <Radio.Group
        style={{ display: 'block', marginBottom: '20px' }}
        value={filter}
        onChange={(event) => setFilter(event.target.value)}
      >
        <Radio value={Filter.ALL}>Все</Radio>
        <Radio value={Filter.PUBLISHED}>Опубликованные</Radio>
        <Radio value={Filter.HIDDEN}>Скрытые</Radio>
      </Radio.Group>
      <Table
        bordered
        loading={{
          indicator: <LoadingOutlined />,
          size: 'large',
          spinning: isLoading,
        }}
        rowKey={(record) => record.id}
        onRow={(record) => ({
          onClick: () => onProductClick(record),
        })}
        rowSelection={{
          type: 'checkbox',
          selectedRowKeys: selectedProductIds,
          onChange: (_: React.Key[], selectedRows: MenuProduct[]) => {
            setSelectedProductIds(selectedRows.map((product) => product.id));
          },
        }}
        columns={[
          {
            key: 'status',
            title: 'Статус',
            dataIndex: 'isPublished',
            render: (isPublished: boolean) => (
              <Status isPublished={isPublished} />
            ),
            width: '10%',
          },
          {
            key: 'title',
            title: 'Название',
            dataIndex: 'title',
            render: (title: string) => <span>{title}</span>,
            width: '50%',
          },
          {
            key: 'size',
            title: 'Размер',
            dataIndex: 'size',
            render: (sizes: MenuProduct['size']) => (
              <Space direction="horizontal">
                {sizes.options.map((size) => (
                  <Typography.Text
                    key={size.key}
                    style={{ whiteSpace: 'nowrap' }}
                  >
                    {size.name} {size.value} {sizes.unit}
                  </Typography.Text>
                ))}
              </Space>
            ),
          },
          {
            key: 'price',
            title: 'Цена',
            dataIndex: 'size',
            render: (size: MenuProduct['size']) => (
              <Space direction="horizontal">
                {size.options.map((size) => (
                  <Typography.Text key={size.key}>
                    {size.price.toLocaleString('ru-RU', {
                      currency: 'rub',
                      style: 'currency',
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </Typography.Text>
                ))}
              </Space>
            ),
          },
        ]}
        dataSource={products.filter(({ isPublished }) =>
          shouldProductBeDisplayed(filter, isPublished)
        )}
        pagination={false}
      />
      <SaveAffix
        loading={loading}
        onPublish={() => handleChangeVisibility(selectedProductIds, true)}
        onHide={() => handleChangeVisibility(selectedProductIds, false)}
        isToggled={selectedProductIds.length > 0}
        itemsSelected={selectedProductIds.length}
      />
    </>
  );
};

import { Config } from 'src/Config';
import { Fetcher, Method } from 'src/helpers';
import { Catalogue, Category } from 'src/types';
import { withAuth } from '../auth/utils';
import { MenuProduct } from './types';

export abstract class MenuService {
  protected static readonly fetcher = new Fetcher(Config.getApiUrl());

  public static async getCategoryProducts(
    categoryId: Category['id']
  ): Promise<MenuProduct[]> {
    const { entities } = await withAuth(() =>
      this.fetcher.request(Method.GET, {
        url: `/bo/menus/${Config.getMenuId()}/categories/${categoryId}/products`,
      })
    );

    return entities;
  }

  public static async getCatalogue(): Promise<Catalogue> {
    const { entity } = await withAuth(() =>
      this.fetcher.request(Method.GET, {
        url: `/bo/menus/${Config.getMenuId()}/catalogue`,
      })
    );

    return entity;
  }

  public static async addProduct(
    ids: number[]
  ): Promise<{ isSucceeded: boolean }> {
    const { isSucceeded } = await withAuth(() =>
      this.fetcher.request(Method.POST, {
        url: `/bo/menus/${Config.getMenuId()}/products`,
        body: {
          productIds: ids,
        },
      })
    );

    return { isSucceeded };
  }
}

export const editProperty = <
  T extends Record<string, string | number | boolean>,
  P extends keyof T
>(
  currentState: T,
  key: P,
  value: T[P]
): T => ({ ...currentState, [key]: value });

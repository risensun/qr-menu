import { Category } from 'src/types/Category';

export type Options = {
  value: number;
  label: string;
  children?: Options[] | undefined;
};

export const createOptions = (
  rootIds: number[],
  catalogue: { [key: string]: Category }
): Options[] => {
  if (Object.keys(catalogue).length > 0) {
    return rootIds.map((id) => ({
      value: catalogue[id].id,
      label: catalogue[id].title,
      children: catalogue[id].children.map((id) => ({
        value: catalogue[id].id,
        label: catalogue[id].title,
      })),
    }));
  }
  return [];
};

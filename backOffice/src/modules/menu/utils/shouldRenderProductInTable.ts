export const shouldRenderProductInTable = (
  currentFilter: 'all' | 'published' | 'hidden',
  productVisibility: boolean
): boolean => {
  if (
    currentFilter === 'all' ||
    (productVisibility && currentFilter === 'published') ||
    (!productVisibility && currentFilter === 'hidden')
  ) {
    return true;
  }

  return false;
};

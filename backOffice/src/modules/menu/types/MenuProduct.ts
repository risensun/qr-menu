import { Product } from 'src/types';

export type MenuProduct = Product & {
  isPublished: boolean;
};

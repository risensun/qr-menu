import { Config } from 'src/Config';
import { Fetcher, Method } from 'src/helpers';
import { withError } from 'src/utils';
import { boolean } from 'yup';

const REFRESH_TOKEN_KEY = 'a_RT';

export abstract class AuthService {
  protected static readonly fetcher = new Fetcher(Config.getApiUrl());

  public static async checkAccessToken(): Promise<boolean> {
    try {
      await this.fetcher.request(Method.HEAD, { url: '/auth/access' });

      return true;
    } catch (error) {
      return false;
    }
  }

  public static async login(login: string, password: string): Promise<boolean> {
    const response = await withError(() =>
      this.fetcher.request(Method.POST, {
        url: '/auth/login',
        body: { login, password },
      })
    );

    if (response) {
      this.saveRefreshTokenToStorage(response.token);

      return this.getAccessToken();
    }

    return false;
  }

  public static async getAccessToken(): Promise<boolean> {
    if (!AuthService.isRefreshTokenExist()) {
      return false;
    }

    try {
      this.fetcher.request(Method.POST, {
        url: '/auth/access',
        body: { token: AuthService.getRefreshTokenFromStorage() },
      });

      return true;
    } catch (error) {
      return false;
    }
  }

  public static logout(): void {
    window.localStorage.removeItem(REFRESH_TOKEN_KEY);
    window.location.href = '/login';
  }

  public static isRefreshTokenExist(): boolean {
    return Boolean(this.getRefreshTokenFromStorage());
  }

  protected static getRefreshTokenFromStorage(): string | null {
    return window.localStorage.getItem(REFRESH_TOKEN_KEY);
  }

  protected static saveRefreshTokenToStorage(token: string): void {
    window.localStorage.setItem(REFRESH_TOKEN_KEY, token);
  }
}

import { useRouter } from 'next/router';
import React from 'react';
import { AuthService } from 'src/modules/auth/AuthService';

export type AuthorizedProps = {
  readonly ignoredPaths: string[];
  readonly children: React.ReactNode;
};

export const Authorized: React.FC<AuthorizedProps> = ({
  ignoredPaths,
  children,
}) => {
  const router = useRouter();
  const [isAuthorized, setIsAuthorized] = React.useState(
    ignoredPaths.includes(router.pathname)
  );

  React.useEffect(() => {
    if (isAuthorized) {
      return;
    }

    if (!AuthService.isRefreshTokenExist()) {
      router.push('/login');

      return;
    }

    AuthService.checkAccessToken().then((isOk) => {
      if (isOk) {
        setIsAuthorized(true);

        return;
      }

      AuthService.getAccessToken().then((isSucceeded) => {
        if (isSucceeded) {
          setIsAuthorized(true);

          return;
        }

        router.push('/login');
      });
    });
  }, [isAuthorized, router]);

  if (!isAuthorized) {
    return <p>Авторизация...</p>;
  }

  return <>{children}</>;
};

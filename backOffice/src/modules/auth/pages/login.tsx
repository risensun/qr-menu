import React from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { Button, Form, Input, Space, Typography } from 'antd';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { createNextPage } from 'src/core/module/factory/createNextPage';
import { Page } from 'src/modules/common/components';
import { AuthService } from '../AuthService';
import { AuthModule } from '..';

const Component: NextPage = () => {
  const router = useRouter();

  const handleFinish = (values: any) => {
    AuthService.login(values.username, values.password)
      .then((isSucceeded) => {
        if (isSucceeded) {
          router.push('/');
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <Page>
      <Space
        direction="vertical"
        style={{ height: '100vh', justifyContent: 'center' }}
        align="center"
      >
        <Form
          name="AuthForm"
          onFinish={handleFinish}
          autoComplete="off"
          style={{ width: '360px' }}
        >
          <Space
            direction="horizontal"
            style={{
              width: '100%',
              justifyContent: 'center',
              marginBottom: '16px',
            }}
          >
            <Typography.Title level={3}>Авторизация</Typography.Title>
          </Space>
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input
              size="large"
              prefix={<UserOutlined />}
              placeholder="имя пользователя"
            />
          </Form.Item>
          <Form.Item name="password" rules={[{ required: true }]}>
            <Input.Password
              size="large"
              placeholder="пароль"
              prefix={<LockOutlined />}
              visibilityToggle={false}
            />
          </Form.Item>
          <Form.Item>
            <Button style={{ width: '100%' }} type="primary" htmlType="submit">
              Войти
            </Button>
          </Form.Item>
        </Form>
      </Space>
    </Page>
  );
};

export const LoginPage = createNextPage({
  module: AuthModule,

  getDefaultExport: () => Component,

  getPrerenderingFunctions: () => ({}),
});

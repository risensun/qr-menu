import { AuthService } from '../AuthService';

export async function withAuth<R>(request: () => Promise<R>): Promise<R> {
  try {
    return await request();
  } catch (error: any) {
    if (error.code === 401) {
      const wasAccessTokenRenewed = await AuthService.getAccessToken();

      if (wasAccessTokenRenewed) {
        return request();
      }
    }

    throw error;
  }
}

import { message } from 'antd';

export async function withError<R>(
  func: () => Promise<R>
): Promise<R | undefined> {
  try {
    return await func();
  } catch (error: any) {
    if ('error' in error) {
      message.error(error.error, 4);

      return undefined;
    }

    throw error;
  }
}

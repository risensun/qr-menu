import { message } from 'antd';

export function displayErrorIfPossible(error: any) {
  if ('error' in error) {
    message.error(error.error, 4);

    return;
  }

  throw error;
}

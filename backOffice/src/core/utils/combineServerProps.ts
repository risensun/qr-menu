export function combineServerProps(
  prebuiltProps: any = {},
  additionalProps: any[] = []
): any {
  const props = [...additionalProps, prebuiltProps];

  const combinedProps = props.reduce(
    (all, current) => ({
      ...all,
      ...current,
    }),
    {}
  );

  return combinedProps;
}

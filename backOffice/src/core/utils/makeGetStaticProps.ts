import { GetStaticProps, GetStaticPropsResult } from 'next';
import { getReduxServerProps } from '../redux/react/getReduxServerProps';
import { combineServerProps } from './combineServerProps';

// Shorhand for creation static page with redux and i18n
export function makeGetStaticProps<P>(
  getStaticProps?: GetStaticProps<P>
): GetStaticProps<P> {
  return async (context) => {
    const result: GetStaticPropsResult<P> = getStaticProps
      ? await getStaticProps(context)
      : { props: {} as any, revalidate: 43200 };

    if ('props' in result) {
      const props = combineServerProps(result.props, [
        getReduxServerProps(),
      ]);

      return {
        ...result,
        props,
      };
    }

    return result;
  };
}

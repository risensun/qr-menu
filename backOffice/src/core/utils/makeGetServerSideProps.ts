import { GetServerSideProps, GetServerSidePropsResult } from 'next';
import { getReduxServerProps } from '../redux/react/getReduxServerProps';
import { combineServerProps } from './combineServerProps';

// Shorhand for creation static page with redux and i18n
export function makeGetServerSideProps<P>(
  getServerSideProps?: GetServerSideProps<P>
): GetServerSideProps<P> {
  return async (context) => {
    const result: GetServerSidePropsResult<P> = getServerSideProps
      ? await getServerSideProps(context)
      : { props: {} as any };

    if ('props' in result) {
      const props = combineServerProps(result.props, [
        getReduxServerProps(),
      ]);

      return {
        ...result,
        props,
      };
    }

    return result;
  };
}

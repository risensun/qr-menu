import { Reducer, Store } from '@reduxjs/toolkit';
import { ReduxInjector } from 'src/core/redux/ReduxInjector';

export function injectReducers(
  store: Store,
  injector: ReduxInjector,
  reducers: { [key: string]: Reducer }
): void {
  for (const key of Object.keys(reducers)) {
    injector.injectReducer(key, reducers[key]);
  }

  injector.updateStoreReducer(store);
}

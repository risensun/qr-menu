import { Saga } from 'redux-saga';
import { ReduxInjector } from 'src/core/redux/ReduxInjector';

export function injectSaga(
  injector: ReduxInjector,
  sagas: [string, Saga<any[]>][]
): void {
  for (const saga of sagas) {
    injector.injectSaga(saga[0], saga[1]);
  }
}

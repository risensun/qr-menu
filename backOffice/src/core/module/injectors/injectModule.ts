import { Store } from '@reduxjs/toolkit';
import { ReduxInjector } from '../../redux/ReduxInjector';
import { BaseModuleConfiguration, ModuleConfiguration } from '../Module';
import { injectReducers } from './injectReducers';
import { injectSaga } from './injectSaga';

// TODO: possible endless cycle of module initializing
// Solution: refactor to ModuleInjector, and store registry of injected modules (add key to each module)
function injectBaseModule(
  store: Store,
  injector: ReduxInjector,
  module: BaseModuleConfiguration
): void {
  if (module.getReducers) {
    injectReducers(store, injector, module.getReducers());
  }

  if (module.getSagas) {
    injectSaga(injector, module.getSagas());
  }

  if (module.dependencies) {
    for (const dependency of module.dependencies) {
      injectModule(store, injector, dependency);
    }
  }
}

export function injectModule<T extends ModuleConfiguration>(
  store: Store,
  injector: ReduxInjector,
  module: T
): void {
  injectBaseModule(store, injector, module);
}

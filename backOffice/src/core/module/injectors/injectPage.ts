import { Store } from '@reduxjs/toolkit';
import {
  BasePageConfiguration,
  NextPageConfiguration,
  PageConfiguration,
  PageType,
} from '../Module';
import { injectReducers } from './injectReducers';
import { injectSaga } from './injectSaga';
import { injectModule } from './injectModule';
import { ReduxInjector } from 'src/core/redux/ReduxInjector';

function injectBasePage(
  store: Store,
  injector: ReduxInjector,
  page: BasePageConfiguration
): void {
  if (page.getReducers) {
    injectReducers(store, injector, page.getReducers());
  }

  if (page.getSagas) {
    injectSaga(injector, page.getSagas());
  }
}

function injectNextPage(
  store: Store,
  injector: ReduxInjector,
  page: NextPageConfiguration
): void {
  injectBasePage(store, injector, page);

  injectModule(store, injector, page.module);
}

export function injectPage<T extends PageConfiguration>(
  store: Store,
  injector: ReduxInjector,
  page: T
): void {
  if (page.type === PageType.Next) {
    injectNextPage(store, injector, page);
  }
}

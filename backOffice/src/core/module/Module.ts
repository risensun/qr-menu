import { AnyAction, Reducer } from '@reduxjs/toolkit';
import { Saga } from 'redux-saga';

type RootSaga = Saga<any[]>;

// Global
export enum ModuleType {
  Common = 'COMMON',
  Next = 'NEXT',
  CSR = 'CLIENT_SIDE_RENDER',
}

export enum PageType {
  Next = 'NEXT_PAGE',
  CSR = 'CLIENT_SIDE_RENDER_PAGE',
}

// Common
export type BasePageConfiguration = {
  readonly type: PageType;

  getReducers?(): { [key: string]: Reducer<any, AnyAction> };

  getSagas?(): [string, RootSaga][];
};

export type BaseModuleConfiguration = {
  readonly type: ModuleType;
  readonly dependencies?: BaseModuleConfiguration[];

  getReducers?(): { [key: string]: Reducer<any, AnyAction> };

  getSagas?(): [string, RootSaga][];
};

// Common
export type CommonModuleConfiguration = BaseModuleConfiguration & {
  readonly type: ModuleType.Common;
};

// Next
export type NextPageConfiguration = BasePageConfiguration & {
  readonly type: PageType.Next;
  readonly module: NextModuleConfiguration;

  getDefaultExport(): any;

  // TODO: resolve typings, to indicate error when export pre-render function, that was not declared
  getPrerenderingFunctions(): any;
};

export type NextModuleConfiguration = BaseModuleConfiguration & {
  readonly type: ModuleType.Next;
};

// CSR
export type CSRModuleConfiguration = BaseModuleConfiguration & {
  readonly type: ModuleType.CSR;
  // getRoutes() - to build real-time routes
};

export type ModuleConfiguration =
  | CommonModuleConfiguration
  | NextModuleConfiguration
  | CSRModuleConfiguration;

export type PageConfiguration = NextPageConfiguration;

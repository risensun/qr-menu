export type Route = {
  readonly name: string;
  readonly url: string;
};

import { combineReducers, configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { BadgeRedux } from 'src/modules/badge/redux';
import { ProductRedux } from 'src/modules/products/redux';
import { ReduxInjector } from './ReduxInjector';

const sagaMiddleware = createSagaMiddleware();

export const REDUX_INJECTOR = new ReduxInjector(sagaMiddleware.run);

export const store = configureStore({
  reducer: combineReducers(ProductRedux.reducer),
  middleware: (getDefaultMiddleware) => [
    ...getDefaultMiddleware(),
    sagaMiddleware,
  ],
  preloadedState: {},
});

import { PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from '../constants';

export type HydrateAction = PayloadAction<any, typeof HYDRATE>;

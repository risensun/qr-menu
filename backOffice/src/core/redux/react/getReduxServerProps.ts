import { HYDRATE } from '../constants';
import { store } from '../store';

export function getReduxServerProps() {
  return {
    [HYDRATE]: store.getState(),
  };
}

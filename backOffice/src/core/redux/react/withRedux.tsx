import React from 'react';
import { HYDRATE } from '../constants';
import { store } from '../store';
import { isOnServerSide } from 'src/core/utils/isOnServeSide';

// eslint-disable-next-line @typescript-eslint/ban-types
type HydaratedReduxProps<P extends {}> = P & { [HYDRATE]: any };

// eslint-disable-next-line @typescript-eslint/ban-types
export function withRedux<P extends {}>(
  Component: React.ComponentType<P>
): React.ComponentType<HydaratedReduxProps<P>> {
  return class extends React.Component<HydaratedReduxProps<P>> {
    public static displayName = `withRedux(${Component.displayName})`;

    public constructor(props: HydaratedReduxProps<P>) {
      super(props);

      if (!isOnServerSide() && props[HYDRATE]) {
        store.dispatch({ type: HYDRATE, payload: props[HYDRATE] });
      }
    }

    public render(): React.ReactNode {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { __HYDRATE__REDUX__EVENT__, ...rest } = this.props;

      return <Component {...(rest as any)} />;
    }
  };
}

import { Config } from 'src/Config';

/* eslint-disable @typescript-eslint/ban-types */
export enum Method {
  HEAD = 'HEAD',
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
}

export type MethodWithBody =
  | Method.POST
  | Method.PUT
  | Method.PATCH
  | Method.DELETE;

export type Params<T extends Method> = {
  url: string;
} & (T extends MethodWithBody
  ? {
      isFormData?: boolean;
      body?: any;
    }
  : {});

function areParamsWithBody(
  params: Params<Method>
): params is Params<MethodWithBody> {
  if ('body' in params) {
    return true;
  }

  return false;
}

export class Fetcher {
  public constructor(protected baseRoute: string) {}

  public async request<T extends Method>(
    method: T,
    params: Params<T>
  ): Promise<any> {
    const { url } = params;

    const headers = new Headers();

    headers.append('X-Tenant-Id', Config.getTenantId());

    if (areParamsWithBody(params) && !params.isFormData) {
      headers.append('Content-Type', 'application/json');
    }

    const response = await fetch(Fetcher.FIX_URI(`${this.baseRoute}/${url}`), {
      method,
      headers,
      credentials: 'include',
      cache: 'default',
      body: areParamsWithBody(params) ? Fetcher.GET_BODY(params) : undefined,
    });

    if (!response.ok) {
      switch (response.status) {
        case 400:
          throw {
            error:
              'Ошибка запроса. Пожалуйста, перепроверьте корректность данных.',
            code: response.status,
          };

        case 401:
          throw {
            error: 'Ошибка ошибка авторизации.',
            code: response.status,
          };

        case 404:
          throw {
            error: 'Запрашиваемый ресурс не найден.',
            code: response.status,
          };

        case 500:
        default:
          throw {
            error:
              'Внутренняя ошибка запроса. Пожалуйста, повторите позже или свяжитесь со службой поддержки.',
            code: response.status,
          };
      }
    }

    if (method === Method.HEAD) {
      return null;
    }

    const data = await response.json();

    return data.data;
  }

  private static GET_BODY(params: Params<MethodWithBody>): any {
    const { body, isFormData } = params;

    if (!body) {
      return undefined;
    }

    if (!isFormData) {
      return JSON.stringify(body);
    }

    return body;
  }

  private static FIX_URI(uri: string): string {
    return uri.replace(/(?<!:)(\/\/)/g, '/');
  }
}

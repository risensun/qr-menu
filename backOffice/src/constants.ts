export const ValidationMessages = {
  REQUIRED: 'Это обязательное поле',
  MUST_BE_POSITIVE: 'Значение должно быть положительным числом',
};

export abstract class Config {
  public static getApiUrl(): string {
    const apiUrl = process.env.NEXT_PUBLIC_API_URL;

    if (!apiUrl) {
      throw new Error('NEXT_PUBLIC_API_URL is not provided.');
    }

    return apiUrl;
  }

  public static getTenantId(): string {
    const tenantId = process.env.TENANT_ID;

    if (!tenantId) {
      throw new Error('TENANT_ID is not provided');
    }

    return tenantId;
  }

  public static getMenuId(): string {
    const menuId = process.env.NEXT_PUBLIC_MENU_ID;

    if (!menuId) {
      throw new Error('NEXT_PUBLIC_MENU_ID is not provided');
    }

    return menuId;
  }
}

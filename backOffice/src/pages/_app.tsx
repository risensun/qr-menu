import 'src/scss/index.scss';
import 'antd/dist/antd.css';
import type { AppProps } from 'next/app';
import { Provider } from 'react-redux';
import { store } from 'src/core/redux';
import { Component } from 'preact';
import { Authorized } from 'src/modules/auth/components';

class MyApp extends Component<AppProps> {
  constructor(props: AppProps) {
    super(props);
  }

  render() {
    return (
      <Provider store={store}>
        <Authorized ignoredPaths={['/login']}>
          <this.props.Component {...this.props.pageProps} />
        </Authorized>
      </Provider>
    );
  }
}

export default MyApp;

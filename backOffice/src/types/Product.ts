import { Badge } from './Badge';
import { Category } from './Category';

export type Product = {
  readonly id: number;
  readonly menuIds: string[];
  readonly createdAt: number;
  readonly tenantId: string;
  readonly categoryId: Category['id'];
  readonly title: string;
  readonly ingredients: string;
  readonly description: string;
  readonly size: Size;
  readonly badges: Badge['id'][];
  readonly options: ProductOption['id'][];
  readonly media: {
    readonly miniatureSrc: string;
    readonly gallery: {
      readonly defaultSrc: string; // .png | .jpg
      readonly modernSrc: string; // .webp
    }[];
  };
};

export type Nutrients = {
  kkal: number;
  proteins: number;
  fat: number;
  carbohydrate: number;
};

export type Size = {
  name: string; //Объем, порция, размер пиццы
  unit: string; //мл, гр, см
  options: SizeOption[];
};

export type SizeOption = {
  name: string; //same as my huge dick
  value: number; // value of property
  price: number; //499
  nutrients: Nutrients;
  key: string; //random unique id
  isActive: boolean; //should size be avaliable on client side
};

export type ProductOption = {
  readonly id: number;
  readonly name: string;
  readonly priceShift: number;
  readonly isDeafult: boolean;
};

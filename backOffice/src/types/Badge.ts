export enum BadgeKey {
  Hot = 'HOT',
  Cold = 'COLD',
}

export type Badge = {
  readonly id: string;
  readonly name: string;
  readonly key: BadgeKey;
};

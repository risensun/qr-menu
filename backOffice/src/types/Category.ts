export type Category = {
  readonly id: number;
  readonly createdAt: number;
  readonly children: number[];
  readonly tenantId: string;
  readonly title: string;
  readonly description: null | string;
};

import { Category } from './Category';

export type Catalogue = {
  readonly ids: Category['id'][];
  readonly rootIds: Category['id'][];
  readonly categories: {
    [key: Category['id']]: Category | undefined;
  };
};

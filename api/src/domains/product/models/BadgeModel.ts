import { UpdateComposerBuilder } from '@glangeo/pollux';
import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { ObjectId } from 'mongodb';
import { Tenant } from 'src/domains/tenant';
import { getBadgeCollectionAdapter } from '../collections';
import { Badge } from '../types';

export class BadgeModel {
  protected readonly adapter: ReturnType<typeof getBadgeCollectionAdapter>;

  public constructor(db: MongoDB) {
    this.adapter = getBadgeCollectionAdapter(db);
  }

  /**
   * Creates new badge
   * @param payload
   */
  public async create(
    payload: Pick<Badge, 'tenantId' | 'name' | 'key'>
  ): Promise<Badge> {
    return this.adapter.create(payload);
  }

  /**
   * Gets tenant badge by id
   * @param tenantId
   * @param id
   */
  public getByTenantAndId(
    tenantId: Tenant['id'],
    id: Badge['id']
  ): Promise<Badge> {
    return this.adapter.getOne({ tenantId, _id: new ObjectId(id) });
  }

  /**
   * Gets all tenant badges
   */
  public getByTenantId(tenantId: Tenant['id']): Promise<Badge[]> {
    return this.adapter.getMany({ tenantId });
  }

  /**
   * Gets update composer
   * @param badge
   */
  public getUpdateComposer(badge: Badge) {
    return new UpdateComposerBuilder<Badge>()
      .addSetter('name')
      .build((changes) =>
        this.adapter.updateOne(
          { _id: new ObjectId(badge.id) },
          { $set: changes }
        )
      );
  }
}

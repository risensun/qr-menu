import { UpdateComposerBuilder } from '@glangeo/pollux';
import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { ObjectId } from 'mongodb';
import { getOptionCollectionAdapter } from '../collections';
import { Option } from '../types';

export class OptionModel {
  protected readonly adapter: ReturnType<typeof getOptionCollectionAdapter>;

  public constructor(db: MongoDB) {
    this.adapter = getOptionCollectionAdapter(db);
  }

  public async create(
    payload: Pick<Option, 'tenantId' | 'name' | 'price'>
  ): Promise<Option> {
    return this.adapter.create(payload);
  }

  public async getById(id: Option['id']): Promise<Option> {
    return this.adapter.getOne({ _id: new ObjectId(id) });
  }

  public async getAll(): Promise<Option[]> {
    return this.adapter.getAll();
  }

  public getUpdateComposer(option: Option) {
    return new UpdateComposerBuilder<Option>()
      .addSetter('name')
      .addSetter('price')
      .build((changes) =>
        this.adapter.updateOne({ id: option.id }, { $set: changes })
      );
  }
}

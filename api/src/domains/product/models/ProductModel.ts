import { UpdateComposerBuilder } from '@glangeo/pollux';
import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { nanoid } from 'nanoid';
import { Category } from 'src/domains/category';
import { Menu } from 'src/domains/menus';
import { Tenant } from 'src/domains/tenant';
import { Registry } from 'src/Registry';
import { getProductCollectionAdapter } from '../collections';
import { Product } from '../types';

export class ProductModel {
  protected readonly adapter: ReturnType<typeof getProductCollectionAdapter>;

  public constructor(protected readonly db: MongoDB) {
    this.adapter = getProductCollectionAdapter(db);
  }

  /**
   * Creates new product
   * @param payload
   */
  public async create(
    payload: Pick<
      Product,
      | 'tenantId'
      | 'title'
      | 'media'
      | 'description'
      | 'size'
      | 'ingredients'
      | 'categoryId'
      | 'options'
      | 'badges'
    >
  ): Promise<Product> {
    const { size } = payload;

    return this.adapter.create({
      ...payload,
      size: {
        ...size,
        options: size.options.map((option) => ({
          ...option,
          key: nanoid(),
        })),
      },
    });
  }

  /**
   * Gets product by ID
   * @param id
   */
  public async getById(id: Product['id']): Promise<Product> {
    return this.adapter.getOne({ id });
  }

  /**
   * Gets list of products by IDs
   * @param ids
   */
  public async getByIds(ids: Product['id'][]): Promise<Product[]> {
    return this.adapter.getMany({ id: { $in: ids } });
  }

  /**
   * Gets products by given category ids
   * @param ids
   */
  public async getByCategoryIds(ids: Category['id'][]): Promise<Product[]> {
    return this.adapter.getMany({ categoryId: { $in: ids } });
  }

  /**
   * Gets all tenant products
   * @param tenantId
   */
  public async getByTenantId(tenantId: Tenant['id']): Promise<Product[]> {
    return this.adapter.getMany({ tenantId });
  }

  /**
   * Gets composer to update product
   * @param product
   */
  public getUpdateComposer(product: Product) {
    return (
      new UpdateComposerBuilder<Product>()
        // TODO: check if category exists
        .addSetter('categoryId')
        .addSetter('title')
        .addSetter('description')
        .addSetter('ingredients')
        .addSetter('media', async (nextMedia) => {
          // TODO: add old photo deleting
          if (
            nextMedia.miniatureSrc !== product.media.miniatureSrc &&
            product.media.miniatureSrc
          ) {
            await Registry.getProductMediaStorage().deleteFileByName(
              product.media.miniatureSrc
            );
          }

          return nextMedia;
        })
        .addSetter('size', (size) => {
          const nextSize: Product['size'] = {
            ...size,
            options: size.options.map((option) => ({
              ...option,
              key: nanoid(),
            })),
          };

          return nextSize;
        })
        .addSetter('options')
        // TODO: check if badges exist
        .addSetter('badges')
        .build(async (changes) => {
          if (changes.categoryId) {
            await Registry.getMenuModel().reactToProductsCategoryChange(
              [product],
              changes.categoryId
            );
          }

          return this.adapter.updateOne({ id: product.id }, { $set: changes });
        })
    );
  }

  /**
   * Deletes products and trigger MenuModel to update all menus containing this product
   */
  public async delete(products: Product[]): Promise<boolean> {
    await Registry.getMenuModel().reactToProductsDeletion(products);

    const productIds = products.map(({ id }) => id);

    return this.adapter.deleteMany({
      id: { $in: productIds },
    });
  }

  /* --- Side Effects --- */

  /**
   * Removes deleted menu from all affected products
   * @param menu
   */
  public async reactToMenuDeletion(menu: Menu): Promise<boolean> {
    const productIds = menu.products.map(({ id }) => id);

    return this.adapter.updateMany(
      { id: { $in: productIds } },
      { $pull: { menuIds: menu.id } }
    );
  }

  /**
   * Adds menuId to products
   * @param productIds
   * @param menu
   */
  public async reactToMenuProductAddition(
    productIds: Product['id'][],
    menu: Menu
  ): Promise<boolean> {
    return this.adapter.updateMany(
      { id: { $in: productIds } },
      { $push: { menuIds: menu.id } }
    );
  }

  /**
   * Removes menuId from products
   * @param productIds
   * @param menu
   */
  public async reactToMenuProductDeletion(
    productIds: Product['id'][],
    menu: Menu
  ): Promise<boolean> {
    return this.adapter.updateMany(
      { id: { $in: productIds } },
      { $pull: { menuIds: menu.id } }
    );
  }
}

import {
  createCollection,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { Badge } from '../types';

type BadgeRecord = RecordSchema<Omit<Badge, 'id'>>;

export function getBadgeCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: 'products__badges',

      createEntityFromDBRecord(record: BadgeRecord): Badge {
        const { _id, ...rest } = record;

        return {
          ...rest,
          id: _id.toHexString(),
        };
      },

      async getRecordDefaultFields() {
        return {
          createdAt: Date.now(),
        };
      },
    })
  );
}

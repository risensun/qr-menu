import {
  createCollection,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { Option } from '../types';

type OptionRecord = RecordSchema<Omit<Option, 'id'>>;

export function getOptionCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: `products__options`,

      createEntityFromDBRecord(record: OptionRecord): Option {
        const { _id, ...rest } = record;

        return {
          ...rest,
          id: _id.toHexString(),
        };
      },

      async getRecordDefaultFields() {
        return {
          createdAt: Date.now(),
        };
      },
    })
  );
}

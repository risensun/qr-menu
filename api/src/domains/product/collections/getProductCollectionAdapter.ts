import {
  createCollection,
  generateEntityId,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { omit } from 'lodash';
import { Product } from '../types';

export type ProductRecord = RecordSchema<Product>;

export function getProductCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: 'products',

      createEntityFromDBRecord(record: ProductRecord): Product {
        return omit(record, '_id');
      },

      async getRecordDefaultFields() {
        return {
          id: await generateEntityId(db.getDb(), this.name),
          menuIds: [],
          createdAt: Date.now(),
        };
      },
    })
  );
}

import { getResourceUrl } from 'src/common/utils';

/**
 * Gets product media public url
 * @param media media filename
 */
export function getProductMediaUrl(media: string): string {
  return getResourceUrl(`products/media/${media}`);
}

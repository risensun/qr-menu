export enum MediaType {
  Photo = 'PHOTO',
  Video = 'VIDEO',
}

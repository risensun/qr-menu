import { Tenant } from 'src/domains/tenant';
import { I18nField } from 'src/plugins/i18n';

export type Badge = {
  readonly id: string;
  readonly tenantId: Tenant['id'];
  readonly createdAt: number;

  name: I18nField<string>;
  key: string;
};

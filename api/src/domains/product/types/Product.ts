import { Category } from 'src/domains/category';
import { I18nField } from 'src/plugins/i18n';
import { Menu } from 'src/domains/menus';
import { Tenant } from 'src/domains/tenant';
import { Badge } from './Badge';
import { Option } from './Option';
import { MediaType } from './MediaType';

export type Product = {
  readonly id: number;
  readonly tenantId: Tenant['id'];
  readonly createdAt: number;

  categoryId: Category['id'];
  menuIds: Menu['id'][];
  media: {
    miniatureSrc: string;
    gallery: {
      type: MediaType;
      defaultSrc: string;
      modernSrc: string | null;
    }[];
  };
  title: I18nField<string>;
  ingredients: I18nField<string>;
  description: I18nField<string>;
  size: {
    name: I18nField<string>;
    unit: I18nField<string>;
    options: {
      key: string;
      name: I18nField<string>;
      value: number;
      price: number;
      nutrients: {
        kkal: number;
        proteins: number;
        fat: number;
        carbohydrate: number;
      };
      isActive: boolean;
    }[];
  };
  badges: Badge['id'][];
  options: Option['id'][];
};

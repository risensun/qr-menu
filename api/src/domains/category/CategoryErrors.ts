export enum CategoryErrors {
  TitleIsNotUnique = 'E_CATEGORY_TITLE_IS_NOT_UNIQUE',
  NotSetOfTrees = 'E_CATEGORY_NOT_SET_OF_TREES',
}

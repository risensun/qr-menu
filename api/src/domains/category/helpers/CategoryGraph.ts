import { Category } from '../types';

export class CategoryGraph {
  protected readonly graph: {
    readonly ids: number[];
    readonly nodes: { [id: number]: Category | undefined };
    readonly nodeToParentMap: { [id: number]: number | undefined };
  };
  protected wasMultipleParentsDetected: boolean;

  public constructor(protected readonly nodes: Category[]) {
    this.graph = {
      ids: [],
      nodes: {},
      nodeToParentMap: {},
    };
    this.wasMultipleParentsDetected = false;

    this.init();
  }

  public getNodeById(id: number): Category | undefined {
    return this.graph.nodes[id];
  }

  public getNodeChildren(id: number): Category[] {
    const node = this.getNodeById(id);

    if (!node) {
      return [];
    }

    return this.getNodesByIds(node.children);
  }

  public getNodeParent(id: number): Category | undefined {
    const parentId = this.graph.nodeToParentMap[id];

    if (!parentId) {
      return undefined;
    }

    return this.getNodeById(parentId);
  }

  public getRootNodeIds(): Category['id'][] {
    const rootNodeIds = this.graph.ids.filter(
      (id) => !this.graph.nodeToParentMap[id]
    );

    return rootNodeIds;
  }

  public getAllNodeIds(): Category['id'][] {
    return this.graph.ids;
  }

  public getNodesByIds(ids: number[]): Category[] {
    const nodes: Category[] = [];

    for (const id of ids) {
      const node = this.getNodeById(id);

      if (node) {
        nodes.push(node);
      }
    }

    return nodes;
  }

  public getPathToRoot(id: number): Category[] {
    const path: number[] = [];

    let parentId = this.graph.nodeToParentMap[id];

    while (parentId) {
      path.unshift(parentId);

      parentId = this.graph.nodeToParentMap[parentId];
    }

    return this.getNodesByIds(path);
  }

  public isEmpty(): boolean {
    return this.graph.ids.length === 0;
  }

  public isConsistFromTrees(): boolean {
    return !this.wasMultipleParentsDetected && !this.hasCycle();
  }

  protected init(): void {
    const nodeIdSet: Set<number> = new Set();

    for (const node of this.nodes) {
      this.graph.nodes[node.id] = node;
      nodeIdSet.add(node.id);

      const hasChildren = node.children.length !== 0;

      if (hasChildren) {
        for (const childId of node.children) {
          if (this.graph.nodeToParentMap[childId]) {
            this.wasMultipleParentsDetected = true;
          }

          this.graph.nodeToParentMap[childId] = node.id;
        }
      }
    }

    this.graph.ids.push(...Array.from(nodeIdSet.values()));
  }

  protected getNodeChildrenIds(id: number): number[] {
    return this.getNodeById(id)?.children || [];
  }

  protected hasCycle(): boolean {
    if (this.isEmpty()) {
      return false;
    }

    const visitednumbersSet: Set<number> = new Set();
    const path: Set<number> = new Set();

    for (const id of this.graph.ids) {
      if (this.isNodeInCycle(id, visitednumbersSet, path)) {
        return true;
      }
    }

    return false;
  }

  protected isNodeInCycle(
    id: number,
    visitedNodeIdSet: Set<number>,
    path: Set<number>
  ): boolean {
    const isInCycle = path.has(id);

    if (isInCycle) {
      return true;
    }

    const wasVisited = visitedNodeIdSet.has(id);

    if (wasVisited) {
      return false;
    }

    const node = this.getNodeById(id);

    if (!node) {
      return false;
    }

    path.add(node.id);

    for (const childId of node.children) {
      if (this.isNodeInCycle(childId, visitedNodeIdSet, path)) {
        return true;
      }
    }

    path.delete(id);

    return false;
  }
}

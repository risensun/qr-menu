import {
  NotFoundException,
  UpdateComposerBuilder,
  ValidationException,
} from '@glangeo/pollux';
import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { throwsException } from '@glangeo/pollux/utils';
import { Tenant } from 'src/domains/tenant';
import { Registry } from 'src/Registry';
import { CategoryErrors } from '../CategoryErrors';
import { getCategoryCollectionAdapter } from '../collections';
import { CategoryGraph } from '../helpers/CategoryGraph';
import { Catalogue, Category } from '../types';

export class CatalogueModel {
  protected readonly adapter: ReturnType<typeof getCategoryCollectionAdapter>;

  public constructor(db: MongoDB) {
    this.adapter = getCategoryCollectionAdapter(db);
  }

  /**
   * Creates new category
   * @param payload
   */
  public async create(
    payload: Pick<Category, 'tenantId' | 'title' | 'description'>
  ): Promise<Category> {
    await this.throwIfTitleIsNotUnique(payload.title);

    return this.adapter.create(payload);
  }

  /**
   * Gets all tenant categories
   * @param tenantId
   */
  public async getByTenantId(tenantId: Tenant['id']): Promise<Category[]> {
    return this.adapter.getMany({ tenantId });
  }

  /**
   * Gets category
   * @param id
   */
  public async getById(id: Category['id']): Promise<Category> {
    return this.adapter.getOne({ id });
  }

  /**
   * Creates tenant category catalogue.
   * If `categories` are provided, builds catalogue including only given
   * categories and their parents
   * @param tenantId
   * @param [categoryIds]
   */
  public async getCatalogue(
    tenantId: Tenant['id'],
    categoryIds: Category['id'][] = []
  ): Promise<Catalogue> {
    const catalogueCategories: Category[] = []; // May have duplicates!

    const tenantCategories = await this.adapter.getMany({ tenantId });

    // If categories are given, collect all their parents
    if (categoryIds.length > 0) {
      const graph = new CategoryGraph(tenantCategories);

      for (const id of categoryIds) {
        const category = graph.getNodeById(id);

        if (category) {
          const parentCategories = graph.getPathToRoot(id);

          catalogueCategories.push(category, ...parentCategories);
        }
      }
    } else {
      // Otherwise use all tenant categories
      catalogueCategories.push(...tenantCategories);
    }

    // Build graph based on collected categories
    const catalogueGraph = new CategoryGraph(catalogueCategories);

    // Remove extra children (needed when catalogueCategories is subset of tenant categories)
    const stripExtraChildren = (category: Category): Category => ({
      ...category,
      children: category.children.filter((id) =>
        Boolean(catalogueGraph.getNodeById(id))
      ),
    });

    // Project graph as an entity adapter
    return {
      ids: catalogueGraph.getAllNodeIds(),
      rootIds: catalogueGraph.getRootNodeIds(),
      categories: catalogueGraph
        .getNodesByIds(catalogueGraph.getAllNodeIds())
        .reduce(
          (acc, curr) => ({
            ...acc,
            [curr.id]: stripExtraChildren(curr),
          }),
          {}
        ),
    };
  }

  /**
   * Gets category update composer
   * @param category
   */
  public getUpdateComposer(category: Category) {
    return new UpdateComposerBuilder<Category>()
      .addSetter('title', async (title) => {
        await this.throwIfTitleIsNotUnique(title);

        return title;
      })
      .addSetter('description')
      .addSetter('children', async (nextChildren) => {
        const categories = await this.getByTenantId(category.tenantId);
        // Set new children to current category in array
        const nextCategories = categories.map((_category) =>
          _category.id === category.id
            ? { ..._category, children: nextChildren }
            : _category
        );

        // Build graph and validate that it consists from trees
        const graph = new CategoryGraph(nextCategories);

        if (!graph.isConsistFromTrees()) {
          throw new ValidationException({
            message: CategoryErrors.NotSetOfTrees,
            publicInfo: {
              message: CategoryErrors.NotSetOfTrees,
            },
          });
        }

        return nextChildren;
      })
      .build((changes) =>
        this.adapter.updateOne({ id: category.id }, { $set: changes })
      );
  }

  /**
   * Deletes category including all its children
   * @param category
   */
  public async delete(category: Category): Promise<boolean> {
    // Get all tenant categories
    const categories = await this.adapter.getMany({
      tenantId: category.tenantId,
    });
    const graph = new CategoryGraph(categories);

    // Remove category reference from its parent
    const parent = graph.getNodeParent(category.id);

    if (parent) {
      await this.adapter.updateOne(
        { id: parent.id },
        { $pull: { children: category.id } }
      );
    }

    // Get all category children (including nested)
    const categoryIdsToDelete: Category['id'][] = [];

    const fillCategoryIdsToDelete = (category: Category) => {
      categoryIdsToDelete.push(category.id);

      for (const child of graph.getNodeChildren(category.id)) {
        fillCategoryIdsToDelete(child);
      }
    };

    fillCategoryIdsToDelete(category);

    // Delete all products related to previously collected categories
    const model = Registry.getProductModel();

    const products = await model.getByCategoryIds(categoryIdsToDelete);
    await model.delete(products);

    return this.adapter.deleteMany({
      id: { $in: categoryIdsToDelete },
    });
  }

  /**
   * Throws exception if given title is not unique
   * @param title
   */
  protected async throwIfTitleIsNotUnique(
    title: Category['title']
  ): Promise<void> {
    const isTitleUnique = await throwsException(
      () => this.adapter.getOne({ title }),
      NotFoundException
    );

    if (!isTitleUnique) {
      throw new ValidationException({
        message: CategoryErrors.TitleIsNotUnique,
        publicInfo: {
          message: CategoryErrors.TitleIsNotUnique,
        },
      });
    }
  }
}

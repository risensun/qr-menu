import {
  createCollection,
  generateEntityId,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { omit } from 'lodash';
import { Category } from '../types';

export function getCategoryCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: 'categories',

      createEntityFromDBRecord(record: RecordSchema<Category>): Category {
        return omit(record, '_id');
      },

      async getRecordDefaultFields() {
        return {
          id: await generateEntityId(db.getDb(), this.name),
          createdAt: Date.now(),
          children: [],
        };
      },
    })
  );
}

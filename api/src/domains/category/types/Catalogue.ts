import { Category } from './Category';

export type Catalogue = {
  ids: Category['id'][];
  rootIds: Category['id'][];
  categories: {
    [id: number]: Category | undefined;
  };
};

import { Tenant } from 'src/domains/tenant';
import { I18nField } from 'src/plugins/i18n';

export type Category = {
  readonly id: number;
  readonly tenantId: Tenant['id'];
  readonly createdAt: number;

  title: I18nField<string>;
  description: I18nField<string | null>;
  children: Category['id'][];
};

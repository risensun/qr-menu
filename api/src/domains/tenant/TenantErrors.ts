export enum TenantErrors {
  TenantIdRequired = 'E_TENANT_TENANT_ID_REQUIRED',
  UserAccessDenied = 'E_TENANT_USER_ACCESS_DENIED',
}

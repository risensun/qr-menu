export enum MemberType {
  Owner = 40,
  Admin = 30,
  Manager = 20,
  Guest = 10,
  Customer = 0,
}

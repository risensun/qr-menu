import { User } from 'src/domains/user';
import { I18nField, Locale } from 'src/plugins/i18n';
import { MemberType } from './MemberType';

export type Tenant = {
  readonly id: string;
  readonly createdAt: number;
  readonly ownerId: User['id'];

  name: I18nField<string>;
  description: I18nField<string | null>;
  defaultLocale: Locale;
  locales: Locale[];
  members: {
    id: User['id'];
    type: MemberType;
  }[];
};

import { ValidationException } from '@glangeo/pollux';
import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { ObjectId } from 'mongodb';
import { User } from 'src/domains/user';
import { getTenantCollectionAdapter } from '../collections';
import { TenantErrors } from '../TenantErrors';

import { Tenant } from '../types';
import { MemberType } from '../types/MemberType';

export class TenantModel {
  protected readonly adapter: ReturnType<typeof getTenantCollectionAdapter>;

  public constructor(db: MongoDB) {
    this.adapter = getTenantCollectionAdapter(db);
  }

  /**
   * Creates new tenant
   * @param payload
   */
  public async create(
    payload: Pick<Tenant, 'ownerId' | 'name' | 'description' | 'defaultLocale'>
  ): Promise<Tenant> {
    const { ownerId, defaultLocale } = payload;

    return this.adapter.create({
      ...payload,
      locales: [defaultLocale],
      members: [
        {
          id: ownerId,
          type: MemberType.Owner,
        },
      ],
    });
  }

  /**
   * Gets tenant
   * @param id
   */
  public getById(id: Tenant['id']): Promise<Tenant> {
    return this.adapter.getOne({ _id: new ObjectId(id) });
  }

  /**
   * Gets user membership in the given tenant
   * @param tenant
   * @param userId
   */
  public getUserMembership(tenant: Tenant, userId: User['id']): MemberType {
    const member = tenant.members.find(({ id }) => id === userId);

    if (!member) {
      throw new ValidationException({
        message: TenantErrors.UserAccessDenied,
        publicInfo: {
          message: TenantErrors.UserAccessDenied,
        },
      });
    }

    return member.type;
  }
}

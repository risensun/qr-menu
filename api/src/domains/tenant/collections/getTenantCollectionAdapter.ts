import {
  createCollection,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { Tenant } from '../types';

export function getTenantCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: 'tenants',

      createEntityFromDBRecord(
        record: RecordSchema<Omit<Tenant, 'id'>>
      ): Tenant {
        const { _id, ...rest } = record;

        return {
          ...rest,
          id: _id.toHexString(),
        };
      },

      async getRecordDefaultFields() {
        return {
          createdAt: Date.now(),
        };
      },
    })
  );
}

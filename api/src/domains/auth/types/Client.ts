import { ClientType } from './ClientType';

export type Client<T extends ClientType = ClientType> = {
  readonly id: string;
  readonly type: T;
  readonly ownerId: string;
  readonly createdAt: number;

  csrfToken: string | null;
  updatedTokenAt: number | null;
  lastLoggedInAt: number | null;
  meta: T extends ClientType.User
    ? {
        ip: string;
        agent: {
          type: 'website' | 'mobile';
          info: string;
        };
      }
    : unknown;
};

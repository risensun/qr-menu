import { Client } from './Client';

export type RefreshTokenPayload = {
  readonly clientId: Client['id'];
};

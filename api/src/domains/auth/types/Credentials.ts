export type Credentials = {
  readonly id: string;
  readonly createdAt: number;

  login: string;
  password: string;
};

import { Client } from './Client';

export type AccessTokenPayload = {
  readonly clientId: Client['id'];
  readonly csrf: string;
};

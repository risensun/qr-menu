import { Exception, ValidationException } from '@glangeo/pollux';
import { getContext, HTTPStatusCode } from '@glangeo/pollux/api';
import { Middleware } from '@glangeo/pollux/core/types/api';
import { ContextWrapper } from 'src/common/ContextWrapper';
import { MemberType } from 'src/domains/tenant/types/MemberType';
import { Registry } from 'src/Registry';
import { AuthErrors } from '../AuthErrors';
import { ClientType } from '../types';

export type UseAuthParams = {
  /**
   * Specifies members that has rights to perform request.
   * If singe value is given authorizes all higher member types.
   * If array is given authorizes only specified member types
   * */
  member: MemberType | MemberType[];

  /**
   * If `false`, authorization to request is optional.
   * Defaults to `true`
   */
  isStrict?: boolean;
};

/**
 * Authorizes user requests
 * @param params
 */
export const useAuth = (params: UseAuthParams): Middleware => {
  const { member: requiredMemberType, isStrict = true } = params;

  const throwIfFailed = (meta: string[]) => {
    throw new ValidationException({
      message: AuthErrors.AuthorizationFailed,
      meta: {
        errors: meta,
      },
      publicInfo: {
        message: AuthErrors.AuthorizationFailed,
      },
      httpStatusCode: HTTPStatusCode.Unauthorized,
    });
  };

  return async (req, res, next) => {
    const context = getContext(req, res);

    try {
      const token = req.cookies['authorization'];

      if (!token) {
        throwIfFailed(['Token is not provided.']);
      }

      const wrapper = new ContextWrapper(context);
      const tenantModel = Registry.getTenantModel();
      const clientModel = Registry.getClientModel();

      const client = await clientModel.getByAccessToken(token);

      if (client.type !== ClientType.User) {
        throwIfFailed(['Client.type is not fit.']);
      }

      const tenantId = wrapper.getTenantId();
      const tenant = await tenantModel.getById(tenantId);

      const memberType = tenantModel.getUserMembership(tenant, client.ownerId);

      if (
        (Array.isArray(requiredMemberType) &&
          !requiredMemberType.includes(memberType)) ||
        memberType < requiredMemberType
      ) {
        throwIfFailed([
          'Current membership is not allow to perform this request.',
        ]);
      }

      const composer = clientModel.getUpdateComposer(client);

      client.lastLoggedInAt = Date.now();
      composer.setLastLoggedInAt(client.lastLoggedInAt);

      await composer.update();

      ContextWrapper.enhancer.setState(
        {
          ...ContextWrapper.enhancer.getState(context),
          client,
        },
        context
      );

      next();
    } catch (exception) {
      if (
        exception instanceof Exception &&
        exception.httpStatusCode === HTTPStatusCode.Unauthorized
      ) {
        if (isStrict) {
          throw exception;
        }

        next();
      }

      throw exception;
    }
  };
};

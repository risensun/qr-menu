import {
  createCollection,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { Client, ClientType } from '../types';

type ClientRecord<T extends ClientType = ClientType> = RecordSchema<
  Omit<Client<T>, 'id'>
>;

export function getClientCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: 'clients',

      createEntityFromDBRecord(record: ClientRecord): Client {
        const { _id, ...rest } = record;

        return {
          ...rest,
          id: _id.toHexString(),
        };
      },

      async getRecordDefaultFields() {
        return {
          createdAt: Date.now(),
          csrfToken: null,
          updatedTokenAt: null,
          lastLoggedInAt: null,
        };
      },
    })
  );
}

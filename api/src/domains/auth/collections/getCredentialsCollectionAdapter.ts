import {
  createCollection,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { Credentials } from '../types';

type CredentialsRecord = RecordSchema<Omit<Credentials, 'id'>>;

export function getCredentialsCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: 'credentials',

      createEntityFromDBRecord(record: CredentialsRecord): Credentials {
        const { _id, ...rest } = record;

        return {
          ...rest,
          id: _id.toHexString(),
        };
      },

      async getRecordDefaultFields() {
        return {
          createdAt: Date.now(),
        };
      },
    })
  );
}

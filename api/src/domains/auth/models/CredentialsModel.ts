import { NotFoundException, ValidationException } from '@glangeo/pollux';
import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { throwsException } from '@glangeo/pollux/utils';
import { PBKDF2 } from '@glangeo/pollux/plugins/auth';
import { getCredentialsCollectionAdapter } from '../collections';
import { AuthErrors } from '../AuthErrors';
import { Credentials } from '../types';

const SALT_SIZE = 32;

export class CredentialsModel {
  protected readonly adapter: ReturnType<
    typeof getCredentialsCollectionAdapter
  >;
  protected readonly pbkdf2: PBKDF2;

  public constructor(db: MongoDB) {
    this.adapter = getCredentialsCollectionAdapter(db);
    this.pbkdf2 = new PBKDF2({ salt: { size: SALT_SIZE } });
  }

  /**
   * Creates credentials
   * @param login
   * @param password
   */
  public async create(login: string, password: string): Promise<Credentials> {
    await this.throwIfLoginNotUnique(login);

    const salt = this.pbkdf2.generateSalt();
    const hash = this.getPasswordHash(password, salt);

    return this.adapter.create({ login, password: hash });
  }

  /**
   * Gets credentials
   * @param login
   */
  public async getByLogin(login: string): Promise<Credentials> {
    return this.adapter.getOne({ login });
  }

  /**
   * Compares credentials password with given password hash
   * @param credentials
   * @param passowrd
   */
  public arePasswordsEquals(
    credentials: Credentials,
    passowrd: string
  ): boolean {
    const salt = credentials.password.slice(0, SALT_SIZE * 2);
    const hash = this.getPasswordHash(passowrd, salt);

    return hash === credentials.password;
  }

  /**
   * Throws exception if collection with the given login exists
   * @param login
   */
  protected async throwIfLoginNotUnique(login: string): Promise<void> {
    const isLoginUnique = await throwsException(
      () => this.getByLogin(login),
      NotFoundException
    );

    if (!isLoginUnique) {
      throw new ValidationException({
        message: AuthErrors.LoginMustBeUnique,
        publicInfo: {
          message: AuthErrors.LoginMustBeUnique,
        },
      });
    }
  }

  /**
   * Gets password hash for storing in db
   * @param passowrd
   * @param salt
   */
  protected getPasswordHash(passowrd: string, salt: string): string {
    const hash = this.pbkdf2.getHash(passowrd.normalize(), salt);

    return `${salt}${hash}`;
  }
}

import { UpdateComposerBuilder, ValidationException } from '@glangeo/pollux';
import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { CSRF, JWT } from '@glangeo/pollux/plugins/auth';
import { ObjectId } from 'mongodb';
import { AuthErrors } from '../AuthErrors';
import { getClientCollectionAdapter } from '../collections';
import { AccessTokenPayload, Client, ClientType } from '../types';
import { RefreshTokenPayload } from '../types/RefreshTokenPayload';

const SALT_SIZE = 32;

export class ClientModel {
  protected readonly adapter: ReturnType<typeof getClientCollectionAdapter>;
  protected readonly csrf: CSRF;
  protected readonly refreshTokenJwt: JWT<RefreshTokenPayload>;
  protected readonly accessTokenJwt: JWT<AccessTokenPayload>;

  public constructor(
    db: MongoDB,
    refreshTokenSecret: string,
    accessTokenSecret: string
  ) {
    this.adapter = getClientCollectionAdapter(db);
    this.csrf = new CSRF(SALT_SIZE);
    this.refreshTokenJwt = new JWT(refreshTokenSecret);
    this.accessTokenJwt = new JWT(accessTokenSecret);
  }

  /**
   * Creates client
   * @param type
   * @param meta
   */
  public async create<T extends ClientType>(
    type: T,
    ownerId: string,
    meta: Client<T>['meta']
  ): Promise<Client<T>> {
    const client = await this.adapter.create({
      type,
      ownerId,
      meta,
    });

    return client as Client<T>;
  }

  /**
   * Gets client
   * @param id
   */
  public async getById(id: Client['id']): Promise<Client> {
    return this.adapter.getOne({ _id: new ObjectId(id) });
  }

  /**
   * Gets client update composer
   * @param client
   */
  public getUpdateComposer<T extends ClientType>(client: Client<T>) {
    return new UpdateComposerBuilder<Client<T>>()
      .addSetter('csrfToken')
      .addSetter('lastLoggedInAt')
      .addSetter('updatedTokenAt')
      .addSetter('meta')
      .build((changes) =>
        this.adapter.updateOne(
          { _id: new ObjectId(client.id) },
          { $set: changes }
        )
      );
  }

  /**
   * Gets refresh token associated with given client
   * @param client
   */
  public getRefreshToken(client: Client): string {
    const token = this.refreshTokenJwt.getToken(
      { clientId: client.id },
      { expiresIn: '1y' }
    );

    return token;
  }

  /**
   * Gets client from associated refresh token
   * @param token
   */
  public async getByRefreshToken(token: string): Promise<Client> {
    const { clientId } = this.refreshTokenJwt.decodeToken(token);
    const client = await this.getById(clientId);

    return client;
  }

  /**
   * Gets access token associated with given client
   * @param client
   */
  public async getAccessToken(client: Client): Promise<string> {
    const csrfToken = this.csrf.getToken();
    const token = this.accessTokenJwt.getToken(
      {
        clientId: client.id,
        csrf: csrfToken,
      },
      { expiresIn: '24h' }
    );

    const composer = this.getUpdateComposer(client);

    composer.setCsrfToken(csrfToken);
    composer.setUpdatedTokenAt(Date.now());

    await composer.update();

    return token;
  }

  /**
   * Gets client from associated access token
   * @param token
   * @param ip
   */
  public async getByAccessToken(token: string): Promise<Client> {
    const { clientId, csrf } = this.accessTokenJwt.decodeToken(token);
    const client = await this.getById(clientId);

    if (client.csrfToken !== csrf) {
      throw new ValidationException({
        message: AuthErrors.ClientCSRFNotValid,
        publicInfo: {
          message: AuthErrors.ClientCSRFNotValid,
        },
      });
    }

    return client;
  }
}

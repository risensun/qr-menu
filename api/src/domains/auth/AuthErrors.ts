export enum AuthErrors {
  AuthorizationFailed = 'E_AUTH_FAILED',
  OtherClientTypeRequired = 'E_AUTH_WRONG_CLIENT_TYPE',
  ClientCSRFNotValid = 'E_AUTH_CLIENT_CSRF_NOT_VALID',
  LoginMustBeUnique = 'E_AUTH_LOGIN_MUST_BE_UNIQUE',
  LoginOrPasswordWrong = 'E_AUTH_LOGIN_OR_PASSWORD_WRONG',
}

import {
  createCollection,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { User } from '../types';

export function getUserCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: 'users',

      createEntityFromDBRecord(record: RecordSchema<Omit<User, 'id'>>): User {
        const { _id, ...rest } = record;

        return {
          ...rest,
          id: _id.toHexString(),
        };
      },

      async getRecordDefaultFields() {
        return {
          createdAt: Date.now(),
          isActive: true,
        };
      },
    })
  );
}

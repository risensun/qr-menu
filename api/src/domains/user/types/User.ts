export type User = {
  readonly id: string;
  readonly email: string;
  readonly createdAt: number;

  firstName: string;
  lastName: string;
};

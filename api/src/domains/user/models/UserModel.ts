import { NotFoundException, ValidationException } from '@glangeo/pollux';
import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { throwsException } from '@glangeo/pollux/utils';
import { ObjectId } from 'mongodb';
import { getUserCollectionAdapter } from '../collections/getUserCollectionAdapter';
import { User } from '../types';
import { UserErros } from '../UserErrors';

export class UserModel {
  protected readonly adapter: ReturnType<typeof getUserCollectionAdapter>;

  public constructor(db: MongoDB) {
    this.adapter = getUserCollectionAdapter(db);
  }

  public async create(
    payload: Pick<User, 'email' | 'firstName' | 'lastName'>
  ): Promise<User> {
    const isEmailUnique = await throwsException(
      () => this.getByEmail(payload.email),
      NotFoundException
    );

    if (!isEmailUnique) {
      throw new ValidationException({
        message: UserErros.EmailNotUnique,
        meta: {
          errors: [`Email: ${payload.email}`],
        },
        publicInfo: {
          message: UserErros.EmailNotUnique,
        },
      });
    }

    return this.adapter.create(payload);
  }

  public async getByEmail(email: User['email']): Promise<User> {
    return this.adapter.getOne({ email });
  }

  public async getById(id: string): Promise<User> {
    return this.adapter.getOne({ _id: new ObjectId(id) });
  }
}

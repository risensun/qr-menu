import {
  createCollection,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { Menu } from '../types';

export type MenuRecord = RecordSchema<Omit<Menu, 'id'>>;

export function getMenuCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: 'menus',

      createEntityFromDBRecord(record: MenuRecord): Menu {
        const { _id, ...rest } = record;

        return {
          ...rest,
          id: _id.toHexString(),
        };
      },

      async getRecordDefaultFields() {
        return {
          products: [],
          catalogueCache: {
            categories: [],
            updatedAt: Date.now(),
          },
          isPublished: false,
          createdAt: Date.now(),
        };
      },
    })
  );
}

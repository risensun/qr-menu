import { NotFoundException, UpdateComposerBuilder } from '@glangeo/pollux';
import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { ObjectId } from 'mongodb';
import { Catalogue, Category } from 'src/domains/category';
import { Product } from 'src/domains/product';
import { Tenant } from 'src/domains/tenant';
import { Registry } from 'src/Registry';
import { getMenuCollectionAdapter } from '../collections';
import { Menu } from '../types';

export class MenuModel {
  protected readonly adapter: ReturnType<typeof getMenuCollectionAdapter>;

  public constructor(protected readonly db: MongoDB) {
    this.adapter = getMenuCollectionAdapter(db);
  }

  /**
   * Creates new menu in tenant
   * @param payload
   */
  public async create(
    payload: Pick<Menu, 'title' | 'tenantId'>
  ): Promise<Menu> {
    return this.adapter.create(payload);
  }

  /**
   * Gets menu
   * @param id
   */
  public async getById(id: Menu['id']): Promise<Menu> {
    return this.adapter.getOne({ _id: new ObjectId(id) });
  }

  /**
   * Gets menu by tenant id
   * @param tenantId
   */
  public async getByTenantId(tenantId: Tenant['id']): Promise<Menu[]> {
    return this.adapter.getMany({ tenantId });
  }

  /**
   * Gets catalogue of products added to menu. Gets only categories and forms tree-like structure
   * @param menu
   * @param shouldIncludeUnpublished include to catalogue unpublished products and categories. `false` by default
   */
  public async getCatalogue(
    menu: Menu,
    shouldIncludeUnpublished = false
  ): Promise<Catalogue> {
    const categoryIds: Category['id'][] = [];

    for (const product of menu.products) {
      const shouldIncludeProduct =
        product.isPublished || shouldIncludeUnpublished;

      if (shouldIncludeProduct) {
        categoryIds.push(product.categoryId);
      }
    }

    const catalogue = await Registry.getCategoryModel().getCatalogue(
      menu.tenantId,
      categoryIds
    );

    return catalogue;
  }

  /**
   * Gets product from menu and checks if it is published
   * @param menu
   * @param productId
   * @param shouldAllowUnpublished allow to get unpublished product from menu. `false` by default
   */
  public async getProductById(
    menu: Menu,
    productId: Product['id'],
    shouldAllowUnpublished = false
  ): Promise<Product> {
    const product = await menu.products.find(({ id }) => id === productId);

    if (!product || (!shouldAllowUnpublished && !product.isPublished)) {
      throw new NotFoundException({
        message: 'Could not find product',
      });
    }

    return Registry.getProductModel().getById(productId);
  }

  /**
   * Gets products from menu category and all its child categories
   * @param menu
   * @param categoryId
   * @param shouldIncludeUnpublished include unpublished products
   */
  public async getCategoryProducts(
    menu: Menu,
    categoryId: Category['id'],
    shouldIncludeUnpublished = false
  ): Promise<Product[]> {
    // Probably need to cache this in future
    const catalogue = await this.getCatalogue(menu, shouldIncludeUnpublished);

    const category = catalogue.categories[categoryId];

    if (!category) {
      throw new NotFoundException({
        message: 'Could find category',
      });
    }

    const categoryIdSet: Set<Category['id']> = new Set();

    const fillCategoryIdSet = (category: Category): void => {
      categoryIdSet.add(category.id);

      for (const id of category.children) {
        if (!categoryIdSet.has(id)) {
          const category = catalogue.categories[id];

          if (category) {
            fillCategoryIdSet(category);
          }
        }
      }
    };

    // Fill categoryIdSet with ids current category and all nested categories
    fillCategoryIdSet(category);

    // Select products that are stored in current category and its children
    const productIds: Product['id'][] = [];

    for (const product of menu.products) {
      const shouldInclude = product.isPublished || shouldIncludeUnpublished;

      if (shouldInclude && categoryIdSet.has(product.categoryId)) {
        productIds.push(product.id);
      }
    }

    return Registry.getProductModel().getByIds(productIds);
  }

  /**
   * Gets update composer
   * @param menu
   */
  public getUpdateComposer(menu: Menu) {
    return new UpdateComposerBuilder<Menu>()
      .addSetter('products', async (nextProducts) => {
        // Get deleted and added product ids
        const nextProductIdSet: Set<Product['id']> = new Set(
          nextProducts.map(({ id }) => id)
        );
        const currentProductIdSet: Set<Product['id']> = new Set(
          menu.products.map(({ id }) => id)
        );

        const deletedProductIds = menu.products.filter(
          ({ id }) => !nextProductIdSet.has(id)
        );
        const addedProductIds = nextProducts.filter(
          ({ id }) => !currentProductIdSet.has(id)
        );

        // Add menu to added products
        if (deletedProductIds.length) {
          await Registry.getProductModel().reactToMenuProductDeletion(
            deletedProductIds.map(({ id }) => id),
            menu
          );
        }

        // Remove menu from deleted products
        if (addedProductIds.length) {
          await Registry.getProductModel().reactToMenuProductAddition(
            addedProductIds.map(({ id }) => id),
            menu
          );
        }

        return nextProducts;
      })
      .build((changes) =>
        this.adapter.updateOne(
          { _id: new ObjectId(menu.id) },
          { $set: changes }
        )
      );
  }

  /**
   * Deletes menu
   *
   * @param menu
   */
  public async delete(menu: Menu): Promise<boolean> {
    await Registry.getProductModel().reactToMenuDeletion(menu);

    return this.adapter.deleteOne({ _id: new ObjectId(menu.id) });
  }

  /* --- Side Effects --- */

  /**
   * Removes deleted products from all affected menus
   * @param products
   */
  public async reactToProductsDeletion(products: Product[]): Promise<boolean> {
    const affectedMenuIds: ObjectId[] = [];
    const productIds: Product['id'][] = [];

    for (const product of products) {
      productIds.push(product.id);
      affectedMenuIds.push(...product.menuIds.map((id) => new ObjectId(id)));
    }

    return this.adapter.updateOne(
      {
        _id: { $in: affectedMenuIds },
      },
      {
        $pull: {
          products: { id: { $in: productIds } },
        },
      }
    );
  }

  /**
   * Updates category of products in menu
   * @param products
   * @param categoryId
   */
  public async reactToProductsCategoryChange(
    products: Product[],
    categoryId: Category['id']
  ): Promise<boolean> {
    const affectedMenuIds: ObjectId[] = [];
    const productIds: Product['id'][] = [];

    for (const product of products) {
      productIds.push(product.id);
      affectedMenuIds.push(...product.menuIds.map((id) => new ObjectId(id)));
    }

    return this.adapter.updateOne(
      {
        _id: { $in: affectedMenuIds, 'products.id': { $in: productIds } },
      },
      {
        $set: {
          'products.$.categoryId': { categoryId },
        },
      }
    );
  }
}

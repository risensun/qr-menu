import { Category } from 'src/domains/category';
import { Product } from 'src/domains/product';
import { Tenant } from 'src/domains/tenant';
import { I18nField } from 'src/plugins/i18n';

export type Menu = {
  readonly id: string;
  readonly tenantId: Tenant['id'];
  readonly createdAt: number;

  /**
   * Name of menu. Is not visible for customer
   */
  title: I18nField<string>;

  /**
   * Published menus are available on web
   */
  isPublished: boolean;

  /**
   * Products added to menu
   */
  products: {
    id: Product['id'];
    categoryId: Category['id'];
    isPublished: boolean;
  }[];
};

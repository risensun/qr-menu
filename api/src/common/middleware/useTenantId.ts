import { ValidationException } from '@glangeo/pollux';
import { getContext } from '@glangeo/pollux/api';
import { Middleware } from '@glangeo/pollux/core/types/api';
import { TenantErrors } from 'src/domains/tenant/TenantErrors';
import { ContextWrapper } from '../ContextWrapper';

export const useTenantId = (): Middleware => (req, res, next) => {
  const context = getContext(req, res);
  const state = ContextWrapper.enhancer.getState(context);

  const tenantId = req.headers['x-tenant-id'];

  if (typeof tenantId !== 'string') {
    throw new ValidationException({
      message: 'Tenant ID is required.',
      publicInfo: {
        message: TenantErrors.TenantIdRequired,
      },
    });
  }

  ContextWrapper.enhancer.setState(
    {
      ...state,
      tenantId,
    },
    context
  );

  next();
};

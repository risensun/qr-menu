import fs from 'fs/promises';
import path from 'path';
import crypto from 'crypto';
import multer from 'multer';

const ALLOWED_MIME_TYPES = ['image/jpeg', 'image/png'];

/**
 * Provides API for quick multer setup
 */
export class MulterStorage {
  private readonly path: string;
  private readonly upload: multer.Multer;

  public constructor(
    /**
     * Used to separate different multer storages
     */
    public readonly key: string,

    /**
     * Name of the folder where all uploaded files are stored
     */
    storageDirectory: string
  ) {
    this.path = path.join(process.cwd(), storageDirectory, key);

    this.upload = this.configureUpload();
  }

  /**
   * Gets multer upload middleware.
   * **Notice**: it is preferably to add multer upload middleware at the end of the middleware list
   * because if error occures you need to delete uploaded files manually
   */
  public getUpload(): multer.Multer {
    return this.upload;
  }

  /**
   * Deletes file from local storage
   * @param fileName file name to delete
   */
  public async deleteFileByName(fileName: string): Promise<void> {
    await fs.unlink(path.join(this.path, fileName));
  }

  /**
   * Configures multer storage
   */
  private configureStorage(): multer.StorageEngine {
    const storage = multer.diskStorage({
      // When passing a string in 'destination' field, multer creates folder by itself
      destination: this.path,

      filename: (_, file, callback) => {
        const filename = [
          file.fieldname,
          Date.now(),
          crypto.randomBytes(16),
        ].join('-');

        const extension = file.originalname.slice(
          file.originalname.lastIndexOf('.') + 1
        );
        const hash = crypto.createHash('md5').update(filename).digest('hex');

        callback(null, `${hash}.${extension}`);
      },
    });

    return storage;
  }

  /**
   * Configures upload middleware
   */
  private configureUpload(): multer.Multer {
    const upload = multer({
      storage: this.configureStorage(),
      limits: {
        fileSize: 10 * 1024 * 1024, // 10MB
        fields: 10,
        files: 10,
        parts: 10,
        headerPairs: 25,
      },

      fileFilter: (req, file, callback) => {
        if (!ALLOWED_MIME_TYPES.includes(file.mimetype)) {
          callback(null, false);

          return;
        }

        callback(null, true);
      },
    });

    return upload;
  }
}

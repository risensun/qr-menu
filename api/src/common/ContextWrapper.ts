import { ValidationException } from '@glangeo/pollux';
import { Context, createContextEnhancer } from '@glangeo/pollux/api';
import { AuthErrors } from 'src/domains/auth/AuthErrors';
import { Client, ClientType } from 'src/domains/auth/types';
import { Tenant } from 'src/domains/tenant';
import { TenantErrors } from 'src/domains/tenant/TenantErrors';
import { Locale } from 'src/plugins/i18n';

export type ContextEnhancedState = {
  locale: Locale;
  tenantId: Tenant['id'] | undefined;
  client: Client | undefined;
};

export class ContextWrapper {
  public static readonly enhancer =
    createContextEnhancer<ContextEnhancedState>();

  protected readonly state: ContextEnhancedState;

  public constructor(context: Context) {
    this.state = ContextWrapper.enhancer.getState(context);
  }

  public getLocale(): Locale | undefined {
    return this.state.locale;
  }

  public getTenantId(): string {
    const tenantId = this.state.tenantId;

    if (!tenantId) {
      throw new ValidationException({
        message: TenantErrors.TenantIdRequired,
        publicInfo: {
          message: TenantErrors.TenantIdRequired,
        },
      });
    }

    return tenantId;
  }

  public getClient<T extends boolean = true>(
    shouldThrowIfNotPresent?: T
  ): T extends true ? Client : Client | undefined {
    const _shouldThrowIfNotPresent =
      typeof shouldThrowIfNotPresent === 'undefined'
        ? true
        : shouldThrowIfNotPresent;
    const client = this.state.client;

    if (_shouldThrowIfNotPresent && !client) {
      throw new ValidationException({
        message: AuthErrors.AuthorizationFailed,
        publicInfo: {
          message: AuthErrors.AuthorizationFailed,
        },
      });
    }

    return client as any;
  }

  public getClientWithType<T extends ClientType, R extends boolean = true>(
    type: T,
    shouldThrowIfNotPresent?: R
  ): R extends true ? Client<T> : Client<T> | undefined {
    const client = this.getClient(shouldThrowIfNotPresent);

    if (client && client.type !== type) {
      throw new ValidationException({
        message: AuthErrors.OtherClientTypeRequired,
        publicInfo: {
          message: AuthErrors.OtherClientTypeRequired,
        },
      });
    }

    return client as any;
  }
}

import { Config } from 'src/Config';

/**
 * Gets resource public url
 * @param name resource name
 */
export function getResourceUrl(name: string): string {
  return `${Config.getCdnUrl()}/${name}`;
}

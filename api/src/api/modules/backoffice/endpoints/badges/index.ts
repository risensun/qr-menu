import * as Yup from 'yup';
import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { ContextWrapper } from 'src/common/ContextWrapper';
import { useTenantId } from 'src/common/middleware/useTenantId';
import { useAuth } from 'src/domains/auth';
import { MemberType } from 'src/domains/tenant/types/MemberType';
import { createDefaultLocaleField, Locale, localize } from 'src/plugins/i18n';
import { Registry } from 'src/Registry';

export default [
  createEndpoint({
    method: EndpointMethod.GET,
    middlewares: [useTenantId()],

    action: async (_, context) => {
      const wrapper = new ContextWrapper(context);
      const tenantId = wrapper.getTenantId();
      const model = Registry.getBadgeModel();

      const badges = await model.getByTenantId(tenantId);

      return { entities: badges.map((badge) => localize(badge, Locale.RU)) };
    },
  }),

  createEndpoint({
    method: EndpointMethod.POST,
    validation: {
      query: undefined,
      params: undefined,
      body: Yup.object({
        name: Yup.string().required(),
        key: Yup.string().required(),
      }),
    },
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async ({ body: { name, key } }, context) => {
      const wrapper = new ContextWrapper(context);
      const tenantId = wrapper.getTenantId();
      const model = Registry.getBadgeModel();

      const badge = await model.create({
        name: createDefaultLocaleField(name),
        key,
        tenantId,
      });

      return { entity: localize(badge, Locale.RU) };
    },
  }),
];

import * as Yup from 'yup';
import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { localize, Locale } from 'src/plugins/i18n';
import { Registry } from 'src/Registry';
import { useAuth } from 'src/domains/auth';
import { useTenantId } from 'src/common/middleware/useTenantId';
import { MemberType } from 'src/domains/tenant/types/MemberType';

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    query: undefined,
    params: Yup.object({
      id: Yup.string().required(),
    }),
  },
  middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

  action: async ({ params: { id } }) => {
    const model = Registry.getMenuModel();

    const menu = await model.getById(id);
    const catalogue = await model.getCatalogue(menu, true);

    return {
      entity: localize(catalogue, Locale.RU),
    };
  },
});

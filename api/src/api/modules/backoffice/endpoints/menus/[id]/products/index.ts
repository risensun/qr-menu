import * as Yup from 'yup';
import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useTenantId } from 'src/common/middleware/useTenantId';
import { Registry } from 'src/Registry';
import { useAuth } from 'src/domains/auth';
import { MemberType } from 'src/domains/tenant/types/MemberType';

const params = Yup.object({
  id: Yup.string().required(),
});

const body = Yup.object({
  productIds: Yup.array(Yup.number().positive().required()).required(),
});

export default [
  createEndpoint({
    method: EndpointMethod.POST,
    validation: {
      query: undefined,
      params,
      body,
    },
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async ({
      params: { id },
      body: { productIds: productIdsFromRequest },
    }) => {
      const model = Registry.getMenuModel();

      const menu = await model.getById(id);
      const productIds = productIdsFromRequest.filter(
        (id) => !menu.products.find((product) => id === product.id)
      );

      if (productIds.length === 0) {
        return { isSucceeded: false };
      }

      const composer = model.getUpdateComposer(menu);
      const products = await Registry.getProductModel().getByIds(productIds);

      const nextProducts = [...menu.products];

      for (const product of products) {
        nextProducts.push({
          id: product.id,
          categoryId: product.categoryId,
          isPublished: false,
        });
      }

      await composer.setProducts(nextProducts);

      const isSucceeded = await composer.update();

      return { isSucceeded };
    },
  }),

  createEndpoint({
    method: EndpointMethod.DELETE,
    validation: {
      query: undefined,
      params,
      body,
    },
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async ({
      params: { id },
      body: { productIds: productIdsFromRequest },
    }) => {
      const model = Registry.getMenuModel();

      const menu = await model.getById(id);
      const composer = model.getUpdateComposer(menu);

      const nextProducts = menu.products.filter(
        ({ id }) => !productIdsFromRequest.includes(id)
      );

      await composer.setProducts(nextProducts);

      const isSucceeded = await composer.update();

      return { isSucceeded };
    },
  }),
];

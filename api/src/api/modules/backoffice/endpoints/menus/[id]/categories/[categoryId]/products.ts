import * as Yup from 'yup';
import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { ProductProjectionDefault } from 'src/api/projections/product/Default';
import { localize, Locale } from 'src/plugins/i18n';
import { project } from 'src/plugins/projection';
import { Registry } from 'src/Registry';
import { Product } from 'src/domains/product';
import { useTenantId } from 'src/common/middleware/useTenantId';
import { useAuth } from 'src/domains/auth';
import { MemberType } from 'src/domains/tenant/types/MemberType';

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    query: undefined,
    params: Yup.object({
      id: Yup.string().required(),
      categoryId: Yup.number().positive().required(),
    }),
  },
  middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

  action: async ({ params: { id, categoryId } }) => {
    const model = Registry.getMenuModel();

    const menu = await model.getById(id);
    const products = await model.getCategoryProducts(menu, categoryId, true);

    const productIdToPublishedMap: Map<Product['id'], boolean> = new Map(
      menu.products.map(({ id, isPublished }) => [id, isPublished])
    );

    return {
      entities: products.map((product) =>
        localize(
          {
            ...project(product, ProductProjectionDefault),
            isPublished: productIdToPublishedMap.get(product.id) || false,
          },
          Locale.RU
        )
      ),
    };
  },
});

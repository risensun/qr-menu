import * as Yup from 'yup';
import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { Registry } from 'src/Registry';
import { useAuth } from 'src/domains/auth';
import { MemberType } from 'src/domains/tenant/types/MemberType';
import { useTenantId } from 'src/common/middleware/useTenantId';

const params = Yup.object({
  id: Yup.string().required(),
});

export default createEndpoint({
  method: EndpointMethod.PATCH,
  validation: {
    query: undefined,
    params,
    body: Yup.object({
      productIds: Yup.array(Yup.number().positive().required()).required(),
      isPublished: Yup.bool().required(),
    }),
  },
  middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

  action: async ({ params: { id }, body: { productIds, isPublished } }) => {
    const model = Registry.getMenuModel();

    const menu = await model.getById(id);
    const composer = model.getUpdateComposer(menu);

    const nextProducts = menu.products.map((product) =>
      productIds.includes(product.id)
        ? {
            ...product,
            isPublished,
          }
        : product
    );

    await composer.setProducts(nextProducts);

    const isSucceeded = await composer.update();

    return { isSucceeded };
  },
});

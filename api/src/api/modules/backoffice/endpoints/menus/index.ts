import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { ContextWrapper } from 'src/common/ContextWrapper';
import { useTenantId } from 'src/common/middleware/useTenantId';
import { useAuth } from 'src/domains/auth';
import { MemberType } from 'src/domains/tenant/types/MemberType';
import { Locale, localize } from 'src/plugins/i18n';
import { Registry } from 'src/Registry';

export default createEndpoint({
  method: EndpointMethod.GET,
  middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

  action: async (_, context) => {
    const wrapper = new ContextWrapper(context);
    const tenantId = wrapper.getTenantId();

    const menus = await Registry.getMenuModel().getByTenantId(tenantId);

    return {
      entities: menus.map((menu) => localize(menu, Locale.RU)),
    };
  },
});

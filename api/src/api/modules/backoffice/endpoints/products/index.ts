import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { ProductProjectionDefault } from 'src/api/projections/product/Default';
import { ContextWrapper } from 'src/common/ContextWrapper';
import { useTenantId } from 'src/common/middleware/useTenantId';
import { useAuth } from 'src/domains/auth';
import { MemberType } from 'src/domains/tenant/types/MemberType';
import { createDefaultLocaleField, Locale, localize } from 'src/plugins/i18n';
import { project } from 'src/plugins/projection';
import { Registry } from 'src/Registry';
import * as Yup from 'yup';

export default [
  createEndpoint({
    method: EndpointMethod.GET,
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async (_, context) => {
      const wrapper = new ContextWrapper(context);
      const tenantId = wrapper.getTenantId();

      const products = await Registry.getProductModel().getByTenantId(tenantId);

      return {
        entities: products.map((product) =>
          localize(project(product, ProductProjectionDefault), Locale.RU)
        ),
      };
    },
  }),

  createEndpoint({
    method: EndpointMethod.POST,
    validation: {
      query: undefined,
      params: undefined,
      body: Yup.object({
        categoryId: Yup.number().positive().required(),
        title: Yup.string().required(),
        ingredients: Yup.string().required(),
        description: Yup.string().required(),
        size: Yup.object({
          name: Yup.string().required(),
          unit: Yup.string().required(),
          options: Yup.array(
            Yup.object({
              name: Yup.string().required(),
              value: Yup.number().positive().required(),
              price: Yup.number().positive().required(),
              nutrients: Yup.object({
                kkal: Yup.number().positive().required(),
                proteins: Yup.number().positive().required(),
                fat: Yup.number().positive().required(),
                carbohydrate: Yup.number().positive().required(),
              }).required(),
              isActive: Yup.boolean().required(),
            }).required()
          )
            .min(1)
            .required(),
        }).required(),
        badges: Yup.array(Yup.string().required()).required(),
      }),
    },
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async ({ body }, context) => {
      const wrapper = new ContextWrapper(context);
      const tenantId = wrapper.getTenantId();
      const model = Registry.getProductModel();

      const { categoryId, title, ingredients, description, size, badges } =
        body;

      const product = await model.create({
        tenantId,
        categoryId,
        title: createDefaultLocaleField(title),
        ingredients: createDefaultLocaleField(ingredients),
        description: createDefaultLocaleField(description),
        size: {
          name: createDefaultLocaleField(size.name),
          unit: createDefaultLocaleField(size.unit),
          options: size.options.map((option) => ({
            ...option,
            name: createDefaultLocaleField(option.name),
            key: '',
          })),
        },
        badges,
        options: [],
        media: {
          miniatureSrc: '',
          gallery: [],
        },
      });

      return { entity: localize(product, Locale.RU) };
    },
  }),
];

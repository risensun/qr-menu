import { ValidationException } from '@glangeo/pollux';
import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useTenantId } from 'src/common/middleware/useTenantId';
import { useAuth } from 'src/domains/auth';
import { ProductErrors } from 'src/domains/product/ProductErrors';
import { MemberType } from 'src/domains/tenant/types/MemberType';
import { Registry } from 'src/Registry';
import * as Yup from 'yup';

const params = Yup.object({
  id: Yup.number().positive().required(),
});

export default createEndpoint({
  method: EndpointMethod.PATCH,
  validation: {
    query: undefined,
    params,
    body: undefined,
  },
  middlewares: [
    useTenantId(),
    useAuth({ member: MemberType.Manager }),
    Registry.getProductMediaStorage().getUpload().single('media'),
  ],

  action: async ({ params: { id } }, _, req) => {
    try {
      if (!req.file) {
        throw new ValidationException({
          message: ProductErrors.PhotoNotUploaded,
          publicInfo: {
            message: ProductErrors.PhotoNotUploaded,
          },
        });
      }

      const model = Registry.getProductModel();

      const product = await model.getById(id);
      const composer = model.getUpdateComposer(product);

      await composer.setMedia({
        miniatureSrc: req.file.filename,
        gallery: [],
      });

      const isSucceeded = await composer.update();

      return { isSucceeded };
    } catch (error) {
      if (req.file) {
        await Registry.getProductMediaStorage().deleteFileByName(
          req.file.filename
        );
      }

      throw error;
    }
  },
});

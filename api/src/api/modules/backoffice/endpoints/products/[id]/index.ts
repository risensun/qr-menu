import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useTenantId } from 'src/common/middleware/useTenantId';
import { useAuth } from 'src/domains/auth';
import { MemberType } from 'src/domains/tenant/types/MemberType';
import { createDefaultLocaleField } from 'src/plugins/i18n';
import { Registry } from 'src/Registry';
import * as Yup from 'yup';

const params = Yup.object({
  id: Yup.number().positive().required(),
});

export default [
  createEndpoint({
    method: EndpointMethod.PATCH,
    validation: {
      query: undefined,
      params,
      body: Yup.object({
        categoryId: Yup.number().positive().optional(),
        title: Yup.string().optional(),
        ingredients: Yup.string().optional(),
        description: Yup.string().optional(),
        size: Yup.object({
          name: Yup.string().required(),
          unit: Yup.string().required(),
          options: Yup.array(
            Yup.object({
              name: Yup.string().required(),
              value: Yup.number().positive().required(),
              price: Yup.number().positive().required(),
              nutrients: Yup.object({
                kkal: Yup.number().positive().required(),
                proteins: Yup.number().positive().required(),
                fat: Yup.number().positive().required(),
                carbohydrate: Yup.number().positive().required(),
              }).required(),
              isActive: Yup.boolean().required(),
            }).required()
          )
            .min(1)
            .required(),
        }).optional(),
        badges: Yup.array(Yup.string().required()).optional(),
      }),
    },
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async ({ params: { id }, body }) => {
      const model = Registry.getProductModel();

      const product = await model.getById(id);
      const composer = model.getUpdateComposer(product);

      const { categoryId, title, description, ingredients, size, badges } =
        body;

      if (categoryId) {
        composer.setCategoryId(categoryId);
      }

      if (title) {
        composer.setTitle(createDefaultLocaleField(title));
      }

      if (description) {
        composer.setDescription(createDefaultLocaleField(description));
      }

      if (ingredients) {
        composer.setIngredients(createDefaultLocaleField(ingredients));
      }

      if (size) {
        composer.setSize({
          ...size,
          name: createDefaultLocaleField(size.name),
          unit: createDefaultLocaleField(size.unit),
          options: size.options.map((option) => ({
            ...option,
            name: createDefaultLocaleField(option.name),
            key: '',
          })),
        });
      }

      if (badges) {
        composer.setBadges(badges);
      }

      const isSucceeded = await composer.update();

      return { isSucceeded };
    },
  }),

  createEndpoint({
    method: EndpointMethod.DELETE,
    validation: {
      query: undefined,
      params,
      body: undefined,
    },
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async ({ params: { id } }) => {
      const model = Registry.getProductModel();

      const product = await model.getById(id);
      const isSucceeded = await model.delete([product]);

      return { isSucceeded };
    },
  }),
];

import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useTenantId } from 'src/common/middleware/useTenantId';
import { useAuth } from 'src/domains/auth';
import { MemberType } from 'src/domains/tenant/types/MemberType';
import { createDefaultLocaleField } from 'src/plugins/i18n';
import { Registry } from 'src/Registry';
import * as Yup from 'yup';

const params = Yup.object({
  id: Yup.number().required().positive(),
});

export default [
  createEndpoint({
    method: EndpointMethod.PATCH,
    validation: {
      query: undefined,
      params,
      body: Yup.object({
        title: Yup.string().optional(),
        children: Yup.array(Yup.number().positive().required()).optional(),
      }),
    },
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async ({ params: { id }, body: { title, children } }) => {
      const model = Registry.getCategoryModel();

      const category = await model.getById(id);
      const composer = model.getUpdateComposer(category);

      if (title) {
        await composer.setTitle(createDefaultLocaleField(title));
      }

      if (children) {
        await composer.setChildren(children);
      }

      const isSucceeded = await composer.update();

      return { isSucceeded };
    },
  }),

  createEndpoint({
    method: EndpointMethod.DELETE,
    validation: {
      query: undefined,
      params,
      body: undefined,
    },
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async ({ params: { id } }) => {
      const model = Registry.getCategoryModel();

      const category = await model.getById(id);
      const isSucceeded = await model.delete(category);

      return { isSucceeded };
    },
  }),
];

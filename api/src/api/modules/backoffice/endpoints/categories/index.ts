import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { ContextWrapper } from 'src/common/ContextWrapper';
import { useTenantId } from 'src/common/middleware/useTenantId';
import { useAuth } from 'src/domains/auth';
import { MemberType } from 'src/domains/tenant/types/MemberType';
import { createDefaultLocaleField, localize, Locale } from 'src/plugins/i18n';
import { Registry } from 'src/Registry';
import * as Yup from 'yup';

export default [
  createEndpoint({
    method: EndpointMethod.GET,
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async (_, context) => {
      const wrapper = new ContextWrapper(context);
      const tenantId = wrapper.getTenantId();

      const categories = await Registry.getCategoryModel().getByTenantId(
        tenantId
      );

      return {
        entities: categories.map((category) => localize(category, Locale.RU)),
      };
    },
  }),

  createEndpoint({
    method: EndpointMethod.POST,
    validation: {
      query: undefined,
      params: undefined,
      body: Yup.object({
        title: Yup.string().required(),
        description: Yup.string().optional().nullable().default(null),
        parentCategoryId: Yup.number().positive().optional(),
      }),
    },
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async (
      { body: { title, description, parentCategoryId } },
      context
    ) => {
      const wrapper = new ContextWrapper(context);
      const model = Registry.getCategoryModel();

      const category = await model.create({
        tenantId: wrapper.getTenantId(),
        title: createDefaultLocaleField(title),
        description: createDefaultLocaleField(description),
      });

      if (parentCategoryId) {
        const parent = await model.getById(parentCategoryId);
        const composer = model.getUpdateComposer(parent);

        await composer.setChildren([...parent.children, category.id]);
        await composer.update();
      }

      return {
        entity: localize(category, Locale.RU),
      };
    },
  }),
];

import { collectEndpoints, createModule, Router } from '@glangeo/pollux/api';

const endpoints = collectEndpoints(__dirname);

export const BackofficeModule = createModule({
  name: 'BackofficeModule',
  router: new Router({
    endpoints,
    path: '/bo',
  }),
});

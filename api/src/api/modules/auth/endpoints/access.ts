import * as Yup from 'yup';
import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { Registry } from 'src/Registry';
import { useTenantId } from 'src/common/middleware/useTenantId';
import { useAuth } from 'src/domains/auth';
import { MemberType } from 'src/domains/tenant/types/MemberType';

export default [
  createEndpoint({
    method: EndpointMethod.HEAD,
    middlewares: [useTenantId(), useAuth({ member: MemberType.Manager })],

    action: async () => ({ isSucceeded: true }),
  }),

  createEndpoint({
    method: EndpointMethod.POST,
    validation: {
      query: undefined,
      params: undefined,
      body: Yup.object({
        token: Yup.string().required(),
      }),
    },

    action: async ({ body: { token } }, _, __, res) => {
      const model = Registry.getClientModel();

      const client = await model.getByRefreshToken(token);
      const access = await model.getAccessToken(client);

      res.cookie('authorization', access, { httpOnly: true });

      return { isSucceeded: true };
    },
  }),
];

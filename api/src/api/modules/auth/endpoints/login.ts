import { ValidationException } from '@glangeo/pollux';
import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { AuthErrors, ClientType } from 'src/domains/auth';
import { Registry } from 'src/Registry';
import * as Yup from 'yup';

export default createEndpoint({
  method: EndpointMethod.POST,
  validation: {
    query: undefined,
    params: undefined,
    body: Yup.object({
      login: Yup.string().required(),
      password: Yup.string().required(),
    }),
  },

  action: async ({ body: { login, password } }, _, req) => {
    const credentialsModel = Registry.getCredentialsModel();
    const userModel = Registry.getUserModel();
    const clientModel = Registry.getClientModel();

    const credentials = await credentialsModel.getByLogin(login);

    if (!credentialsModel.arePasswordsEquals(credentials, password)) {
      throw new ValidationException({
        message: AuthErrors.LoginOrPasswordWrong,
        publicInfo: {
          message: AuthErrors.LoginOrPasswordWrong,
        },
      });
    }

    const user = await userModel.getByEmail(credentials.login);
    const client = await clientModel.create(ClientType.User, user.id, {
      ip: req.ip,
      agent: {
        type: 'website',
        info: req.headers['user-agent'] || 'unknown',
      },
    });

    const refreshToken = clientModel.getRefreshToken(client);

    return { token: refreshToken };
  },
});

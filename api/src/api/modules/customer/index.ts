import { collectEndpoints, createModule, Router } from '@glangeo/pollux/api';

const endpoints = collectEndpoints(__dirname);

export const CustomerModule = createModule({
  name: 'CustomerModule',
  router: new Router({
    endpoints,
    path: '/menu',
  }),
});

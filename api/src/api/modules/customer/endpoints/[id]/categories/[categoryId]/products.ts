import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { ProductProjectionDefault } from 'src/api/projections/product/Default';
import { Locale, localize } from 'src/plugins/i18n';
import { project } from 'src/plugins/projection';
import { Registry } from 'src/Registry';
import * as Yup from 'yup';

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    query: undefined,
    params: Yup.object({
      id: Yup.string().required(),
      categoryId: Yup.number().positive().required(),
    }),
  },

  action: async ({ params: { id, categoryId } }) => {
    const model = Registry.getMenuModel();

    const menu = await model.getById(id);
    const products = await model.getCategoryProducts(menu, categoryId);

    return {
      entities: products.map((product) =>
        localize(project(product, ProductProjectionDefault), Locale.RU)
      ),
    };
  },
});

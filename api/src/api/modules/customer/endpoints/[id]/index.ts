import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { Locale, localize } from 'src/plugins/i18n';
import { Registry } from 'src/Registry';
import * as Yup from 'yup';

const params = Yup.object({
  id: Yup.string().required(),
});

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    query: undefined,
    params,
  },
  middlewares: [],

  action: async ({ params: { id } }) => {
    const model = Registry.getMenuModel();

    const menu = await model.getById(id);
    const catalogue = await model.getCatalogue(menu);

    return {
      entity: {
        catalogue: localize(catalogue, Locale.RU),
      },
    };
  },
});

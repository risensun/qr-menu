import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { ProductProjectionDefault } from 'src/api/projections/product/Default';
import { localize, Locale } from 'src/plugins/i18n';
import { project } from 'src/plugins/projection';
import { Registry } from 'src/Registry';
import * as Yup from 'yup';

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    query: undefined,
    params: Yup.object({
      id: Yup.string().required(),
      productId: Yup.number().required(),
    }),
  },

  action: async ({ params: { id, productId } }) => {
    const model = Registry.getMenuModel();

    const menu = await model.getById(id);
    const product = await model.getProductById(menu, productId);

    return {
      entity: localize(project(product, ProductProjectionDefault), Locale.RU),
    };
  },
});

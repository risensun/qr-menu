import path from 'path';
import cors from 'cors';
import express from 'express';
import cookieParser from 'cookie-parser';
import { ValidationException } from '@glangeo/pollux';
import { App as PolluxApp } from '@glangeo/pollux/api';
import { Config } from 'src/Config';
import { MenuModel } from 'src/domains/menus';
import { TenantModel } from 'src/domains/tenant';
import { UserModel } from 'src/domains/user';
import { createDefaultLocaleField, Locale } from 'src/plugins/i18n';
import { Registry } from 'src/Registry';
import { AuthModule } from './modules/auth';
import { BackofficeModule } from './modules/backoffice';
import { CustomerModule } from './modules/customer';

export class App extends PolluxApp {
  protected async beforeInit(): Promise<void> {
    const mongoDb = Registry.getMongoDb();
    await mongoDb.connect();

    const userModel = new UserModel(mongoDb);
    const tenantModel = new TenantModel(mongoDb);

    try {
      const user = await userModel.create({
        email: Config.getAdminLogin(),
        firstName: 'Администратор',
        lastName: '',
      });

      await Registry.getCredentialsModel().create(
        user.email,
        Config.getAdminPassword()
      );

      const tenant = await tenantModel.create({
        ownerId: user.id,
        name: createDefaultLocaleField('Mona Tea'),
        description: createDefaultLocaleField(''),
        defaultLocale: Locale.RU,
      });

      const menuModel = new MenuModel(mongoDb);
      await menuModel.create({
        tenantId: tenant.id,
        title: createDefaultLocaleField('Основное меню'),
      });
    } catch (error) {
      if (!(error instanceof ValidationException)) {
        throw error;
      }
    }
  }

  public async enableModules(): Promise<void> {
    await this.addModule(AuthModule);
    await this.addModule(BackofficeModule);
    await this.addModule(CustomerModule);
  }

  protected async applyMiddleware(): Promise<void> {
    await super.applyMiddleware();

    this.server.use(cookieParser());
    this.server.use(
      cors({
        origin: Config.getOrigin(),
        credentials: true,
      })
    );
    this.server.use(
      '/uploaded',
      express.static(
        path.join(process.cwd(), Config.getLocalStorageFolderName())
      )
    );
  }
}

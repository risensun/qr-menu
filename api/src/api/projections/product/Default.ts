import { Product } from 'src/domains/product';
import { getProductMediaUrl } from 'src/domains/product/utils';
import { ProjectionSchema } from 'src/plugins/projection';

export const ProductProjectionDefault = new ProjectionSchema<Product>().make({
  id: 1,
  tenantId: 1,
  createdAt: 1,
  categoryId: 1,
  menuIds: 1,
  media: (media) => ({
    miniatureSrc: media.miniatureSrc && getProductMediaUrl(media.miniatureSrc),
    gallery: [],
  }),
  title: 1,
  ingredients: 1,
  description: 1,
  size: 1,
  badges: 1,
  options: 1,
});

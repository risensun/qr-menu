import { I18nField } from './I18nField';
import { isI18nField } from './isI18nField';
import { Locale } from './Locale';

export type Localized<T extends Record<string, any>> = {
  [P in keyof T]: T[P] extends I18nField<infer U>
    ? U
    : T[P] extends I18nField<infer U> | undefined
    ? U | undefined
    : T[P] extends I18nField<infer U> | null
    ? U | null
    : T[P] extends Record<string, any>
    ? Localized<T[P]>
    : T[P];
};

export function localize<T extends Record<string, any>>(
  obj: T,
  locale: Locale,
  options: {
    isLocaleSafe?: boolean;
  } = {}
): Localized<T> {
  if (typeof obj !== 'object' || obj === null) {
    return obj;
  }

  const { isLocaleSafe = true } = options;
  const localizedObject: any = {};

  for (const fieldName of Object.getOwnPropertyNames(obj)) {
    const fieldValue = obj[fieldName];

    if (isI18nField(fieldValue)) {
      localizedObject[fieldName] = getI18NFieldValue(fieldValue, locale, {
        isLocaleSafe,
      });
    } else {
      if (Array.isArray(fieldValue)) {
        localizedObject[fieldName] = fieldValue.map((item: any) =>
          localize(item, locale, options)
        );
      } else if (typeof fieldValue === 'object') {
        localizedObject[fieldName] = localize(fieldValue, locale, options);
      } else {
        localizedObject[fieldName] = fieldValue;
      }
    }
  }

  return localizedObject as Localized<T>;
}

function getI18NFieldValue<T extends any>(
  field: I18nField<T>,
  locale: Locale,
  options: {
    isLocaleSafe: boolean;
  }
): T {
  const { isLocaleSafe } = options;

  if (isLocaleSafe) {
    const value = field[locale];

    if (typeof value === 'undefined') {
      const saveValue = Object.values(Locale)
        .map((locale) => field[locale] || null)
        .find((value) => value) as T;

      return saveValue;
    }

    return value;
  }

  return field[locale];
}

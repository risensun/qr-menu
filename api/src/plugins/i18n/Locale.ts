export enum Locale {
  AM = 'am',
  EN = 'en',
  RU = 'ru',
}

import { I18nField } from './I18nField';
import { Locale } from './Locale';

export function isI18nField<T>(obj: any): obj is I18nField<T> {
  const areAllLocaleFieldsDefined = Object.values(Locale).every(
    (locale) => typeof obj[locale] !== 'undefined'
  );

  return areAllLocaleFieldsDefined;
}

import { I18nField } from './I18nField';
import { Locale } from './Locale';

export function createDefaultLocaleField<T>(value: T): I18nField<T> {
  return Object.values(Locale).reduce<I18nField<T>>(
    (acc, curr) => ({
      ...acc,
      [curr]: value,
    }),
    {} as any
  );
}

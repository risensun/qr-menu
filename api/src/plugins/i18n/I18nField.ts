import { Locale } from './Locale';

export type I18nField<T> = {
  [key in Locale]: T;
};

import { IProjectable, ProjectedDataType, ProjectionSchemaType } from './types';

export interface IProjection<T extends IProjectable> {
  project<V extends ProjectionSchemaType<T>>(
    schema: V
  ): ProjectedDataType<T, V>;
}

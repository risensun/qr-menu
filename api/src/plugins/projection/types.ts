// === Base types
type Primitive = string | number | boolean | null;
type PlainObjectField = Primitive | IProjectable | Primitive[] | IProjectable[];

export interface IProjectable extends Record<string, PlainObjectField> {}
// eslint-disable-next-line @typescript-eslint/naming-convention

// === Schema types
export type ProjectionSchemaType<T extends IProjectable> = {
  [K in keyof T]?: T[K] extends Primitive
    ? number
    : T[K] extends Primitive[]
    ? number
    : T[K] extends IProjectable[]
    ?
        | number
        | ProjectionSchemaType<T[K][0]>
        | ((value: T[K]) => PlainObjectField)
    : T[K] extends IProjectable
    ? number | ProjectionSchemaType<T[K]> | ((value: T[K]) => PlainObjectField)
    : any;
};

export type ProjectedDataType<
  T extends IProjectable,
  V extends ProjectionSchemaType<T>
> = {
  [K in keyof T]: V[K] extends undefined
    ? never
    : V[K] extends number
    ? T[K]
    : V[K] extends Record<string, any>
    ? T[K] extends IProjectable
      ? ProjectedDataType<T[K], V[K]>
      : T[K] extends IProjectable[]
      ? ProjectedDataType<T[K][0], V[K]>[]
      : undefined
    : V[K] extends (value: T[K]) => PlainObjectField[]
    ? IProjectable[]
    : V[K] extends (value: T[K]) => PlainObjectField
    ? IProjectable
    : never;
};

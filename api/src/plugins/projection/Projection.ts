import { IProjection } from './interfaces';
import { IProjectable, ProjectionSchemaType, ProjectedDataType } from './types';

export class Projection<T extends IProjectable> implements IProjection<T> {
  public constructor(protected readonly data: T) {}

  public project<V extends ProjectionSchemaType<T>>(schema: V) {
    return project(this.data, schema);
  }
}

export function project<
  T extends IProjectable,
  V extends ProjectionSchemaType<T>
>(data: T, schema: V): ProjectedDataType<T, V> {
  const result: any = {};

  for (const propName of Object.getOwnPropertyNames(schema)) {
    const schemaPropValue = schema[propName];

    switch (typeof schemaPropValue) {
      case 'number':
        result[propName] = data[propName];
        break;

      case 'function':
        result[propName] = (schemaPropValue as (...args: any[]) => any)(
          data[propName] as any
        );
        break;

      case 'object':
        {
          const dataPropValue = data[propName];

          if (Array.isArray(dataPropValue)) {
            result[propName] = (dataPropValue as any[]).map((value) => {
              if (value && typeof value === 'object') {
                return project(value, schemaPropValue as any);
              }

              throw new Error('Value is not object!');
            });
          } else if (dataPropValue && typeof dataPropValue === 'object') {
            result[propName] = project(dataPropValue, schemaPropValue as any);
          }
        }
        break;

      default:
        break;
    }
  }

  return result;
}

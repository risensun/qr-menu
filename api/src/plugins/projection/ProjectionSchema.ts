import { IProjectable, ProjectionSchemaType } from './types';

export class ProjectionSchema<T extends IProjectable> {
  public constructor() {}

  public make<V extends ProjectionSchemaType<T>>(schema: V) {
    return schema;
  }
}

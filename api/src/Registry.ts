import { RegistryBuilder } from '@glangeo/pollux';
import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { Config } from 'src/Config';
import { MulterStorage } from './common/helpers';
import { ClientModel, CredentialsModel } from './domains/auth/models';
import { CatalogueModel } from './domains/category';
import { MenuModel } from './domains/menus';
import { BadgeModel, ProductModel } from './domains/product';
import { TenantModel } from './domains/tenant';
import { UserModel } from './domains/user';

export const Registry = new RegistryBuilder()
  .addMethod({
    key: 'getMongoDb',
    factory: () => new MongoDB(Config.getMongoDbUrl(), Config.getMongoDbName()),
  })
  .addMethod({
    key: 'getTenantModel',
    factory: (registry) => new TenantModel(registry.getMongoDb()),
  })
  .addMethod({
    key: 'getUserModel',
    factory: (registry) => new UserModel(registry.getMongoDb()),
  })
  .addMethod({
    key: 'getMenuModel',
    factory: (registry) => new MenuModel(registry.getMongoDb()),
  })
  .addMethod({
    key: 'getCategoryModel',
    factory: (registry) => new CatalogueModel(registry.getMongoDb()),
  })
  .addMethod({
    key: 'getProductModel',
    factory: (registry) => new ProductModel(registry.getMongoDb()),
  })
  .addMethod({
    key: 'getBadgeModel',
    factory: (registry) => new BadgeModel(registry.getMongoDb()),
  })
  .addMethod({
    key: 'getCredentialsModel',
    factory: (registry) => new CredentialsModel(registry.getMongoDb()),
  })
  .addMethod({
    key: 'getClientModel',
    factory: (registry) =>
      new ClientModel(
        registry.getMongoDb(),
        Config.getJwtRefreshTokenSecret(),
        Config.getJwtAccessTokenSecret()
      ),
  })
  .addMethod({
    key: 'getProductMediaStorage',
    factory: () =>
      new MulterStorage('products/media', Config.getLocalStorageFolderName()),
  })
  .build();

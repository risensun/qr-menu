import { Config as PolluxConfig } from '@glangeo/pollux';

const PORT = 'PORT';

const INF_ORIGIN = 'INF_ORIGIN';
const INF_CDN_URL = 'INF_CDN_URL';
const INF_MONGO_DB_URL = 'INF_MONGO_DB_URL';
const INF_MONGO_DB_NAME = 'INF_MONGO_DB_NAME';
const INF_ADMIN_LOGIN = 'INF_ADMIN_LOGIN';
const INF_ADMIN_PASSWORD = 'INF_ADMIN_PASSWORD';
const INF_LOCAL_STORAGE_FOLDER_NAME = 'INF_LOCAL_STORAGE_FOLDER_NAME';

const SCRT_JWT_SECRET_REFRESH = 'SCRT_JWT_SECRET_REFRESH';
const SCRT_JWT_SECRET_ACCESS = 'SCRT_JWT_SECRET_ACCESS';

export abstract class Config extends PolluxConfig {
  public static getPort(): number {
    return Number(this.safeGetEnvVar(PORT));
  }

  public static getCdnUrl(): string {
    return this.safeGetEnvVar(INF_CDN_URL);
  }

  public static getOrigin(): string {
    return this.safeGetEnvVar(INF_ORIGIN);
  }

  public static getMongoDbUrl(): string {
    return this.safeGetEnvVar(INF_MONGO_DB_URL);
  }

  public static getMongoDbName(): string {
    return this.safeGetEnvVar(INF_MONGO_DB_NAME);
  }

  public static getAdminLogin(): string {
    return this.safeGetEnvVar(INF_ADMIN_LOGIN);
  }

  public static getAdminPassword(): string {
    return this.safeGetEnvVar(INF_ADMIN_PASSWORD);
  }

  public static getLocalStorageFolderName(): string {
    return this.safeGetEnvVar(INF_LOCAL_STORAGE_FOLDER_NAME);
  }

  public static getJwtRefreshTokenSecret(): string {
    return this.safeGetEnvVar(SCRT_JWT_SECRET_REFRESH);
  }

  public static getJwtAccessTokenSecret(): string {
    return this.safeGetEnvVar(SCRT_JWT_SECRET_ACCESS);
  }
}

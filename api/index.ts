import { loadAliases, loadEnvironment } from '@glangeo/pollux/utils';

loadAliases();
loadEnvironment();

import { Config } from 'src/Config';
import { App } from './src/api/App';

const app = new App({
  port: Config.getPort(),
  baseRoute: '/api/monatea',
  logging: {
    isEnabled: true,
    router: {
      routeAdded: true,
    },
  },
});

app.init().then(() => app.listen());

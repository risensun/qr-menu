import React from 'react';
import { NextPage } from 'next';
import Head from 'next/head';
import { Footer } from 'src/modules/common/components/molecules/Footer';
import { Header } from 'src/modules/common/components/molecules/Header';
import { Container } from 'src/modules/common/components/templates/Container';
import {
  Breakpoint,
  useBreakpoints,
} from 'src/modules/common/hooks/useBreakpoints';
import { NotFound } from 'src/modules/common/components/molecules/NotFound';

const NotFoundPage: NextPage = () => {
  const [shouldDisplayFooter, setShouldDisplayFooter] = React.useState(false);

  useBreakpoints((breakpoint) => {
    setShouldDisplayFooter(breakpoint <= Breakpoint.LG);
  });

  return (
    <>
      <Head>
        <title>404</title>
      </Head>
      <Header
        activeCategoryId={0}
        categories={[]}
        onCategoryChange={() => {}}
      />
      <Container>
        <NotFound />
      </Container>
      {shouldDisplayFooter && <Footer />}
    </>
  );
};

export default NotFoundPage;

import Document, { Html, Head, Main, NextScript } from 'next/document';

class CustomDocument extends Document {
  public render(): JSX.Element {
    return (
      <Html>
        <Head>
          <link rel="icon" type="image/ico" href="/Favicon.ico" />
          <meta name="description" content="Mona tea menu website" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default CustomDocument;

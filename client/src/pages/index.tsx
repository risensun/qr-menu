import { GetServerSideProps } from 'next';
import { MenuService } from 'src/modules/common/services/MenuService';
import { getCategoryRelativeUrl } from 'src/modules/common/utils/getCategoryRelativeUrl';

export default function RedirectPage() {
  return <div />;
}

export const getServerSideProps: GetServerSideProps = async () => {
  const catalogue = await MenuService.getMenu();
  const { rootIds } = catalogue;

  return {
    redirect: {
      destination: getCategoryRelativeUrl(rootIds[0]),
      permanent: false,
    },
  };
};

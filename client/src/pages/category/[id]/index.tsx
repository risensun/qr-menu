import React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import { Footer } from 'src/modules/common/components/molecules/Footer';
import { Header } from 'src/modules/common/components/molecules/Header';
import { Tag } from 'src/modules/common/components/molecules/Tag';
import { ProductCard } from 'src/modules/common/components/organisms/ProductCard';
import { Container } from 'src/modules/common/components/templates/Container';
import { GridContainer } from 'src/modules/common/components/templates/GridContainer';
import { OverflowList } from 'src/modules/common/components/templates/OverflowList';
import {
  Breakpoint,
  useBreakpoints,
} from 'src/modules/common/hooks/useBreakpoints';
import {
  Catalogue,
  MenuService,
} from 'src/modules/common/services/MenuService';
import { Product } from 'src/modules/common/types/Product';
import { getCategoriesAndTabs } from 'src/modules/common/utils/getCategoriesAndTabs';
import { getCategoryRelativeUrl } from 'src/modules/common/utils/getCategoryRelativeUrl';

const ProductModalProvider: any = dynamic(
  () =>
    import('src/modules/common/components/organisms/ProductModal').then(
      (module) => module.ProductModalProvider
    ) as any,
  {
    ssr: false,
  }
);

export type LandingPageProps = {
  readonly catalogue: Catalogue;
  readonly currentState: {
    readonly categoryId: number;
    readonly products: Product[];
  };
};

const LandingPage: NextPage<LandingPageProps> = ({
  catalogue,
  currentState,
}) => {
  const { categoryId, products } = currentState;

  const router = useRouter();
  const [openedProduct, setOpenedProduct] = React.useState<Product | null>(
    null
  );
  const [shouldDisplayFooter, setShouldDisplayFooter] = React.useState(false);

  useBreakpoints((breakpoint) => {
    setShouldDisplayFooter(breakpoint <= Breakpoint.LG);
  });

  const { activeCategoryId, categories, activeTabId, tabs } = React.useMemo(
    () =>
      getCategoriesAndTabs(categoryId, catalogue.rootIds, catalogue.categories),
    [catalogue, categoryId]
  );

  const navigateToCategory = React.useCallback(
    (id: number) => {
      if (id !== categoryId) {
        router.push(getCategoryRelativeUrl(id));
      }
    },
    [categoryId, router]
  );

  return (
    <>
      <Head>
        <title>QR Menu</title>
      </Head>
      <Header
        activeCategoryId={activeCategoryId}
        categories={categories}
        onCategoryChange={navigateToCategory}
      />
      <Container>
        {tabs.length > 0 && (
          <OverflowList>
            <Tag
              text="Все"
              onClick={() => navigateToCategory(activeCategoryId)}
              isActive={activeTabId === activeCategoryId}
            />
            {tabs.map((tab) => (
              <Tag
                key={tab.id}
                text={tab.title}
                isActive={tab.id === activeTabId}
                onClick={() => navigateToCategory(tab.id)}
              />
            ))}
          </OverflowList>
        )}
        <GridContainer>
          {products.map((product) => (
            <ProductCard
              product={product}
              key={product.id}
              onOpenProduct={() => setOpenedProduct(product)}
            />
          ))}
        </GridContainer>
      </Container>
      {shouldDisplayFooter && <Footer />}
      <ProductModalProvider
        product={openedProduct}
        onClose={() => setOpenedProduct(null)}
      />
    </>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const catalogue = await MenuService.getMenu();

  const paths = catalogue.ids.map((id) => {
    const category = catalogue.categories[id];

    return {
      params: { id: String(category.id) },
    };
  });

  return {
    paths,
    fallback: 'blocking',
  };
};

export const getStaticProps: GetStaticProps = async (context) => {
  const idFromParams = context.params?.id;

  const id = Number(idFromParams);

  if (Number.isNaN(id)) {
    return {
      notFound: true,
    };
  }

  const catalogue = await MenuService.getMenu();

  if (!catalogue.ids.includes(id)) {
    return {
      notFound: true,
    };
  }

  const products = await MenuService.getProductsByCategoryId(Number(id));

  return {
    props: {
      catalogue,
      currentState: {
        products,
        categoryId: id,
      },
    },
    revalidate: 60 * 1, // 1 minute
  };
};

export default LandingPage;

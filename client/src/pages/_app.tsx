import 'src/scss/index.scss';
import type { AppProps } from 'next/app';
import { Component } from 'preact';
import Router from 'next/router';
import NProgress from 'nprogress'; //nprogress module
import 'nprogress/nprogress.css'; //styles of nprogress

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

class MyApp extends Component<AppProps> {
  constructor(props: AppProps) {
    super(props);
  }

  render() {
    return <this.props.Component {...this.props.pageProps} />;
  }
}

export default MyApp;

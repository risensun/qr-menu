import React from 'react';

export const Hot: React.FC = () => (
  <svg
    width="22"
    height="22"
    viewBox="0 0 22 22"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M5.2627 13.5499C5.2627 9.84443 10.2033 6.97568 9.0877 2.7124C11.7174 2.7124 16.7377 6.5374 16.7377 13.5499C16.7377 15.0716 16.1332 16.5309 15.0572 17.6069C13.9812 18.6829 12.5219 19.2874 11.0002 19.2874C9.47852 19.2874 8.01916 18.6829 6.94317 17.6069C5.86718 16.5309 5.2627 15.0716 5.2627 13.5499V13.5499Z"
      stroke="white"
      strokeWidth="1.2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.5502 15.4624C13.5502 17.7618 12.2752 18.6499 11.0002 18.6499C9.7252 18.6499 8.4502 17.7618 8.4502 15.4624C8.4502 13.163 10.0439 12.0359 9.7252 10.3624C11.3986 10.3624 13.5502 13.163 13.5502 15.4624Z"
      stroke="white"
      strokeWidth="1.2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

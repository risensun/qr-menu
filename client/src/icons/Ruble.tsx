import React from 'react';

export const Ruble: React.FC = () => (
  <svg
    width="10"
    height="12"
    viewBox="0 0 10 12"
    fill="currentColor"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M1.28 9.712V11.424H3.696V9.712H6.16V8.16H3.696V6.912H4.736C5.84533 6.912 6.72 6.74667 7.36 6.416C8.01067 6.08533 8.47467 5.648 8.752 5.104C9.02933 4.56 9.168 3.97867 9.168 3.36C9.168 2.31467 8.82667 1.49333 8.144 0.896C7.472 0.298667 6.4 0 4.928 0H1.28V4.928H0V6.912H1.28V8.16H0V9.712H1.28ZM3.696 4.928H4.496C4.95467 4.928 5.34933 4.88 5.68 4.784C6.01067 4.67733 6.26666 4.512 6.448 4.288C6.62933 4.064 6.72 3.78133 6.72 3.44C6.72 2.94933 6.56533 2.58667 6.256 2.352C5.94667 2.10667 5.46133 1.984 4.8 1.984H3.696V4.928Z"
      fill="#5C5269"
    />
  </svg>
);

export function isOnServerSide(): boolean {
  return typeof window === 'undefined';
}

export class UrlRegistry {
  public static getAngularUrl(): string {
    const url = process.env.NEXT_PUBLIC_ANGULAR_URL;

    if (!url) {
      throw new Error(
        'NEXT_PUBLIC_ANGULAR_URL is undefined. Have you created your .env* files?'
      );
    }

    return url;
  }

  public static getUserAPIUrl(): string {
    const url = process.env.NEXT_PUBLIC_USER_API_URL;

    if (!url) {
      throw new Error(
        'NEXT_PUBLIC_USER_API_URL is not defined in your .env* file'
      );
    }

    return url;
  }

  public static getMentoringAPIUrl(): string {
    const url = process.env.NEXT_PUBLIC_MENTORING_API_URL;

    if (!url) {
      throw new Error(
        'NEXT_PUBLIC_MENTORING_API_URL is not defined in your .env* file'
      );
    }

    return url;
  }

  public static getMysqlAPIUrl(): string {
    const url = process.env.NEXT_PUBLIC_MYSQL_API_URL;

    if (!url) {
      throw new Error(
        'NEXT_PUBLIC_MYSQL_API_URL is not defined in your .env* file'
      );
    }

    return url;
  }

  public static getCDNUrl(): string {
    const url = process.env.NEXT_PUBLIC_CDN_URL;

    if (!url) {
      throw new Error('NEXT_PUBLIC_CDN_URL is not defined in your .env* file');
    }

    return url;
  }

  public static getResourceUrl(src: string): string {
    return `${this.getCDNUrl()}/${src}`;
  }
}

import { CommonModuleConfiguration, ModuleType } from '../Module';

export function createCommonModule<T extends CommonModuleConfiguration>(
  configuration: Omit<T, 'type'>
): T {
  return {
    type: ModuleType.Common,
    ...configuration,
  } as T;
}

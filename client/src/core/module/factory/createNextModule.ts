import { ModuleType, NextModuleConfiguration } from '../Module';

export function createNextModule<T extends NextModuleConfiguration>(
  module: Omit<T, 'type'>
): T {
  return {
    type: ModuleType.Next,
    ...module,
  } as T;
}

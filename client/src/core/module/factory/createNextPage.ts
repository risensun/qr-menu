import { NextPageConfiguration, PageType } from '../Module';

export function createNextPage<T extends NextPageConfiguration>(
  configuration: Omit<T, 'type'>
): T {
  return {
    type: PageType.Next,
    ...configuration,
  } as T;
}

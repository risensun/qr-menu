import { AnyAction, combineReducers, Reducer, Store } from '@reduxjs/toolkit';
import { Saga, Task } from 'redux-saga';

export type RunSaga = <S extends Saga<any[]>>(
  saga: S,
  ...args: Parameters<S>
) => Task;

export class ReduxInjector {
  private readonly reducerRegistry: { [key: string]: Reducer<any, AnyAction> };
  private readonly sagaRegistry: {
    [key: string]: {
      saga: Saga<any>;
      task: Task;
    };
  };

  public constructor(private readonly runSaga: RunSaga) {
    this.reducerRegistry = {};
    this.sagaRegistry = {};
  }

  public injectReducer(key: string, reducer: Reducer<any, AnyAction>): void {
    if (this.reducerRegistry[key] && this.reducerRegistry[key] === reducer) {
      return;
    }

    this.reducerRegistry[key] = reducer;
  }

  public updateStoreReducer(store: Store): void {
    // TODO: check, if we registered any reducer
    store.replaceReducer(combineReducers({ ...this.reducerRegistry }));
  }

  public injectSaga<S extends Saga<any[]>>(key: string, saga: S): void {
    let isSagaInjected = Boolean(this.sagaRegistry[key]);

    if (process.env.NODE_ENV !== 'production') {
      // enable sagas hot reloading
      if (isSagaInjected && this.sagaRegistry[key].saga !== saga) {
        this.sagaRegistry[key].task.cancel();

        isSagaInjected = false;
      }
    }

    if (isSagaInjected) {
      return;
    }

    this.sagaRegistry[key] = {
      saga,
      task: this.runSaga(saga as any),
    };
  }
}

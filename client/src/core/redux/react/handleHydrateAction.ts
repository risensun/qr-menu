import { ActionReducerMapBuilder, CaseReducer } from '@reduxjs/toolkit';
import { HYDRATE } from '../constants';
import { HydrateAction } from './HydateAction';

export function handleHydrateAction<S>(
  builder: ActionReducerMapBuilder<S>,
  handler: CaseReducer<S, HydrateAction>
): void {
  builder.addCase<typeof HYDRATE, HydrateAction>(HYDRATE, handler);
}

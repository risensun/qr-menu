import { combineReducers, configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { ReduxInjector } from './ReduxInjector';

const sagaMiddleware = createSagaMiddleware();

export const REDUX_INJECTOR = new ReduxInjector(sagaMiddleware.run);

export const store = configureStore({
  reducer: combineReducers({}),
  middleware: (getDefaultMiddleware) => [
    ...getDefaultMiddleware(),
    sagaMiddleware,
  ],
  preloadedState: {},
});

import Head from 'next/head';
import { createNextPage } from 'src/core/module/factory/createNextPage';
import { injectPage } from 'src/core/module/injectors.ts/injectPage';
import { REDUX_INJECTOR, store, withRedux } from 'src/core/redux';
import { makeGetStaticProps } from 'src/core/utils/makeGetStaticProps';
import { ReduxTest } from 'src/modules/test/components/ReduxTest';
import { TestModule } from '..';

const Page = withRedux(() => (
  <div>
    <Head>
      <title>QR Menu</title>
      <meta name="description" content="Generated by create next app" />
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <ReduxTest />
  </div>
));

export const TestPage = createNextPage({
  module: TestModule,

  getDefaultExport() {
    return Page;
  },

  getPrerenderingFunctions() {
    return {
      getStaticProps: makeGetStaticProps(async (context) => ({ props: {} })),
    };
  },
});

injectPage(store, REDUX_INJECTOR, TestPage);

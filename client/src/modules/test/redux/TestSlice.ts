import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type TestSliceState = {
  test: 'test' | null;
  loaders: {
    assessmentMentors: boolean;
  };
};

const initialState: TestSliceState = {
  test: null,
  loaders: {
    assessmentMentors: false,
  },
};

const selectTestState = (state: any): TestSliceState =>
  state
    ? state[TestSlice.name] || TestSlice.initialState
    : TestSlice.initialState;

const testSlice = createSlice({
  name: 'test',
  initialState,
  reducers: {
    getTest(
      state,
      action: PayloadAction<{
        test: 'test';
      }>
    ) {
      state.test = action.payload.test;
    },
  },
});

export const { getTest } = testSlice.actions;
export const landingReducer = testSlice.reducer;

export const TestSlice = {
  initialState,
  name: testSlice.name,
  actions: testSlice.actions,
  selectState: selectTestState,
  reducer: testSlice.reducer,
};

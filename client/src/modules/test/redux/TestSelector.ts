import { TestSlice, TestSliceState } from './TestSlice';

export const selectTestState = (state: any): TestSliceState =>
  state[TestSlice.name] || TestSlice.initialState;

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectTestState } from '../../redux/TestSelector';
import { TestSlice } from '../../redux/TestSlice';

export type ReduxTestProps = Record<string, unknown>;

export const ReduxTest: React.FC<ReduxTestProps> = () => {
  const dispatch = useDispatch();
  const state = useSelector(selectTestState);

  React.useEffect(() => {
    dispatch(TestSlice.actions.getTest({ test: 'test' }));
  }, []);

  const marginTop = 50;

  return (
    <div className="container">
      <div className="content">{state.test}</div>
      <style jsx>
        {`
          .container {
            display: flex;
            justify-content: center;
            margin-top: ${marginTop}px;
          }

          .content {
            font-size: 28px;
            font-weight: 900;
            color: red;
          }
        `}
      </style>
    </div>
  );
};

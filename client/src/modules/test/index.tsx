import { createNextModule } from 'src/core/module/factory/createNextModule';
import { TestSlice } from './redux/TestSlice';

export const TestModule = createNextModule({
  dependencies: [],

  getReducers() {
    return {
      [TestSlice.name]: TestSlice.reducer,
    };
  },
});

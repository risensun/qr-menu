import { apiManager } from '../../core/modules/apiManager';
import { UrlBuilder } from '../../core/utils/UrlBuilder';
import { Product } from '../../types/Product';
import { Config } from '../../utils/Config';

export interface ICatalogueResponse {
  data: {
    entity: {
      catalogue: Catalogue;
    };
  };
}

export type Catalogue = {
  ids: number[];
  rootIds: number[];
  categories: { [key: string]: ICategory };
};

export interface IProductResponse {
  data: {
    entities: Product[];
  };
}

export interface ICategory {
  id: number;
  createdAt: number;
  children: number[];
  tenantId: string;
  title: string;
  description: string | null;
}

export class MenuService {
  public static async getMenu(): Promise<Catalogue> {
    const api = new apiManager.API(
      UrlBuilder.getApiRoute(`menu/${Config.getMenuId()}`)
    );

    const response = await api.get<ICatalogueResponse>();

    if (response.status === 'success') {
      return response.result.data.entity.catalogue;
    }

    throw new Error('Could not get menu catalogue!');
  }

  public static async getProductsByCategoryId(
    categoryId: number
  ): Promise<Product[] | null> {
    const api = new apiManager.API(
      UrlBuilder.getApiRoute(
        `menu/${Config.getMenuId()}/categories/${categoryId}/products`
      )
    );

    const response = await api.get<IProductResponse>();

    if (response.status === 'success') {
      return response.result.data.entities;
    }

    return null;
  }
}

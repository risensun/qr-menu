import clsx from 'clsx';
import React from 'react';
import { Typography } from '../../atoms/Typography';
import styles from './search-modal.module.scss';
import productMock from 'src/mocks/product.json';
import Image from 'next/image';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

export type SearchModalProps = {
  readonly isToggled: boolean;
  readonly onClose: (...args: any[]) => void;
};

export const SearchModal: React.FC<SearchModalProps> = ({
  isToggled,
  onClose,
}) => {
  const [searchValue, setSearchValue] = React.useState('');
  const modalRef = React.useRef<HTMLDivElement | null>(null);

  React.useEffect(() => {
    if (modalRef.current) {
      if (isToggled) {
        disableBodyScroll(modalRef.current);
      } else {
        enableBodyScroll(modalRef.current);
      }
    }
  }, [isToggled]);

  return (
    <div
      className={clsx(styles.root, isToggled && styles.toggled)}
      ref={modalRef}
    >
      <div className={styles.root__search}>
        <input
          className={styles.root__search__input}
          type="text"
          placeholder="Что будем искать?"
          value={searchValue}
          onChange={(e) => setSearchValue(e.target.value)}
        />
        <button className={styles.root__search__cancel} onClick={onClose}>
          <Typography preset="common">Отмена</Typography>
        </button>
      </div>
      <div className={styles.root__results}>
        {productMock.map((product) => (
          <div className={styles.root__results__product} key={product.id}>
            <div className={styles.root__results__product__image}>
              <Image
                src="/images/src1.png"
                // src={product.src}
                alt={product.title}
                className={styles.root__results__product__image__src}
                objectFit="cover"
                layout="fill"
              />
            </div>

            <Typography
              className={styles.root__results__product__title}
              preset="search-products"
            >
              {product.title}
            </Typography>
          </div>
        ))}
      </div>
    </div>
  );
};

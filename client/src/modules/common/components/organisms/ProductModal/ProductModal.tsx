import React, { useEffect, useRef, useState } from 'react';
import clsx from 'clsx';
import styles from './product-modal.module.scss';
import { Typography } from '../../atoms/Typography';
import { Size } from '../../atoms/Size';
import { disableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';
import { Product } from 'src/modules/common/types/Product';
import { getProductVolume } from 'src/modules/common/utils/getProductVolume';
import { getNutrientNameByKey } from 'src/modules/common/utils/getNutrientNameByKey';
import { Ruble } from 'src/icons/Ruble';
import { usePresence } from 'framer-motion';
import { Image } from './partials/Image';

export type ProudctModalProps = {
  readonly product: Product;
  readonly onClose: (event: React.MouseEvent) => void;
};

export const ProductModal: React.FC<ProudctModalProps> = ({
  product,
  onClose,
}) => {
  const modalRef = useRef<HTMLDivElement | null>(null);
  const [isPresent, safeToRemove] = usePresence();
  const [isFadeOutAnimationEnd, setIsFadeOutAnimationEnd] =
    React.useState(false);
  const [currentSizeIndex, setCurrentSizeIndex] = useState(0);

  const size = product
    ? product.size.options[currentSizeIndex].nutrients
    : null;

  useEffect(() => {
    const node = modalRef.current;

    if (!node) {
      return;
    }

    const handleWindowResize = () => {
      const node = modalRef.current;

      if (!node) {
        return;
      }

      node.style.height = `${window.innerHeight}px`;
    };

    disableBodyScroll(node);

    window.addEventListener('resize', handleWindowResize);
    handleWindowResize();

    return () => {
      window.removeEventListener('reset', handleWindowResize);
      clearAllBodyScrollLocks();
    };
  }, []);

  useEffect(() => {
    if (!isPresent) {
      clearAllBodyScrollLocks();

      if (isFadeOutAnimationEnd) {
        safeToRemove();
      }
    }
  }, [isFadeOutAnimationEnd, isPresent, safeToRemove]);

  return (
    <div
      ref={modalRef}
      onClick={onClose}
      className={clsx(styles.wrapper, isPresent && styles.visible)}
      onAnimationEnd={() => !isPresent && setIsFadeOutAnimationEnd(true)}
    >
      <section className={styles.root} onClick={(e) => e.stopPropagation()}>
        <Image
          src={product.media.miniatureSrc}
          alt={product.title}
          onClose={onClose}
          badges={product.badges}
          className={styles.root__image}
        />

        <div className={styles.root__info}>
          <div className={styles.root__info__title}>
            <div className={styles.root__info__title__left}>
              <Typography preset="card-title">{product.title}</Typography>
              <Typography preset="card-volume">
                {getProductVolume(
                  product.size.name,
                  product.size.options[currentSizeIndex].value,
                  product.size.unit
                )}
              </Typography>
            </div>
            {product.size.options.length > 1 && (
              <div className={styles.root__info__title__sizes}>
                {product.size.options.map((size, index) =>
                  size.isActive ? (
                    <Size
                      text={size.name}
                      key={size.key}
                      onClick={(e) => {
                        setCurrentSizeIndex(index);
                        e.stopPropagation();
                      }}
                      isPicked={index === currentSizeIndex}
                    />
                  ) : null
                )}
              </div>
            )}
          </div>
          <div className={styles.root__info__line} />
          <Typography
            preset="common-light"
            className={styles.root__info__ingredients}
          >
            {product.ingredients}
          </Typography>
          <div className={styles.root__info__line} />
          <div className={styles.root__info__nutrients}>
            {size &&
              Object.keys(size).map((key, index) => {
                const nutrientAmount = size[key as keyof typeof size];

                return (
                  <div
                    className={styles.root__info__nutrients__item}
                    key={index}
                  >
                    <Typography preset="nutrients-number">
                      {nutrientAmount}
                    </Typography>
                    <Typography preset="nutrients-text">
                      {getNutrientNameByKey(key as keyof typeof size)}
                    </Typography>
                  </div>
                );
              })}
          </div>
          <div className={styles.root__info__line} />
          <Typography
            preset="common-light"
            className={styles.root__info__description}
          >
            {product.description}
          </Typography>
          <Typography align="center" className={styles.root__info__price}>
            {product.size.options[currentSizeIndex].price} <Ruble />
          </Typography>
        </div>
      </section>
    </div>
  );
};

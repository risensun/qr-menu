import { AnimatePresence } from 'framer-motion';
import { Product } from 'src/modules/common/types/Product';
import { ProductModal } from './ProductModal';

export type ProductModalProvider = {
  readonly product: Product | null;
  readonly onClose: () => void;
};

export const ProductModalProvider: React.FC<ProductModalProvider> = ({
  product,
  onClose,
}) => (
  <AnimatePresence>
    {product && (
      <ProductModal
        key={String(product.id)}
        product={product}
        onClose={onClose}
      />
    )}
  </AnimatePresence>
);

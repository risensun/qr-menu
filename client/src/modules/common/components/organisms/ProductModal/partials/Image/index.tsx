import React from 'react';
import NextImage from 'next/image';

import styles from './Image.module.scss';
import { Modificator } from 'src/modules/common/components/atoms/Modificator';
import { getModificatorComponent } from 'src/modules/common/utils/getModificatorComponent';
import { CloseButton } from 'src/modules/common/components/atoms/CloseButton';
import clsx from 'clsx';

export type ImageProps = {
  readonly src: string;
  readonly alt: string;
  readonly badges: string[];
  readonly className?: string;
  readonly onClose: (event: React.MouseEvent) => void;
};

export const Image: React.FC<ImageProps> = ({
  src,
  alt,
  badges,
  className,
  onClose,
}) => (
  <div className={clsx(styles.root, className)}>
    <NextImage
      layout="fill"
      objectFit="cover"
      alt={alt}
      src={src || '/images/placeholder.jpg'}
      onError={(event) => (event.currentTarget.src = '/images/placeholder.jpg')}
      className={styles.root__image}
    />
    <div className={styles.root__header}>
      <div className={styles.root__badges}>
        {badges.map((id) => (
          <Modificator key={id} icon={getModificatorComponent(id)} />
        ))}
      </div>
      <CloseButton onClick={onClose} />
    </div>
  </div>
);

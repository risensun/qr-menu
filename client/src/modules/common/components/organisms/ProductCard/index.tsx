import Image from 'next/image';
import React from 'react';
import { Product } from 'src/modules/common/types/Product';
import { getModificatorComponent } from 'src/modules/common/utils/getModificatorComponent';
import { Typography } from '../../atoms/Typography';
import { Modificator } from '../../atoms/Modificator';
import styles from './product-card.module.scss';
import { Size } from '../../atoms/Size';
import { getProductVolume } from 'src/modules/common/utils/getProductVolume';
import { Ruble } from 'src/icons/Ruble';

export type ProductCardProps = {
  readonly product: Product;
  readonly onOpenProduct: (...args: any[]) => void;
};

export const ProductCard: React.FC<ProductCardProps> = ({
  product,
  onOpenProduct,
}) => {
  const [pickedSize, setPickedSize] = React.useState(0);

  return (
    <div className={styles.root} onClick={onOpenProduct}>
      <div className={styles.root__image}>
        <div className={styles.root__image__modificators}>
          {product.badges.map((id) => (
            <Modificator key={id} icon={getModificatorComponent(id)} />
          ))}
        </div>
        <Image
          src={product.media.miniatureSrc || '/images/placeholder.jpg'}
          decoding="async"
          layout="fill"
          alt={product.title}
          objectFit="cover"
          className={styles.root__image__src}
        />
      </div>
      <div className={styles.root__price}>
        <Typography preset="price">
          {product.size.options[pickedSize].price} <Ruble />
        </Typography>
        {product.size.options.length > 1 && (
          <div className={styles.root__price__sizes}>
            {product.size.options.map((size, index) =>
              size.isActive ? (
                <Size
                  text={size.name}
                  key={size.key}
                  onClick={(e) => {
                    setPickedSize(index);
                    e.stopPropagation();
                  }}
                  isPicked={index === pickedSize}
                />
              ) : null
            )}
          </div>
        )}
      </div>
      <Typography preset="common">{product.title}</Typography>
      <Typography preset="common-volume">
        {getProductVolume(
          product.size.name,
          product.size.options[pickedSize].value,
          product.size.unit
        )}
      </Typography>
    </div>
  );
};

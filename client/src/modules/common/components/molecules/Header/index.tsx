import React from 'react';
import { Logo } from 'src/icons/Logo';
import { Search } from 'src/icons/Search';
import {
  Breakpoint,
  useBreakpoints,
} from 'src/modules/common/hooks/useBreakpoints';
import { Typography } from '../../atoms/Typography';
import { SearchModal } from '../../organisms/SearchModal';
import { Categories } from '../Categories';
import styles from './header.module.scss';
import { SearchBar } from '../SearchBar';

export type HeaderProps = {
  readonly activeCategoryId: number;
  readonly categories: {
    readonly id: number;
    readonly title: string;
  }[];
  readonly onCategoryChange: (categoryId: number) => void;
};

export const Header: React.FC<HeaderProps> = ({
  activeCategoryId,
  categories,
  onCategoryChange,
}) => {
  const [breakpoint, setBreakpoint] = React.useState<Breakpoint>(Breakpoint.SM);
  // const [isSearchModalOpen, setIsSearchModalOpen] = React.useState(false);
  // const [searchValue, setSearchValue] = React.useState('');
  const isMobile = breakpoint <= Breakpoint.SM;
  const isDesktop = breakpoint > Breakpoint.MD;

  useBreakpoints((breakpoint) => {
    setBreakpoint(breakpoint);
  });

  return (
    <>
      <header className={styles.root}>
        <div className={styles.root__content}>
          <div className={styles.root__about}>
            <Logo />
            {isDesktop && (
              <div className={styles.root__about__info}>
                <a
                  className={styles.root__about__info__link}
                  href="tel:+79526678888"
                >
                  <Typography preset="about" component="span">
                    {'+7 (952) 667 88 88'}
                  </Typography>
                </a>
                <a
                  href="https://yandex.ru/maps/-/CCUFvGRO1C"
                  className={styles.root__about__info__link}
                  target="_blank"
                  rel="noreferrer"
                >
                  <Typography preset="about">Спб, Невский 65</Typography>
                </a>
              </div>
            )}
          </div>
          <div className={styles.root__title}>
            <Typography preset="logo">mona tea</Typography>
            {!isMobile && (
              <Typography preset="subtitle">
                молочные чаи | смузи | матча | десерты
              </Typography>
            )}
          </div>

          <div className={styles.root__search}>
            {/* {isDesktop ? (
              <SearchBar
                value={searchValue}
                setValue={setSearchValue}
                onSearch={() => alert(searchValue)}
              />
            ) : (
              <button
                className={styles.root__search__button}
                onClick={() => setIsSearchModalOpen(true)}
              >
                <Search />
              </button>
            )} */}
          </div>
        </div>
        <Categories
          categories={categories}
          pickedId={activeCategoryId}
          onPick={onCategoryChange}
        />
      </header>
      {/* <SearchModal
        isToggled={isSearchModalOpen}
        onClose={() => setIsSearchModalOpen(false)}
      /> */}
    </>
  );
};

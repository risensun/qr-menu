import { useRouter } from 'next/router';
import React from 'react';
import {
  Breakpoint,
  useBreakpoints,
} from 'src/modules/common/hooks/useBreakpoints';
import { Typography } from '../../atoms/Typography';
import styles from './not-found.module.scss';

export const NotFound: React.FC = () => {
  const [isMobile, setIsMobile] = React.useState(false);
  const router = useRouter();

  useBreakpoints((breakpoint) => {
    setIsMobile(breakpoint < Breakpoint.MD);
  });

  const handleRedirectHome = () => {
    router.push('/');
  };

  return (
    <section className={styles.root}>
      <Typography preset="not-found-number" color="element">
        404
      </Typography>
      <div className={styles.root__info}>
        <Typography
          preset="not-found-title"
          align="center"
          className={styles.root__info__title}
        >
          Страница не найдена
        </Typography>
        <Typography
          preset="not-found-subtitle"
          align={isMobile ? 'center' : 'left'}
        >
          Воспользуйтесь меню, чтобы перейти в интересующий вас раздел!
        </Typography>
        <button
          className={styles.root__info__button}
          onClick={handleRedirectHome}
        >
          <Typography
            preset="not-found-button"
            className={styles.root__info__button__action}
            align="center"
          >
            Вернуться в меню
          </Typography>
        </button>
      </div>
    </section>
  );
};

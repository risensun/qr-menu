import React from 'react';
import clsx from 'clsx';
import { Typography } from '../../atoms/Typography';
import styles from './categories.module.scss';

export type CategoriesProps = {
  readonly categories: { id: number; title: string }[];
  readonly pickedId: number;
  readonly onPick: (categoryId: number) => void;
};

export const Categories: React.FC<CategoriesProps> = ({
  categories,
  pickedId,
  onPick,
}) => (
  <div className={styles.root}>
    {categories.map((category) => (
      <button
        className={clsx(
          styles.root__category,
          pickedId === category.id && styles.picked
        )}
        key={category.id}
        onClick={() => onPick(category.id)}
      >
        <Typography preset="category">{category.title}</Typography>
        <div
          className={clsx(
            styles.root__category__decoration,
            pickedId === category.id && styles.picked
          )}
        />
      </button>
    ))}
  </div>
);

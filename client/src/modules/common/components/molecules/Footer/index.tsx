import React from 'react';
import { About } from '../../atoms/About';
import styles from './footer.module.scss';

export const Footer: React.FC = () => (
  <footer className={styles.root}>
    <About />
  </footer>
);

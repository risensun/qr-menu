import clsx from 'clsx';
import React from 'react';
import { Typography } from '../../atoms/Typography';
import styles from './tag.module.scss';

export type TagProps = {
  readonly text: string;
  readonly isActive?: boolean;
  readonly onClick?: (...args: any[]) => void;
};

export const Tag: React.FC<TagProps> = ({
  text,
  onClick,
  isActive = false,
}) => (
  <button
    className={clsx(styles.root, isActive && styles.active)}
    onClick={onClick}
  >
    <Typography
      component="p"
      preset="tag"
      className={styles.root__text}
      color={isActive ? 'background' : 'paragraph'}
    >
      {text}
    </Typography>
  </button>
);

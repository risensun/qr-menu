import React from 'react';
import { Search } from 'src/icons/Search';
import styles from './search-bar.module.scss';

export type SearchBarProps = {
  readonly value: string;
  readonly setValue: React.Dispatch<React.SetStateAction<string>>;
  readonly onSearch?: React.MouseEventHandler<HTMLButtonElement>;
  readonly onChange?: React.ChangeEventHandler<HTMLInputElement>;
};

export const SearchBar: React.FC<SearchBarProps> = ({
  value,
  setValue,
  onSearch,
  onChange,
}) => (
  <div className={styles.root}>
    <button onClick={onSearch} className={styles.root__button}>
      <Search />
    </button>
    <input
      type="text"
      className={styles.root__input}
      value={value}
      onChange={(e) => {
        setValue(e.target.value);
        if (onChange) {
          onChange(e);
        }
      }}
      placeholder="Поиск"
    />
  </div>
);

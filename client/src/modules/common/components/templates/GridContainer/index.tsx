import clsx from 'clsx';
import React from 'react';
import styles from './grid-container.module.scss';

export type GridContainerProps = {
  readonly children: React.ReactNode;
  readonly className?: string;
};

export const GridContainer: React.FC<GridContainerProps> = ({
  children,
  className,
}) => <div className={clsx(styles.root, className)}>{children}</div>;

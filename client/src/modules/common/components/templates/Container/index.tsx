import clsx from 'clsx';
import React, { CSSProperties } from 'react';
import styles from './container.module.scss';

export type ContainerProps = {
  readonly children: React.ReactNode;
  readonly component?: React.ElementType;
  readonly className?: string;
  readonly style?: CSSProperties;
};

export const Container: React.FC<ContainerProps> = ({
  children,
  style,
  component = 'div',
  className: classNameFromProps,
}) => {
  const className = clsx(styles.root, classNameFromProps);

  return React.createElement(component, { style, className }, children);
};

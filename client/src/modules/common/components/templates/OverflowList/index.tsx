import clsx from 'clsx';
import React from 'react';
import styles from './overflow-list.module.scss';

export type OverflowListProps = {
  readonly children: React.ReactNode;
  readonly className?: string;
};

export const OverflowList: React.FC<OverflowListProps> = ({
  children,
  className,
}) => (
  <div className={clsx(styles.root, className)}>
    <ul className={styles.root__list}>{children}</ul>
  </div>
);

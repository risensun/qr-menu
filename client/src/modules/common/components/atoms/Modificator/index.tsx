import React from 'react';
import styles from './modificator.module.scss';

export type ModificatorProps = {
  readonly icon: JSX.Element;
};

export const Modificator: React.FC<ModificatorProps> = ({ icon }) => (
  <div className={styles.root}>{icon}</div>
);

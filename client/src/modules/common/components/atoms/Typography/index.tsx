import React, { CSSProperties } from 'react';
import clsx from 'clsx';
import { Color } from 'src/types/Color';
import { Font } from 'src/types/Font';
import styles from './typography.module.scss';

export type TypographyProps = {
  readonly children?: React.ReactNode;
  readonly component?: React.ElementType;
  readonly preset?:
    | 'common'
    | 'common-light'
    | 'common-volume'
    | 'price'
    | 'size'
    | 'nutrients-number'
    | 'nutrients-text'
    | 'category'
    | 'tag'
    | 'about'
    | 'subtitle'
    | 'logo'
    | 'header'
    | 'search-products'
    | 'card-title'
    | 'card-volume'
    | 'not-found-number'
    | 'not-found-title'
    | 'not-found-subtitle'
    | 'not-found-button';
  readonly font?: Font;
  readonly color?: Color;
  readonly style?: CSSProperties;
  readonly align?: 'left' | 'center' | 'right';
  readonly className?: string;
};

export const Typography: React.FC<TypographyProps> = ({
  children,
  style,
  color = 'paragraph',
  component = 'p',
  preset = 'common',
  font = 'ProximaNova',
  align = 'left',
  className: classNameFromProps,
}) => {
  const className = clsx(
    styles.root,
    styles[font],
    styles[preset],
    styles[color],
    styles[align],
    classNameFromProps
  );

  return React.createElement(component, { style, className }, children);
};

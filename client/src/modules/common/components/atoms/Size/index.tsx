import clsx from 'clsx';
import React from 'react';
import { Typography } from 'src/modules/common/components/atoms/Typography';
import styles from './size.module.scss';

export type SizeProps = {
  readonly text: string;
  readonly isPicked?: boolean;
  readonly onClick?: (...args: any[]) => void;
};

export const Size: React.FC<SizeProps> = ({
  text,
  isPicked = true,
  onClick,
}) => (
  <button
    className={clsx(styles.root, isPicked && styles.picked)}
    onClick={onClick}
  >
    <Typography
      component="span"
      color={isPicked ? 'background' : 'element'}
      preset="size"
    >
      {text}
    </Typography>
  </button>
);

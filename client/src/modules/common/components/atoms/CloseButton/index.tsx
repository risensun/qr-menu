import clsx from 'clsx';
import React from 'react';
import styles from './close-button.module.scss';

export type CloseButtonProps = {
  readonly onClick: React.MouseEventHandler<HTMLButtonElement>;
  readonly className?: string;
};

export const CloseButton: React.FC<CloseButtonProps> = ({
  onClick,
  className,
}) => (
  <button className={clsx(styles.root, className)} onClick={onClick}>
    <span className={styles.root__first} />
    <span className={styles.root__second} />
  </button>
);

import React from 'react';
import { Logo } from 'src/icons/Logo';
import { Typography } from '../Typography';
import styles from './about.module.scss';

export const About: React.FC = () => (
  <div className={styles.root}>
    <Logo />
    <div className={styles.root__info}>
      <a className={styles.root__info__link} href="tel:+79526678888">
        <Typography preset="about" component="span">
          {'+7 (952) 667 88 88'}
        </Typography>
      </a>
      <a
        href="https://yandex.ru/maps/-/CCUFvGRO1C"
        className={styles.root__info__link}
        target="_blank"
        rel="noreferrer"
      >
        <Typography preset="about">Спб, Невский 65</Typography>
      </a>
    </div>
  </div>
);

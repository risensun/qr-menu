import {
  IErrorResponse,
  IResponse,
  IRequestParams,
  IRequestWithBodyParams,
  RequestMethod,
  IAPIConfig,
  ISuccessResponse,
} from '../types';

export type Entity<T> = {
  entity: T;
};

export type Entities<T> = {
  entities: T[];
  count: number;
};

export class API {
  private static readonly defaultParams = {
    contentType: 'application/json',
    accept: 'application/json',
  };
  private static readonly tenantId = process.env.TENANT_ID;

  public constructor(
    private readonly baseUrl: string = baseUrl + API.tenantId
  ) {}

  public post<T>(
    params?: IRequestParams | IRequestWithBodyParams
  ): Promise<IResponse<T>> {
    return this.handleRequest({
      ...API.defaultParams,
      ...params,
      method: RequestMethod.POST,
    });
  }

  public get<T>(
    params?: IRequestParams | IRequestWithBodyParams
  ): Promise<IResponse<T>> {
    return this.handleRequest({
      ...API.defaultParams,
      ...params,
      method: RequestMethod.GET,
    });
  }

  private async handleRequest<T>(
    params: IRequestParams
  ): Promise<IResponse<T>> {
    try {
      const response = await fetch(
        this.baseUrl + API.stringifyQueryParams(params.query),
        API.getRequestInit(params)
      );

      const body = (await response.json()) as T;

      if (response.ok) {
        return { status: 'success', result: body };
      }

      return {
        status: 'error',
        errors: { error: response.statusText },
      };
    } catch (e: any) {
      return e.message;
    }
  }

  private static getRequestInit(
    params: IRequestParams | IRequestWithBodyParams
  ): RequestInit {
    const requestInit: RequestInit = {};

    requestInit.headers = API.getHeaders(params);
    requestInit.method = params.method;
    requestInit.cache = 'default';

    if ((params as IRequestWithBodyParams).body) {
      requestInit.body = API.getBody(params as IRequestWithBodyParams);
    }

    return requestInit;
  }

  private static getBody(
    params: IRequestWithBodyParams
  ):
    | string
    | Blob
    | ArrayBufferView
    | ArrayBuffer
    | FormData
    | URLSearchParams
    | ReadableStream<Uint8Array>
    | null
    | undefined {
    switch (params.contentType) {
      case 'application/json':
        return JSON.stringify(params.body);
      case 'multipart/form-data':
        return params.body as FormData;
      default:
        throw new Error("Can't convert to this body type");
    }
  }

  private static getHeaders(params: IRequestParams): Headers {
    const { headers: headersFromParams } = params;
    const headers = new Headers(headersFromParams);
    const tenantId = process.env.TENANT_ID;

    if (!tenantId) {
      throw new Error('[API]: TENANT_ID was not defined in .env files');
    }

    headers.append('X-Tenant-ID', tenantId);
    headers.append('Accept', 'application/json');

    if (
      'contentType' in params &&
      (params as IRequestWithBodyParams).contentType !== 'multipart/form-data' // this is done to force automatic header generation;
    ) {
      headers.append(
        'Content-Type',
        (params as IRequestWithBodyParams).contentType as any
      );
    }

    return headers;
  }

  private static stringifyQueryParams(
    query?: Record<string, string | number | boolean>
  ) {
    if (!query) {
      return '';
    }

    let currentQuery = '';
    Object.keys(query).forEach((key) => {
      currentQuery += `${
        currentQuery.length === 0 ? '?' : '&'
      }${key}=${encodeURIComponent(query[key])}`;
    });

    return currentQuery;
  }
}

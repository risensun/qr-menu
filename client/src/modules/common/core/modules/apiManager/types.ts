export interface IErrorResponse {
  status: 'error';
  errors: { [key: string]: string };
}

export interface ISuccessResponse<R = any> {
  status: 'success';
  result: R;
}

export type IResponse<R = any> = ISuccessResponse<R> | IErrorResponse;

export enum Status {
  SwitchingProtocols = 101,
  OK = 200,
  Created = 201,
  NoContent = 204,
  BadRequest = 400,
  Unauthorized = 401,
  Forbidden = 403,
  NotFound = 404,
  UnprocessableEntity = 422,
  InternalServerError = 500,
}

export enum RequestMethod {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
  PATCH = 'PATCH',
}

export interface IRequestParams {
  method?: RequestMethod;
  action?: string;
  headers?: Headers;
  isWithAuth?: boolean;
  query?: Record<string, string | number | boolean>;
  accept?: string;
}

// ? maybe we should pass generic param
export interface IRequestWithBodyParams extends IRequestParams {
  contentType?: string;
  body: any;
}

export interface IAPIConfig {}

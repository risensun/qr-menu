import { Config } from '../../utils/Config';

export class UrlBuilder {
  public static getApiRoute(route: string): string {
    const baseUrl = Config.getApiUrl();
    const menuId = Config.getMenuId();

    if (!baseUrl) {
      throw new Error('NEXT_PUBLIC_API_BASE_URL is not defined in .env.*');
    }
    return baseUrl + '/' + route;
  }
}

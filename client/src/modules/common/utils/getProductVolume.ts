export const getProductVolume = (
  name: string,
  value: number,
  unit: string
): string => `${name}: ${value} ${unit}`;

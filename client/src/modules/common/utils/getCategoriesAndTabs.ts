import { ICategory } from '../services/MenuService';

type Tab = {
  id: number;
  title: string;
};

type CategoriesAndTabs = {
  activeCategoryId: number;
  categories: Tab[];
  activeTabId: number;
  tabs: Tab[];
};

/**
 *
 * @param routeId - current id from route
 * @param rootIds - rootIds from back-end
 * @param categories - catalogues from back-end
 */

export const getCategoriesAndTabs = (
  routeId: number,
  rootIds: number[],
  categories: { [key: string]: ICategory }
) => {
  const result: CategoriesAndTabs = {
    activeCategoryId: routeId,
    categories: rootIds.map((id) => ({
      id: categories[id].id,
      title: categories[id].title,
    })),
    activeTabId: routeId,
    tabs: [], //
  };

  if (rootIds.includes(routeId)) {
    result.activeCategoryId = routeId;
    result.tabs = categories[routeId].children.map((id) => ({
      id: categories[id].id,
      title: categories[id].title,
    }));
  } else {
    for (let key in categories) {
      if (categories[key].children.includes(routeId)) {
        result.activeCategoryId = categories[key].id;
        result.tabs = categories[key].children.map((id) => ({
          id: categories[id].id,
          title: categories[id].title,
        }));
      }
    }
  }

  return result;
};

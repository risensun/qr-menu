export abstract class Config {
  public static getApiUrl() {
    const apiUrl = process.env.NEXT_PUBLIC_API_BASE_URL;

    if (!apiUrl) {
      throw new Error(
        '[Config]: NEXT_PUBLIC_API_BASE_URL is not defined in *.env'
      );
    }

    return apiUrl;
  }

  public static getMenuId() {
    const menuId = process.env.MENU_ID;

    if (!menuId) {
      throw new Error('[Config]: MENU_ID is not defined in *.env');
    }

    return menuId;
  }

  public static getHotBadgeId() {
    const hotBadgeId = process.env.MENU_BADGE_HOT_ID;

    if (!hotBadgeId) {
      throw new Error('[Config]: MENU_BADGE_HOT_ID is not defined in *.env');
    }

    return hotBadgeId;
  }

  public static getColdBadgeId() {
    const coldBadgeId = process.env.MENU_BADGE_COLD_ID;

    if (!coldBadgeId) {
      throw new Error('[Config]: MENU_BADGE_COLD_ID is not defined in *.env');
    }

    return coldBadgeId;
  }
}

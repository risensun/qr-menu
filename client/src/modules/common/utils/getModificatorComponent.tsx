import { Warning, Hot, Cold } from 'src/icons';
import { Config } from './Config';

const modificators = [
  { id: Config.getHotBadgeId(), component: <Hot /> },
  { id: Config.getColdBadgeId(), component: <Cold /> },
];

export const getModificatorComponent = (modificatorId: string): JSX.Element => {
  const modificator = modificators.find(
    (modificator) => modificator.id === modificatorId
  );

  if (modificator) {
    return modificator.component;
  }

  // eslint-disable-next-line no-console
  console.error(
    `[getModifiactorComponent]: No icon for id ${modificator} was found.`
  );

  return <Warning />;
};

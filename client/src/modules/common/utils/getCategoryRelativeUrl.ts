export function getCategoryRelativeUrl(id: number) {
  return `/category/${id}`;
}

import { Nutrients } from '../types/Product';

export enum NutrientsText {
  kkal = 'ккал',
  proteins = 'белки',
  fat = 'жиры',
  carbohydrate = 'углеводы',
}

export const getNutrientNameByKey = (key: keyof Nutrients): string =>
  NutrientsText[key];

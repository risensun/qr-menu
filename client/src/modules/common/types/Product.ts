export type Product = {
  id: number;
  menuIds: string[];
  createdAt: number;
  tenantId: string;
  categoryId: number;
  title: string;
  ingredients: string;
  description: string;
  size: Size;
  badges: string[];
  options: number[]; // not finished yet
  media: {
    miniatureSrc: string;
    gallery: string[]; //TODO: not finished type
  };
};

export type Size = {
  name: string;
  unit: string;
  options: Options[];
};

export type Options = {
  name: string;
  value: number;
  price: number;
  nutrients: Nutrients;
  key: string;
  isActive: boolean;
};

export type Nutrients = {
  kkal: number;
  proteins: number;
  fat: number;
  carbohydrate: number;
};

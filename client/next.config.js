/** @type {import('next').NextConfig} */

const withPreact = require('next-plugin-preact');

const nextConfig = withPreact({
  reactStrictMode: true,
  env: {
    NEXT_PUBLIC_API_BASE_URL: process.env.NEXT_PUBLIC_API_BASE_URL,
    TENANT_ID: process.env.TENANT_ID,
    MENU_BADGE_HOT_ID: process.env.MENU_BADGE_HOT_ID,
    MENU_BADGE_COLD_ID: process.env.MENU_BADGE_COLD_ID,
  },
  images: {
    domains: ['localhost', 'dev2.risensun.agency'],
  },
  webpack: (config, { dev, isServer }) => {
    if (!dev && !isServer) {
      Object.assign(config.resolve.alias, {
        react: 'preact/compat',
        'react-dom/test-utils': 'preact/test-utils',
        'react-dom': 'preact/compat',
      });
    }

    return config;
  },
});

module.exports = nextConfig;

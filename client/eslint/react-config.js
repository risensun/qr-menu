const types = require('./types.js');

module.exports = {
  rules: {
    'react-hooks/exhaustive-deps': types.OFF,
    'react/prop-types': types.OFF,
    'react/react-in-jsx-scope': types.OFF,
    'react/display-name': types.OFF,
    'class-methods-use-this': [
      'error',
      {
        exceptMethods: [
          'render',
          'getInitialState',
          'getDefaultProps',
          'componentDidMount',
          'componentWillReceiveProps',
          'shouldComponentUpdate',
          'componentDidUpdate',
          'componentWillUnmount',
        ],
      },
    ],
  },
};
